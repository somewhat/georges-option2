import React, { Component } from 'react';
import google_btn from './images/google-btn.png';
import app_btn from './images/app-btn.png';
import fold_1_bg from './images/fold-1-bg.png';
import fold_1_mob from './images/fold-1-mob.png';
import arrow from './images/arrow.png';

class Fold1 extends Component {
  render() {
    return (
		<div className="fold first-fold">
        	<div className="fold-left heading-wrap">
            	<div>
                	<div className="heading">
                    	<p><br /><br />YOU WORK HARD <br />FOR YOUR MONEY.</p>
                        <p>NOW LET'S PUT <br className="desk-hide" />YOUR <br />MONEY TO WORK.</p>
                    </div>
                    <div className="desc">
                    	<p>Grow your dollars effortlessy, for <br />free. No investment experience <br />needed.</p>
                        <p>We'll grind. You shine.</p>
					</div>
					 <img src={fold_1_mob} className="mob-img mobile" alt="" />
                    <div className="app-links">
                        <a href="https://bnc.lt/E1yh/4FtmTYT4MJ" target="_blank" rel="noopener noreferrer"><img src={google_btn} alt="" /></a>
                        <a href="https://bnc.lt/E1yh/4FtmTYT4MJ" target="_blank" rel="noopener noreferrer"><img src={app_btn} alt="" /></a>
                    </div>
                </div>
               
            </div>
            <div className="fold-right w-70 desktop">
            	<img src={fold_1_bg} className="currency" alt="" />
                <div className="scroll-link"><img src={arrow} alt="" /> Scroll down</div>
            </div>
        </div>
    );
  }
}


export default Fold1;
