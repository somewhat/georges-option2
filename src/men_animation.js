import React, { Component } from 'react';

import men0 from './images/3x_5/3x_509.png';

import f_1_1 from './images/3x_5/3x_510.png';
import f_1_2 from './images/3x_5/3x_511.png';
import f_1_3 from './images/3x_5/3x_512.png';
import f_1_4 from './images/3x_5/3x_513.png';
import f_1_5 from './images/3x_5/3x_514.png';
import f_1_6 from './images/3x_5/3x_515.png';
import f_1_7 from './images/3x_5/3x_516.png';
import f_1_8 from './images/3x_5/3x_517.png';
import f_1_9 from './images/3x_5/3x_518.png';
import f_1_10 from './images/3x_5/3x_519.png';
import f_1_11 from './images/3x_5/3x_520.png';
import f_1_12 from './images/3x_5/3x_521.png';

import f_2_1 from './images/3x_11/3x_1107.png';
import f_2_2 from './images/3x_11/3x_1110.png';
import f_2_3 from './images/3x_11/3x_1111.png';
import f_2_4 from './images/3x_11/3x_1112.png';
import f_2_5 from './images/3x_11/3x_1113.png';
import f_2_6 from './images/3x_11/3x_1114.png';
import f_2_7 from './images/3x_11/3x_1115.png';
import f_2_8 from './images/3x_11/3x_1144.png';
import f_2_9 from './images/3x_11/3x_1148.png';

import f_3_1 from './images/3x_15/3x_15042.png';
import f_3_2 from './images/3x_15/3x_15043.png';
import f_3_3 from './images/3x_15/3x_15045.png';
import f_3_4 from './images/3x_15/3x_15046.png';
import f_3_5 from './images/3x_15/3x_15047.png';
import f_3_6 from './images/3x_15/3x_15050.png';
import f_3_7 from './images/3x_15/3x_15066.png';
import f_3_8 from './images/3x_15/3x_15067.png';
import f_3_9 from './images/3x_15/3x_15068.png';
import f_3_10 from './images/3x_15/3x_15069.png';
import f_3_11 from './images/3x_15/3x_15070.png';
import f_3_12 from './images/3x_15/3x_15071.png';
import f_3_13 from './images/3x_15/3x_15072.png';
import f_3_14 from './images/3x_15/3x_15073.png';
import f_3_15 from './images/3x_15/3x_15074.png';
import f_3_16 from './images/3x_15/3x_15089.png';
import f_3_17 from './images/3x_15/3x_15090.png';
import f_3_18 from './images/3x_15/3x_15091.png';
import f_3_19 from './images/3x_15/3x_15093.png';
import f_3_20 from './images/3x_15/3x_15094.png';
import f_3_21 from './images/3x_15/3x_15095.png';

import f_4_1 from './images/3x_22/3x_22043.png';
import f_4_2 from './images/3x_22/3x_22044.png';
import f_4_3 from './images/3x_22/3x_22050.png';
import f_4_4 from './images/3x_22/3x_22062.png';
import f_4_5 from './images/3x_22/3x_22063.png';
import f_4_6 from './images/3x_22/3x_22064.png';
import f_4_7 from './images/3x_22/3x_22065.png';
import f_4_8 from './images/3x_22/3x_22066.png';
import f_4_9 from './images/3x_22/3x_22067.png';
import f_4_10 from './images/3x_22/3x_22068.png';
import f_4_11 from './images/3x_22/3x_22074.png';
import f_4_12 from './images/3x_22/3x_22096.png';

import f_5_1 from './images/3x_27/3x_2707.png';
import f_5_2 from './images/3x_27/3x_2708.png';
import f_5_3 from './images/3x_27/3x_2714.png';
import f_5_4 from './images/3x_27/3x_2719.png';
import f_5_5 from './images/3x_27/3x_2727.png';
import f_5_6 from './images/3x_27/3x_2741.png';
import f_5_7 from './images/3x_27/3x_2753.png';
import f_5_8 from './images/3x_27/3x_2759.png';
import f_5_9 from './images/3x_27/3x_2765.png';

import f_6_1 from './images/3x_31/3x_31000.png';
import f_6_2 from './images/3x_31/3x_31010.png';
import f_6_3 from './images/3x_31/3x_31043.png';
import f_6_4 from './images/3x_31/3x_31047.png';
import f_6_5 from './images/3x_31/3x_31048.png';
import f_6_6 from './images/3x_31/3x_31049.png';
import f_6_7 from './images/3x_31/3x_31050.png';
import f_6_8 from './images/3x_31/3x_31051.png';
import f_6_9 from './images/3x_31/3x_31063.png';
import f_6_10 from './images/3x_31/3x_31066.png';
import f_6_11 from './images/3x_31/3x_31075.png';
import f_6_12 from './images/3x_31/3x_31076.png';
import f_6_13 from './images/3x_31/3x_31082.png';
import f_6_14 from './images/3x_31/3x_31093.png';
import f_6_15 from './images/3x_31/3x_31094.png';
import f_6_16 from './images/3x_31/3x_31095.png';
import f_6_17 from './images/3x_31/3x_31096.png';
import f_6_18 from './images/3x_31/3x_31097.png';
import f_6_19 from './images/3x_31/3x_31101.png';

import f_7_1 from './images/3x_35/3x_35000.png';
import f_7_2 from './images/3x_35/3x_35030.png';
import f_7_3 from './images/3x_35/3x_35044.png';
import f_7_4 from './images/3x_35/3x_35045.png';
import f_7_5 from './images/3x_35/3x_35046.png';
import f_7_6 from './images/3x_35/3x_35047.png';
import f_7_7 from './images/3x_35/3x_35048.png';
import f_7_8 from './images/3x_35/3x_35049.png';
import f_7_9 from './images/3x_35/3x_35050.png';
import f_7_10 from './images/3x_35/3x_35051.png';
import f_7_11 from './images/3x_35/3x_35052.png';
import f_7_12 from './images/3x_35/3x_35059.png';
import f_7_13 from './images/3x_35/3x_35064.png';
import f_7_14 from './images/3x_35/3x_35065.png';
import f_7_15 from './images/3x_35/3x_35071.png';
import f_7_16 from './images/3x_35/3x_35089.png';
import f_7_17 from './images/3x_35/3x_35090.png';
import f_7_18 from './images/3x_35/3x_35091.png';
import f_7_19 from './images/3x_35/3x_35092.png';
import f_7_20 from './images/3x_35/3x_35093.png';
import f_7_21 from './images/3x_35/3x_35094.png';
import f_7_22 from './images/3x_35/3x_35105.png';

import f_8_1 from './images/3x_39/3x_3948.png';
import f_8_2 from './images/3x_39/3x_3949.png';
import f_8_3 from './images/3x_39/3x_3954.png';
import f_8_4 from './images/3x_39/3x_3955.png';
import f_8_5 from './images/3x_39/3x_3956.png';
import f_8_6 from './images/3x_39/3x_3957.png';
import f_8_7 from './images/3x_39/3x_3958.png';
import f_8_8 from './images/3x_39/3x_3969.png';

class Menanimation extends Component {
  render() {
    return (
		<div className="men-animation desktop" >
        	
        	<img src={men0} className="men-0" alt="" />
            
            <img src={f_1_1} className="men-1 fold-1" alt="" />
            <img src={f_1_2} className="men-1 fold-1" alt="" />
            <img src={f_1_3} className="men-1 fold-1" alt="" />
            <img src={f_1_4} className="men-1 fold-1" alt="" />
            <img src={f_1_5} className="men-1 fold-1" alt="" />
            <img src={f_1_6} className="men-1 fold-1" alt="" />
            <img src={f_1_7} className="men-1 fold-1" alt="" />
            <img src={f_1_8} className="men-1 fold-1" alt="" />
            <img src={f_1_9} className="men-1 fold-1" alt="" />
            <img src={f_1_10} className="men-1 fold-1" alt="" />
            <img src={f_1_11} className="men-1 fold-1" alt="" />
            <img src={f_1_12} className="men-1 fold-1" alt="" />
            
            <img src="" img-url={f_2_1} className="men-1 fold-2" alt="" />
            <img src="" img-url={f_2_2} className="men-2 fold-2" alt="" />
            <img src="" img-url={f_2_3} className="men-3 fold-2" alt="" />
            <img src="" img-url={f_2_4} className="men-4 fold-2" alt="" />
            <img src="" img-url={f_2_5} className="men-5 fold-2" alt="" />
            <img src="" img-url={f_2_6} className="men-6 fold-2" alt="" />
            <img src="" img-url={f_2_7} className="men-7 fold-2" alt="" />
            <img src="" img-url={f_2_8} className="men-8 fold-2" alt="" />
            <img src="" img-url={f_2_9} className="men-9 fold-2" alt="" />
            
            <img src="" img-url={f_3_1} className="men-10 fold-3" alt="" />
            <img src="" img-url={f_3_2} className="men-11 fold-3" alt="" />
            <img src="" img-url={f_3_3} className="men-12 fold-3" alt="" />
            <img src="" img-url={f_3_4} className="men-13 fold-3" alt="" />
            <img src="" img-url={f_3_5} className="men-14 fold-3" alt="" />
            <img src="" img-url={f_3_6} className="men-15 fold-3" alt="" />
            <img src="" img-url={f_3_7} className="men-16 fold-3" alt="" />
            <img src="" img-url={f_3_8} className="men-17 fold-3" alt="" />
            <img src="" img-url={f_3_9} className="men-17 fold-3" alt="" />
            <img src="" img-url={f_3_10} className="men-17 fold-3" alt="" />
            <img src="" img-url={f_3_11} className="men-17 fold-3" alt="" />
            <img src="" img-url={f_3_12} className="men-18 fold-3" alt="" />
            <img src="" img-url={f_3_13} className="men-18 fold-3" alt="" />
            <img src="" img-url={f_3_14} className="men-18 fold-3" alt="" />
            <img src="" img-url={f_3_15} className="men-18 fold-3" alt="" />
            <img src="" img-url={f_3_16} className="men-19 fold-3" alt="" />
            <img src="" img-url={f_3_17} className="men-20 fold-3" alt="" />
            <img src="" img-url={f_3_18} className="men-20 fold-3" alt="" />
            <img src="" img-url={f_3_19} className="men-20 fold-3" alt="" />
            <img src="" img-url={f_3_20} className="men-20 fold-3" alt="" />
            <img src="" img-url={f_3_21} className="men-20 fold-3" alt="" />
            
            <img src="" img-url={f_4_1} className="men-21 fold-4" alt="" />
            <img src="" img-url={f_4_2} className="men-22 fold-4" alt="" />
            <img src="" img-url={f_4_3} className="men-23 fold-4" alt="" />
            <img src="" img-url={f_4_4} className="men-24 fold-4" alt="" />
            <img src="" img-url={f_4_5} className="men-25 fold-4" alt="" />
            <img src="" img-url={f_4_6} className="men-26 fold-4" alt="" />
            <img src="" img-url={f_4_7} className="men-27 fold-4" alt="" />
            <img src="" img-url={f_4_8} className="men-28 fold-4" alt="" />
            <img src="" img-url={f_4_9} className="men-29 fold-4" alt="" />
            <img src="" img-url={f_4_10} className="men-30 fold-4" alt="" />
            <img src="" img-url={f_4_11} className="men-31 fold-4" alt="" />
            <img src="" img-url={f_4_12} className="men-32 fold-4" alt="" />
            
            <img src="" img-url={f_5_1} className="men-32 fold-5" alt="" />
            <img src="" img-url={f_5_2} className="men-33 fold-5" alt="" />
            <img src="" img-url={f_5_3} className="men-34 fold-5" alt="" />
			<img src="" img-url={f_5_4} className="men-35 fold-5" alt="" />
            <img src="" img-url={f_5_5} className="men-36 fold-5" alt="" />
            <img src="" img-url={f_5_6} className="men-37 fold-5" alt="" />
            <img src="" img-url={f_5_7} className="men-38 fold-5" alt="" />
            <img src="" img-url={f_5_8} className="men-39 fold-5" alt="" />
            <img src="" img-url={f_5_9} className="men-40 fold-5" alt="" />
            
            <img src="" img-url={f_6_1} className="men-41 fold-6" alt="" />
            <img src="" img-url={f_6_2} className="men-42 fold-6" alt="" />
            <img src="" img-url={f_6_3} className="men-43 fold-6" alt="" />
            <img src="" img-url={f_6_4} className="men-44 fold-6" alt="" />
            <img src="" img-url={f_6_5} className="men-45 fold-6" alt="" />
            <img src="" img-url={f_6_6} className="men-46 fold-6" alt="" />
            <img src="" img-url={f_6_7} className="men-47 fold-6" alt="" />
            <img src="" img-url={f_6_8} className="men-48 fold-6" alt="" />
            <img src="" img-url={f_6_9} className="men-49 fold-6" alt="" />
            <img src="" img-url={f_6_10} className="men-50 fold-6" alt="" />
            <img src="" img-url={f_6_11} className="men-51 fold-6" alt="" />
            <img src="" img-url={f_6_12} className="men-52 fold-6" alt="" />
            <img src="" img-url={f_6_13} className="men-53 fold-6" alt="" />
            <img src="" img-url={f_6_14} className="men-54 fold-6" alt="" />
            <img src="" img-url={f_6_15} className="men-55 fold-6" alt="" />
            <img src="" img-url={f_6_16} className="men-56 fold-6" alt="" />
            <img src="" img-url={f_6_17} className="men-57 fold-6" alt="" />
            <img src="" img-url={f_6_18} className="men-58 fold-6" alt="" />
            <img src="" img-url={f_6_19} className="men-59 fold-6" alt="" />

			<img src="" img-url={f_7_1} className="men-60 fold-7" alt="" />
            <img src="" img-url={f_7_2} className="men-61 fold-7" alt="" />
            <img src="" img-url={f_7_3} className="men-62 fold-7" alt="" />
            <img src="" img-url={f_7_4} className="men-63 fold-7" alt="" />
            <img src="" img-url={f_7_5} className="men-64 fold-7" alt="" />
            <img src="" img-url={f_7_6} className="men-65 fold-7" alt="" />
            <img src="" img-url={f_7_7} className="men-66 fold-7" alt="" />
            <img src="" img-url={f_7_8} className="men-67 fold-7" alt="" />
            <img src="" img-url={f_7_9} className="men-68 fold-7" alt="" />
            <img src="" img-url={f_7_10} className="men-69 fold-7" alt="" />
            <img src="" img-url={f_7_11} className="men-70 fold-7" alt="" />
            <img src="" img-url={f_7_12} className="men-71 fold-7" alt="" />
            <img src="" img-url={f_7_13} className="men-72 fold-7" alt="" />
            <img src="" img-url={f_7_14} className="men-73 fold-7" alt="" />
            <img src="" img-url={f_7_15} className="men-74 fold-7" alt="" />
            <img src="" img-url={f_7_16} className="men-75 fold-7" alt="" />
            <img src="" img-url={f_7_17} className="men-76 fold-7" alt="" />
            <img src="" img-url={f_7_18} className="men-77 fold-7" alt="" />
            <img src="" img-url={f_7_19} className="men-78 fold-7" alt="" />
            <img src="" img-url={f_7_20} className="men-79 fold-7" alt="" />
            <img src="" img-url={f_7_21} className="men-80 fold-7" alt="" />
            <img src="" img-url={f_7_22} className="men-81 fold-7" alt="" />
            
            <img src="" img-url={f_8_1} className="men-82 fold-8" alt="" />
            <img src="" img-url={f_8_2} className="men-83 fold-8" alt="" />
            <img src="" img-url={f_8_3} className="men-84 fold-8" alt="" />
            <img src="" img-url={f_8_4} className="men-85 fold-8" alt="" />
            <img src="" img-url={f_8_5} className="men-86 fold-8" alt="" />
            <img src="" img-url={f_8_6} className="men-87 fold-8" alt="" />
            <img src="" img-url={f_8_7} className="men-88 fold-8" alt="" />
            <img src="" img-url={f_8_8} className="men-89 fold-8" alt="" />
        </div>
    );
  }
}


export default Menanimation;
