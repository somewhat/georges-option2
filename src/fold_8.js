import React, { Component } from 'react';
import fold_8_mob from './images/fold-8-mob.png';
import pin from './images/pin.png';
import fold_9 from './images/fold-9.png';

import map_1_1 from './images/3x_36/3x_36037.png';
import map_1_2 from './images/3x_36/3x_36038.png';
import map_1_3 from './images/3x_36/3x_36039.png';
import map_1_4 from './images/3x_36/3x_36040.png';
import map_1_5 from './images/3x_36/3x_36041.png';
import map_1_6 from './images/3x_36/3x_36042.png';
import map_1_7 from './images/3x_36/3x_36043.png';
import map_1_8 from './images/3x_36/3x_36044.png';
import map_1_9 from './images/3x_36/3x_36045.png';
import map_1_10 from './images/3x_36/3x_36046.png';
import map_1_11 from './images/3x_36/3x_36047.png';
import map_1_12 from './images/3x_36/3x_36048.png';
import map_1_13 from './images/3x_36/3x_36049.png';
import map_1_14 from './images/3x_36/3x_36050.png';
import map_1_15 from './images/3x_36/3x_36051.png';
import map_1_16 from './images/3x_36/3x_36052.png';
import map_1_17 from './images/3x_36/3x_36053.png';
import map_1_18 from './images/3x_36/3x_36054.png';
import map_1_19 from './images/3x_36/3x_36055.png';
import map_1_20 from './images/3x_36/3x_36056.png';
import map_1_21 from './images/3x_36/3x_36057.png';

class Fold8 extends Component {
  render() {
    return (
		<div className="fold" id="fold_8">
        	<div className="fold-left w-30">
            	<div>
                	<div className="small-heading">LONG-TERM GOALS</div>
                	<div className="sub-heading">You get more from your money.</div>
                    <div className="sub-desc">Life is good when you have a personalized portfolio in the palm of your hand (literally) and a professional strategy keeping George on track.</div>
                </div>
                <img src={fold_8_mob} className="mob-img mobile" alt="" />
            </div>
            <div className="fold-right w-70 desktop">
            	<div className="map-wrap">
                	<div className="map-inner-wrap">
                    	<img src="" img-url={map_1_1} className="map-1" alt="" />
						<img src="" img-url={map_1_2} className="map-1" alt="" />
						<img src="" img-url={map_1_3} className="map-1" alt="" />
						<img src="" img-url={map_1_4} className="map-1" alt="" />
						<img src="" img-url={map_1_5} className="map-1" alt="" />
						<img src="" img-url={map_1_6} className="map-1" alt="" />
						<img src="" img-url={map_1_7} className="map-1" alt="" />
						<img src="" img-url={map_1_8} className="map-1" alt="" />
						<img src="" img-url={map_1_9} className="map-1" alt="" />
						<img src="" img-url={map_1_10} className="map-1" alt="" />
						<img src="" img-url={map_1_11} className="map-1" alt="" />
						<img src="" img-url={map_1_12} className="map-1" alt="" />
						<img src="" img-url={map_1_13} className="map-1" alt="" />
						<img src="" img-url={map_1_14} className="map-1" alt="" />
						<img src="" img-url={map_1_15} className="map-1" alt="" />
						<img src="" img-url={map_1_16} className="map-1" alt="" />
						<img src="" img-url={map_1_17} className="map-1" alt="" />
						<img src="" img-url={map_1_18} className="map-1" alt="" />
						<img src="" img-url={map_1_19} className="map-1" alt="" />
						<img src="" img-url={map_1_20} className="map-1" alt="" />
						<img src="" img-url={map_1_21} className="map-1" alt="" />

                    </div>
                    <img src="" img-url={pin} className="pin" alt="" />
                    <div className="fold-9">
                    	<img src="" img-url={fold_9} className="men-90" alt="" />
                    </div>
                </div>
            </div>
        </div>
    );
  }
}


export default Fold8;
