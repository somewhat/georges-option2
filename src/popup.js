import React, { Component } from 'react';
import dollar_img from './images/dollar-img.png';

class Popup extends Component {
  render() {
    return (
		<div className="popup-mask">
    	
			<div className="popup-inner-wrap" id="risk-wrap">
				<div className="popup-close"></div>
				<div className="popup-heading">We listen.</div>
				<p>To understand your needs and risk tolerance (i.e., how much fluctuation in investment value you’re okay with), we’ll ask you a few questions in the app about how long you plan to let your investments grow and how comfortable you are with risk. These questions will give us an idea of your objectives and risk comfort level.</p>
				<p>Rest assured that your answers are a guide, and we’ll use this information and our expertise to provide you with a portfolio that makes sense for you. For all accounts, we seek to protect members’ contributions (their principal) and grow their money steadily.</p> 
			</div>
			<div className="popup-inner-wrap" id="asset-wrap">
				<div className="popup-close"></div>
				<div className="popup-heading">We build.</div>
				<p>Asset allocation is the super-important process of selecting the right investment mix of asset types, such as equities (stocks) and fixed income (bonds), for you. We love this part!</p>
				<p>Our partner Wilshire Funds Management leverages the same insights and resources that it uses to manage its largest institutional clients to create a series of five asset allocation portfolios specifically designed for MoneyLion clients. We’ll match you with one based on your objectives and risk comfort level.</p> 
			</div>
			<div className="popup-inner-wrap big" id="investment-wrap">
				<div className="popup-close"></div>
				<div className="popup-heading">We select.</div>
				<div>
					<div className="w-30">
						
						<p>Investment products are the basic building blocks of your portfolio, but picking the right ones to implement your asset allocation can be daunting.<br />Don’t worry - We’ve got you!</p>
						<p>Wilshire routinely surveys and monitors the entire universe of over 2,000  ETFs, selecting only those that are well priced, <a href="https://blog.moneylion.com/know-your-liquidity-needs-to-invest-smarter/" target="_blank" rel="noopener noreferrer">highly liquid</a>, most efficiently traded, well established, and have a demonstrable track record of closely replicating the returns of their benchmarks. We use these ETFs to implement our asset allocation recommendations.</p> 
					</div>
					
					<div className="select-table">
						<table cellPadding="0" cellSpacing="0" width="100%" >
							<thead>
							<tr>
								<th>Asset class</th>
								<th>ETF</th>
								<th>Ticker</th>
							</tr>
							</thead>
							<tbody>
							<tr>
								<td>US large-cap stocks</td>
								<td>Vanguard S&P 500 ETF</td>
								<td>VOO</td>
							</tr>                                                
							<tr>
								<td>International developed market stocks</td>
								<td>Vanguard FTSE Developed Markets ETF</td>
								<td>VEA</td>
							</tr>
							<tr>
								<td>US small/mid-cap stocks</td>
								<td>Vanguard Extended Market ETF</td>
								<td>VXF</td>
							</tr>
							<tr>
								<td>International emerging market stocks </td>
								<td>Vanguard FTSE Emerging Markets ETF</td>
								<td>VWO</td>
							</tr>                                                
							<tr>
								<td>Short-term municipal bonds </td>
								<td>iShares Short-Term Nat'l Muni Bond ETF</td>
								<td>SUB</td>
							</tr>
							<tr>
								<td>US municipal bonds</td>
								<td>Vanguard Tax-Exempt Bond ETF</td>
								<td>VTEB</td>
							</tr>
							<tr>
								<td>International stocks </td>
								<td>Vanguard Total International Stock ETF</td>
								<td>VXUS</td>
							</tr>                                                
							<tr>
								<td>Cash</td>
								<td>Cash</td>
								<td>N/A</td>
							</tr>
							</tbody>
						</table>
					</div>
				</div>
			</div>
			<div className="popup-inner-wrap" id="personalized-wrap">
				<div className="popup-close"></div>
				<div className="popup-heading">We invest.</div>
				<p>We give you a well-diversified portfolio aligned to your asset allocation and stated preferences. When you add money to your investment account, we’ll invest it in shares of ETFs or fractions of shares for you, to help make your dollar go further. With no management fees and no minimums.</p>
				<p>The five asset class portfolios will comprise different weightings of asset classes and be implemented with carefully selected ETFs based on your risk preference. When you invest money (however much you choose, whenever you choose), we will buy ETF shares (or fractions of shares) on your behalf. And you can view your portfolio of investments any time in the MoneyLion app.</p> 
				<p><img src={dollar_img} alt="" /></p>
			</div>
			<div className="popup-inner-wrap" id="rebalancing-wrap">
				<div className="popup-close"></div>
				<div className="popup-heading">We manage.</div>
				<p>Over time, your allocations might "drift" based on the performance (change in value) of different investments in your portfolio. Portfolio rebalancing may be needed when this happens. Again, we’ve got you!</p>
				<p>We’ll monitor your account and rebalance as needed to keep it aligned to your personalized allocation, which as you remember, is aligned to your unique needs and goals.</p> 
			</div>
			
		</div>
    );
  }
}


export default Popup;
