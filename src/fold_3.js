import React, { Component } from 'react';
import fold_3_mob from './images/fold-3-mob.png';

import comfort_1 from './images/3x_12/3x_12000.png';
import comfort_2 from './images/3x_12/3x_12005.png';
import comfort_3 from './images/3x_12/3x_12010.png';
import comfort_4 from './images/3x_12/3x_12015.png';
import comfort_5 from './images/3x_12/3x_12020.png';
import comfort_6 from './images/3x_12/3x_12028.png';
import comfort_7 from './images/3x_12/3x_12031.png';
import comfort_8 from './images/3x_12/3x_12035.png';
import comfort_9 from './images/3x_12/3x_12039.png';
import comfort_10 from './images/3x_12/3x_12053.png';
import comfort_11 from './images/3x_12/3x_12066.png';
import comfort_12 from './images/3x_12/3x_12075.png';
import comfort_13 from './images/3x_12/3x_12081.png';
import comfort_14 from './images/3x_12/3x_12087.png';
import comfort_15 from './images/3x_12/3x_12100.png';
import comfort_16 from './images/3x_12/3x_12104.png';
import comfort_17 from './images/3x_12/3x_12109.png';
import comfort_18 from './images/3x_12/3x_12117.png';

import time_1 from './images/3x_13/3x_13036.png';
import time_2 from './images/3x_13/3x_13037.png';
import time_3 from './images/3x_13/3x_13039.png';
import time_4 from './images/3x_13/3x_13040.png';
import time_5 from './images/3x_13/3x_13041.png';
import time_6 from './images/3x_13/3x_13042.png';
import time_7 from './images/3x_13/3x_13043.png';
import time_8 from './images/3x_13/3x_13045.png';
import time_9 from './images/3x_13/3x_13046.png';
import time_10 from './images/3x_13/3x_13047.png';
import time_11 from './images/3x_13/3x_13048.png';
import time_12 from './images/3x_13/3x_13049.png';
import time_13 from './images/3x_13/3x_13050.png';
import time_14 from './images/3x_13/3x_13055.png';
import time_15 from './images/3x_13/3x_13061.png';
import time_16 from './images/3x_13/3x_13062.png';
import time_17 from './images/3x_13/3x_13063.png';
import time_18 from './images/3x_13/3x_13064.png';
import time_19 from './images/3x_13/3x_13065.png';
import time_20 from './images/3x_13/3x_13073.png';

import objective_1 from './images/3x_14/3x_14000.png';
import objective_2 from './images/3x_14/3x_14005.png';
import objective_3 from './images/3x_14/3x_14010.png';
import objective_4 from './images/3x_14/3x_14015.png';
import objective_5 from './images/3x_14/3x_14020.png';
import objective_6 from './images/3x_14/3x_14025.png';
import objective_7 from './images/3x_14/3x_14030.png';
import objective_8 from './images/3x_14/3x_14035.png';
import objective_9 from './images/3x_14/3x_14040.png';
import objective_10 from './images/3x_14/3x_14045.png';
import objective_11 from './images/3x_14/3x_14050.png';
import objective_12 from './images/3x_14/3x_14055.png';
import objective_13 from './images/3x_14/3x_14060.png';
import objective_14 from './images/3x_14/3x_14065.png';
import objective_15 from './images/3x_14/3x_14070.png';
import objective_16 from './images/3x_14/3x_14075.png';
import objective_17 from './images/3x_14/3x_14080.png';
import objective_18 from './images/3x_14/3x_14085.png';
import objective_19 from './images/3x_14/3x_14090.png';
import objective_20 from './images/3x_14/3x_14095.png';
import objective_21 from './images/3x_14/3x_14100.png';
import objective_22 from './images/3x_14/3x_14105.png';
import objective_23 from './images/3x_14/3x_14110.png';
import objective_24 from './images/3x_14/3x_14115.png';

class Fold3 extends Component {
  render() {
    return (
		<div className="fold" id="fold_3">
        	<div className="fold-left w-30">
            	<div>
                	<div className="small-heading">RISK PROFILE</div>
                	<div className="sub-heading">We listen.</div>
                    <div className="sub-desc">With a few taps in the app, we’ll identify your investing comfort zone and objectives. This is all about you.</div>
					<img src={fold_3_mob} className="mob-img mobile" alt="" />
                    <span rel="risk-wrap" className="learn-more-link">LEARN MORE</span>
                </div>
                
            </div>
            <div className="fold-right w-70 desktop">
            	<div className="fold-3-inner-wrap">
                	<div className="comfort-wrap">
                    	<img src="" img-url={comfort_1} className="comfort-img" alt="" />
                        <img src="" img-url={comfort_2} className="comfort-img" alt="" />
                        <img src="" img-url={comfort_3} className="comfort-img" alt="" />
                        <img src="" img-url={comfort_4} className="comfort-img" alt="" />
                        <img src="" img-url={comfort_5} className="comfort-img" alt="" />
                        <img src="" img-url={comfort_6} className="comfort-img" alt="" />
                        <img src="" img-url={comfort_7} className="comfort-img" alt="" />
                        <img src="" img-url={comfort_8} className="comfort-img" alt="" />
                        <img src="" img-url={comfort_9} className="comfort-img" alt="" />
                        <img src="" img-url={comfort_10} className="comfort-img" alt="" />
                        <img src="" img-url={comfort_11} className="comfort-img" alt="" />
                        <img src="" img-url={comfort_12} className="comfort-img" alt="" />
                        <img src="" img-url={comfort_13} className="comfort-img" alt="" />
                        <img src="" img-url={comfort_14} className="comfort-img" alt="" />
                        <img src="" img-url={comfort_15} className="comfort-img" alt="" />
                        <img src="" img-url={comfort_16} className="comfort-img" alt="" />
                        <img src="" img-url={comfort_17} className="comfort-img" alt="" />
                        <img src="" img-url={comfort_18} className="comfort-img" alt="" />
                    </div>
                    <div className="time-wrap">
                    	<img src="" img-url={time_1} className="time-img" alt="" />
                        <img src="" img-url={time_2} className="time-img" alt="" />
                        <img src="" img-url={time_3} className="time-im9g" alt="" />
                        <img src="" img-url={time_4} className="time-img" alt="" />
                        <img src="" img-url={time_5} className="time-img" alt="" />
                        <img src="" img-url={time_6} className="time-img" alt="" />
                        <img src="" img-url={time_7} className="time-img" alt="" />
                        <img src="" img-url={time_8} className="time-img" alt="" />
                        <img src="" img-url={time_9} className="time-img" alt="" />
                        <img src="" img-url={time_10} className="time-img" alt="" />
                        <img src="" img-url={time_11} className="time-img" alt="" />
                        <img src="" img-url={time_12} className="time-img" alt="" />
                        <img src="" img-url={time_13} className="time-img" alt="" />
                        <img src="" img-url={time_14} className="time-img" alt="" />
                        <img src="" img-url={time_15} className="time-img" alt="" />
                        <img src="" img-url={time_16} className="time-img" alt="" />
                        <img src="" img-url={time_17} className="time-img" alt="" />
                        <img src="" img-url={time_18} className="time-img" alt="" />
                        <img src="" img-url={time_19} className="time-img" alt="" />
                        <img src="" img-url={time_20} className="time-img" alt="" />
                    </div>
                    <div className="objective-wrap">
                    	<img src="" img-url={objective_1} className="objective-img" alt="" />
                        <img src="" img-url={objective_2} className="objective-img" alt="" />
                        <img src="" img-url={objective_3} className="objective-img" alt="" />
                        <img src="" img-url={objective_4} className="objective-img" alt="" />
                        <img src="" img-url={objective_5} className="objective-img" alt="" />
                        <img src="" img-url={objective_6} className="objective-img" alt="" />
                        <img src="" img-url={objective_7} className="objective-img" alt="" />
                        <img src="" img-url={objective_8} className="objective-img" alt="" />
                        <img src="" img-url={objective_9} className="objective-img" alt="" />
                        <img src="" img-url={objective_10} className="objective-img" alt="" />
                        <img src="" img-url={objective_11} className="objective-img" alt="" />
                        <img src="" img-url={objective_12} className="objective-img" alt="" />
                        <img src="" img-url={objective_13} className="objective-img" alt="" />
                        <img src="" img-url={objective_14} className="objective-img" alt="" />
                        <img src="" img-url={objective_15} className="objective-img" alt="" />
                        <img src="" img-url={objective_16} className="objective-img" alt="" />
                        <img src="" img-url={objective_17} className="objective-img" alt="" />
                        <img src="" img-url={objective_18} className="objective-img" alt="" />
                        <img src="" img-url={objective_19} className="objective-img" alt="" />
                        <img src="" img-url={objective_20} className="objective-img" alt="" />
                        <img src="" img-url={objective_21} className="objective-img" alt="" />
                        <img src="" img-url={objective_22} className="objective-img" alt="" />
                        <img src="" img-url={objective_23} className="objective-img" alt="" />
                        <img src="" img-url={objective_24} className="objective-img" alt="" />
                    </div>
                </div>
            </div>
        </div>
    );
  }
}


export default Fold3;
