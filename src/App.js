import React, { Component } from 'react';
import './App.css';
import $ from 'jquery'

import Header from './header';
import Menanimation from './men_animation';
import Fold1 from './fold_1';
import Fold2 from './fold_2';
import Fold3 from './fold_3';
import Fold4 from './fold_4';
import Fold5 from './fold_5';
import Fold6 from './fold_6';
import Fold7 from './fold_7';
import Fold8 from './fold_8';
import Footer from './footer';
import Popup from './popup'

class App extends Component {
  render() {
    return (
      <div >
        <Header />
		<Menanimation />
		<Fold1 />
		<Fold2 />
		<Fold3 />
		<Fold4 />
		<Fold5 />
		<Fold6 />
		<Fold7 />
		<Fold8 />
		<Footer />
		<Popup />
	  </div>
    );
  }
  componentDidMount() {
		var winHeight = 0;
		var adjustHeight = 0;
		var maxHeight = 0;
		var menOffsetTop = 0;
		
		var fold_2_start = 0;
		var fold_3_start = 0;
		var fold_4_start = 0;
		var fold_5_start = 0;
		var fold_6_start = 0;
		var fold_7_start = 0;
		var fold_8_start = 0;
		$(window).on('load', function () {
			$(window).scrollTop(0,0);
			setTimeout(function(){
				$('.loader').fadeOut(500);	
				if($(window).width() > 1024){
					setPosition();
					pageScroll();
				}
				
			}, 2000);
			if($(window).width() > 1024){
				loadAllImages();
				setPosition();
			}
			if(navigator.userAgent.indexOf('Mac') > 0){
				$('body').addClass('mac-os');
			}
			
			$('.menu-toggle').click(function(e) {
				if(!$(this).hasClass('toggled-on')){
					$(this).addClass('toggled-on');	
					$('header nav').addClass('active');	
				}else {
					$(this).removeClass('toggled-on');	
					$('header nav').removeClass('active');	
				}
			});
			$('.scroll-link').click(function(e) {
				var body = $("html, body");
				body.stop().animate({scrollTop:(fold_2_start+($('.fold').eq(1).height()/4))}, 1000, 'swing', function() { });
			});
			
			
			$('.learn-more-link').click(function(e) {
				e.preventDefault();
				var relElem = $(this).attr('rel');
				$('.popup-mask').fadeIn(1000,function(){
					$('.popup-mask').css({'display':'flex'});
					$('#'+relElem).fadeIn(100);
				});
			});
			$('.popup-close').click(function(e) {
				$('.popup-mask').fadeOut(1000,function(){
					$('.popup-inner-wrap').fadeOut(100);
				});
			});
			
		
			
		});
		
		
		$(window).on('scroll', function () {
			if($(window).width() > 1024){
				pageScroll();
			}
			
		});
		$(window).on('resize', function () {
			if($(window).width() > 1024){
				setPosition();
			}
		});
		function loadAllImages(){
			$('img').each(function(index, element) {
				if($(this).attr('img-url')){
					$(this).attr('src',	$(this).attr('img-url'));
				}
			});	
		}
		function setPosition(){
			winHeight = $('.fold').eq(1).height()+(($('.fold').eq(1).height()-$(window).height())/2);
			adjustHeight = ($('.fold').eq(1).height()-$(window).height())/2;
			maxHeight = $("#fold_8").offset().top+adjustHeight+10;
			
			
			$('.men-animation').css({'left':$('.currency').offset().left+100});
			menOffsetTop = $('.men-animation').position().top;
			
			fold_2_start = Math.round(winHeight/2);
			fold_3_start = Math.round((winHeight/2)+$('.fold').eq(1).height());
			fold_4_start = Math.round((winHeight/2)+($('.fold').eq(1).height()*2));
			fold_5_start = Math.round((winHeight/2)+($('.fold').eq(1).height()*3));
			fold_6_start = Math.round((winHeight/2)+($('.fold').eq(1).height()*4));
			fold_7_start = Math.round((winHeight/2)+($('.fold').eq(1).height()*5));
			fold_8_start = Math.round((winHeight/2)+($('.fold').eq(1).height()*6));
			
		}
		function pageScroll(){
			
			if($(window).scrollTop() === 0){
				$('.fold-1, .fold-2').removeClass('active');
			}
			
			if($(window).scrollTop() <= 0){
				$('.fold-1').removeClass('active');
			}
			if($(window).scrollTop() >= 0 && $(window).scrollTop() <= 20){
				$('.fold-1').removeClass('active');
				$('.fold-1').eq(0).addClass('active');
			}
			if($(window).scrollTop() >= 21 && $(window).scrollTop() <= 40){
				$('.fold-1').removeClass('active');
				$('.fold-1').eq(1).addClass('active');
			}
			if($(window).scrollTop() >= 41 && $(window).scrollTop() <= 60){
				$('.fold-1').removeClass('active');
				$('.fold-1').eq(2).addClass('active');
			}
			if($(window).scrollTop() >= 61 && $(window).scrollTop() <= 80){
				$('.fold-1').removeClass('active');
				$('.fold-1').eq(3).addClass('active');
			}
			if($(window).scrollTop() >= 81 && $(window).scrollTop() <= 100){
				$('.fold-1').removeClass('active');
				$('.fold-1').eq(4).addClass('active');
			}
			if($(window).scrollTop() >= 101 && $(window).scrollTop() <= 120){
				$('.fold-1').removeClass('active');
				$('.fold-1').eq(5).addClass('active');
			}
			if($(window).scrollTop() >= 121 && $(window).scrollTop() <= 140){
				$('.fold-1').removeClass('active');
				$('.fold-1').eq(6).addClass('active');
			}
			if($(window).scrollTop() >= 141 && $(window).scrollTop() <= 160){
				$('.fold-1').removeClass('active');
				$('.fold-1').eq(7).addClass('active');
			}
			if($(window).scrollTop() >= 161 && $(window).scrollTop() <= 180){
				$('.fold-1').removeClass('active');
				$('.fold-1').eq(8).addClass('active');
			}
			if($(window).scrollTop() >= 181 && $(window).scrollTop() <= 200){
				$('.fold-1').removeClass('active');
				$('.fold-1').eq(9).addClass('active');
			}
			if($(window).scrollTop() >= 201 && $(window).scrollTop() <= 220){
				$('.fold-1').removeClass('active');
				$('.fold-1').eq(10).addClass('active');
			}
			if($(window).scrollTop() >= 221 && $(window).scrollTop() <= 240){
				$('.fold-1').removeClass('active');
				$('.fold-1').eq(11).addClass('active');
			}
			
			if($(window).scrollTop() <= 0 ){
				$('.men-animation').css({'-moz-transform': 'scale(.4)','-webkit-transform': 'scale(.4)','-o-transform': 'scale(.4)','-ms-transform': 'scale(.4)','transform': 'scale(.4)'});	
			}
			if($(window).scrollTop() >= 0 && $(window).scrollTop() <= 30){
				$('.men-animation').css({'-moz-transform': 'scale(.4)','-webkit-transform': 'scale(.4)','-o-transform': 'scale(.4)','-ms-transform': 'scale(.4)','transform': 'scale(.4)'});	
			}
			if($(window).scrollTop() >= 31 && $(window).scrollTop() <= 50){
				$('.men-animation').css({'-moz-transform': 'scale(.425)','-webkit-transform': 'scale(.425)','-o-transform': 'scale(.425)','-ms-transform': 'scale(.425)','transform': 'scale(.425)'});	
			}
			if($(window).scrollTop() >= 51 && $(window).scrollTop() <= 70){
				$('.men-animation').css({'-moz-transform': 'scale(.45)','-webkit-transform': 'scale(.45)','-o-transform': 'scale(.45)','-ms-transform': 'scale(.45)','transform': 'scale(.45)'});	
			}
			if($(window).scrollTop() >= 71 && $(window).scrollTop() <= 90){
				$('.men-animation').css({'-moz-transform': 'scale(.475)','-webkit-transform': 'scale(.475)','-o-transform': 'scale(.475)','-ms-transform': 'scale(.475)','transform': 'scale(.475)'});	
			}
			if($(window).scrollTop() >= 91 && $(window).scrollTop() <= 110){
				$('.men-animation').css({'-moz-transform': 'scale(.5)','-webkit-transform': 'scale(.5)','-o-transform': 'scale(.5)','-ms-transform': 'scale(.5)','transform': 'scale(.5)'});	
			}
			if($(window).scrollTop() >= 111 && $(window).scrollTop() <= 130){
				$('.men-animation').css({'-moz-transform': 'scale(.525)','-webkit-transform': 'scale(.525)','-o-transform': 'scale(.525)','-ms-transform': 'scale(.525)','transform': 'scale(.525)'});	
			}
			if($(window).scrollTop() >= 131 && $(window).scrollTop() <= 150){
				$('.men-animation').css({'-moz-transform': 'scale(.55)','-webkit-transform': 'scale(.55)','-o-transform': 'scale(.55)','-ms-transform': 'scale(.55)','transform': 'scale(.55)'});	
			}
			if($(window).scrollTop() >= 151 && $(window).scrollTop() <= 170){
				$('.men-animation').css({'-moz-transform': 'scale(.575)','-webkit-transform': 'scale(.575)','-o-transform': 'scale(.575)','-ms-transform': 'scale(.575)','transform': 'scale(.575)'});	
			}
			if($(window).scrollTop() >= 171 && $(window).scrollTop() <= 190){
				$('.men-animation').css({'-moz-transform': 'scale(.6)','-webkit-transform': 'scale(.6)','-o-transform': 'scale(.6)','-ms-transform': 'scale(.6)','transform': 'scale(.6)'});	
			}
			if($(window).scrollTop() >= 191 && $(window).scrollTop() <= 210){
				$('.men-animation').css({'-moz-transform': 'scale(.625)','-webkit-transform': 'scale(.625)','-o-transform': 'scale(.625)','-ms-transform': 'scale(.625)','transform': 'scale(.625)'});	
			}
			if($(window).scrollTop() >= 211 && $(window).scrollTop() <= 230){
				$('.men-animation').css({'-moz-transform': 'scale(.65)','-webkit-transform': 'scale(.65)','-o-transform': 'scale(.65)','-ms-transform': 'scale(.65)','transform': 'scale(.65)'});	
			}
			if($(window).scrollTop() >= 231 && $(window).scrollTop() <= 250){
				$('.men-animation').css({'-moz-transform': 'scale(.675)','-webkit-transform': 'scale(.675)','-o-transform': 'scale(.675)','-ms-transform': 'scale(.675)','transform': 'scale(.675)'});	
			}
			if($(window).scrollTop() >= 251 && $(window).scrollTop() <= 270){
				$('.men-animation').css({'-moz-transform': 'scale(.7)','-webkit-transform': 'scale(.7)','-o-transform': 'scale(.7)','-ms-transform': 'scale(.7)','transform': 'scale(.7)'});	
			}
			if($(window).scrollTop() >= 271 && $(window).scrollTop() <= 290){
				$('.men-animation').css({'-moz-transform': 'scale(.725)','-webkit-transform': 'scale(.725)','-o-transform': 'scale(.725)','-ms-transform': 'scale(.725)','transform': 'scale(.725)'});	
			}
			if($(window).scrollTop() >= 291 && $(window).scrollTop() <= 310){
				$('.men-animation').css({'-moz-transform': 'scale(.75)','-webkit-transform': 'scale(.75)','-o-transform': 'scale(.75)','-ms-transform': 'scale(.75)','transform': 'scale(.75)'});	
			}
			if($(window).scrollTop() >= 311 && $(window).scrollTop() <= 330){
				$('.men-animation').css({'-moz-transform': 'scale(.775)','-webkit-transform': 'scale(.775)','-o-transform': 'scale(.775)','-ms-transform': 'scale(.775)','transform': 'scale(.775)'});	
			}
			if($(window).scrollTop() >= 331 && $(window).scrollTop() <= 350){
				$('.men-animation').css({'-moz-transform': 'scale(.8)','-webkit-transform': 'scale(.8)','-o-transform': 'scale(.8)','-ms-transform': 'scale(.8)','transform': 'scale(.8)'});	
			}
			if($(window).scrollTop() >= 351 && $(window).scrollTop() <= 370){
				$('.men-animation').css({'-moz-transform': 'scale(.825)','-webkit-transform': 'scale(.825)','-o-transform': 'scale(.825)','-ms-transform': 'scale(.825)','transform': 'scale(.825)'});	
			}
			if($(window).scrollTop() >= 371 && $(window).scrollTop() <= 390){
				$('.men-animation').css({'-moz-transform': 'scale(.85)','-webkit-transform': 'scale(.85)','-o-transform': 'scale(.85)','-ms-transform': 'scale(.85)','transform': 'scale(.85)'});	
			}
			if($(window).scrollTop() >= 391 && $(window).scrollTop() <= 410){
				$('.men-animation').css({'-moz-transform': 'scale(.875)','-webkit-transform': 'scale(.875)','-o-transform': 'scale(.875)','-ms-transform': 'scale(.875)','transform': 'scale(.875)'});	
			}
			if($(window).scrollTop() >= 411 && $(window).scrollTop() <= 430){
				$('.men-animation').css({'-moz-transform': 'scale(.9)','-webkit-transform': 'scale(.9)','-o-transform': 'scale(.9)','-ms-transform': 'scale(.9)','transform': 'scale(.9)'});	
			}
			if($(window).scrollTop() >= 431 && $(window).scrollTop() <= 450){
				$('.men-animation').css({'-moz-transform': 'scale(.925)','-webkit-transform': 'scale(.925)','-o-transform': 'scale(.925)','-ms-transform': 'scale(.925)','transform': 'scale(.925)'});	
			}
			if($(window).scrollTop() >= 451 && $(window).scrollTop() <= 470){
				$('.men-animation').css({'-moz-transform': 'scale(.95)','-webkit-transform': 'scale(.95)','-o-transform': 'scale(.95)','-ms-transform': 'scale(.95)','transform': 'scale(.95)'});	
			}
			if($(window).scrollTop() >= 471 && $(window).scrollTop() <= 490){
				$('.men-animation').css({'-moz-transform': 'scale(.975)','-webkit-transform': 'scale(.975)','-o-transform': 'scale(.975)','-ms-transform': 'scale(.975)','transform': 'scale(.975)'});	
			}
			if($(window).scrollTop() >= 491 && $(window).scrollTop() <= 510){
				$('.men-animation').css({'-moz-transform': 'scale(1)','-webkit-transform': 'scale(1)','-o-transform': 'scale(1)','-ms-transform': 'scale(1)','transform': 'scale(1)'});	
			}
			if($(window).scrollTop() >= 510){
				$('.men-animation').css({'-moz-transform': 'scale(1)','-webkit-transform': 'scale(1)','-o-transform': 'scale(1)','-ms-transform': 'scale(1)','transform': 'scale(1)'});	
			}
			
			
			if($(window).scrollTop() >= maxHeight){
				//$('.men-animation').addClass('remove-freez').css({'top':(maxHeight+menOffsetTop)});
				$('.fold-9').css({'visibility':'visible'});
				$('.men-animation').hide();
			}else {
				$('.fold-9').css({'visibility':'hidden'});
				$('.men-animation').show();
				//$('.men-animation').removeClass('remove-freez').css({'top':''});
			}
			
			fold_2_anim();
			fold_3_anim();
			fold_4_anim();
			fold_5_anim();
			fold_6_anim();
			fold_7_anim();
			fold_8_anim();
		}
		function fold_2_anim() {
			
			if($(window).scrollTop() <= fold_2_start+100){
				$('.fold-2').removeClass('active');
			}
			if($(window).scrollTop() >= fold_2_start+100 && $(window).scrollTop() <= fold_2_start+125){
				$('.fold-2').removeClass('active');
				$('.fold-2').eq(0).addClass('active');
			}
			if($(window).scrollTop() >= fold_2_start+126 && $(window).scrollTop() <= fold_2_start+150){
				$('.fold-2').removeClass('active');
				$('.fold-2').eq(1).addClass('active');
			}
			if($(window).scrollTop() >= fold_2_start+151 && $(window).scrollTop() <= fold_2_start+175){
				$('.fold-2').removeClass('active');
				$('.fold-2').eq(2).addClass('active');
			}
			if($(window).scrollTop() >= fold_2_start+176 && $(window).scrollTop() <= fold_2_start+200){
				$('.fold-2').removeClass('active');
				$('.fold-2').eq(3).addClass('active');
			}
			if($(window).scrollTop() >= fold_2_start+201 && $(window).scrollTop() <= fold_2_start+225){
				$('.fold-2').removeClass('active');
				$('.fold-2').eq(4).addClass('active');
			}
			if($(window).scrollTop() >= fold_2_start+226 && $(window).scrollTop() <= fold_2_start+250){
				$('.fold-2').removeClass('active');
				$('.fold-2').eq(5).addClass('active');
			}
			if($(window).scrollTop() >= fold_2_start+251 && $(window).scrollTop() <= fold_2_start+275){
				$('.fold-2').removeClass('active');
				$('.fold-2').eq(6).addClass('active');
			}
			if($(window).scrollTop() >= fold_2_start+276 && $(window).scrollTop() <= fold_2_start+300){
				$('.fold-2').removeClass('active');
				$('.fold-2').eq(7).addClass('active');
			}
			if($(window).scrollTop() >= fold_2_start+301 && $(window).scrollTop() <= fold_2_start+325){
				$('.fold-2').removeClass('active');
				$('.fold-2').eq(8).addClass('active');
			}
			if($(window).scrollTop() >= fold_2_start+326 && $(window).scrollTop() <= fold_2_start+350){
				$('.fold-2').removeClass('active');
				$('.fold-2').eq(9).addClass('active');
			}
		
			
			
			
			// FLIGHT
			if($(window).scrollTop() >= fold_2_start && $(window).scrollTop() <= fold_2_start+50){
				$('.flight').css({'left':0});
			}
			if($(window).scrollTop() >= fold_2_start+51 && $(window).scrollTop() <= fold_2_start+100){
				$('.flight').css({'left':100});
			}
			if($(window).scrollTop() >= fold_2_start+101 && $(window).scrollTop() <= fold_2_start+150){
				$('.flight').css({'left':200});
			}
			if($(window).scrollTop() >= fold_2_start+151 && $(window).scrollTop() <= fold_2_start+200){
				$('.flight').css({'left':300});
			}
			if($(window).scrollTop() >= fold_2_start+201 && $(window).scrollTop() <= fold_2_start+250){
				$('.flight').css({'left':400});
			}
			if($(window).scrollTop() >= fold_2_start+251 && $(window).scrollTop() <= fold_2_start+300){
				$('.flight').css({'left':550});
			}
			if($(window).scrollTop() >= fold_2_start+301 && $(window).scrollTop() <= fold_2_start+350){
				$('.flight').css({'left':750});
			}
			if($(window).scrollTop() >= fold_2_start+351 && $(window).scrollTop() <= fold_2_start+400){
				$('.flight').css({'left':850});
			}
			if($(window).scrollTop() >= fold_2_start+401){
				$('.flight').css({'left':850});
			}
		}
		function fold_3_anim() {
			
				
			if($(window).scrollTop() <= fold_3_start){
				$('.fold-3').removeClass('active');
			}
			if($(window).scrollTop() >= fold_3_start && $(window).scrollTop() <= fold_3_start+25){
				$('.fold-3').removeClass('active');
				$('.fold-3').eq(0).addClass('active');
			}
			if($(window).scrollTop() >= fold_3_start+26 && $(window).scrollTop() <= fold_3_start+50){
				$('.fold-3').removeClass('active');
				$('.fold-3').eq(1).addClass('active');
			}
			if($(window).scrollTop() >= fold_3_start+51 && $(window).scrollTop() <= fold_3_start+75){
				$('.fold-3').removeClass('active');
				$('.fold-3').eq(2).addClass('active');
			}
			if($(window).scrollTop() >= fold_3_start+76 && $(window).scrollTop() <= fold_3_start+100){
				$('.fold-3').removeClass('active');
				$('.fold-3').eq(3).addClass('active');
			}
			if($(window).scrollTop() >= fold_3_start+101 && $(window).scrollTop() <= fold_3_start+125){
				$('.fold-3').removeClass('active');
				$('.fold-3').eq(4).addClass('active');
			}
			if($(window).scrollTop() >= fold_3_start+126 && $(window).scrollTop() <= fold_3_start+150){
				$('.fold-3').removeClass('active');
				$('.fold-3').eq(5).addClass('active');
			}
			if($(window).scrollTop() >= fold_3_start+151 && $(window).scrollTop() <= fold_3_start+175){
				$('.fold-3').removeClass('active');
				$('.fold-3').eq(6).addClass('active');
			}
			if($(window).scrollTop() >= fold_3_start+176 && $(window).scrollTop() <= fold_3_start+200){
				$('.fold-3').removeClass('active');
				$('.fold-3').eq(7).addClass('active');
			}
			if($(window).scrollTop() >= fold_3_start+201 && $(window).scrollTop() <= fold_3_start+225){
				$('.fold-3').removeClass('active');
				$('.fold-3').eq(8).addClass('active');
			}
			if($(window).scrollTop() >= fold_3_start+226 && $(window).scrollTop() <= fold_3_start+250){
				$('.fold-3').removeClass('active');
				$('.fold-3').eq(9).addClass('active');
			}
			if($(window).scrollTop() >= fold_3_start+251 && $(window).scrollTop() <= fold_3_start+275){
				$('.fold-3').removeClass('active');
				$('.fold-3').eq(10).addClass('active');
			}
			if($(window).scrollTop() >= fold_3_start+276 && $(window).scrollTop() <= fold_3_start+300){
				$('.fold-3').removeClass('active');
				$('.fold-3').eq(11).addClass('active');
			}
			if($(window).scrollTop() >= fold_3_start+301 && $(window).scrollTop() <= fold_3_start+325){
				$('.fold-3').removeClass('active');
				$('.fold-3').eq(12).addClass('active');
			}
			if($(window).scrollTop() >= fold_3_start+326 && $(window).scrollTop() <= fold_3_start+350){
				$('.fold-3').removeClass('active');
				$('.fold-3').eq(13).addClass('active');
			}
			if($(window).scrollTop() >= fold_3_start+351 && $(window).scrollTop() <= fold_3_start+375){
				$('.fold-3').removeClass('active');
				$('.fold-3').eq(14).addClass('active');
			}
			if($(window).scrollTop() >= fold_3_start+376 && $(window).scrollTop() <= fold_3_start+400){
				$('.fold-3').removeClass('active');
				$('.fold-3').eq(15).addClass('active');
			}
			if($(window).scrollTop() >= fold_3_start+401 && $(window).scrollTop() <= fold_3_start+425){
				$('.fold-3').removeClass('active');
				$('.fold-3').eq(16).addClass('active');
			}
			if($(window).scrollTop() >= fold_3_start+426 && $(window).scrollTop() <= fold_3_start+450){
				$('.fold-3').removeClass('active');
				$('.fold-3').eq(17).addClass('active');
			}
			if($(window).scrollTop() >= fold_3_start+451 && $(window).scrollTop() <= fold_3_start+475){
				$('.fold-3').removeClass('active');
				$('.fold-3').eq(18).addClass('active');
			}
			if($(window).scrollTop() >= fold_3_start+476 && $(window).scrollTop() <= fold_3_start+500){
				$('.fold-3').removeClass('active');
				$('.fold-3').eq(19).addClass('active');
			}
		
			
			// COMFORT
			if($(window).scrollTop() <= fold_3_start){
				$('.comfort-img').removeClass('active');
				$('.comfort-img').eq(0).addClass('active');
			}
			if($(window).scrollTop() >= fold_3_start && $(window).scrollTop() <= fold_3_start+15){
				$('.comfort-img').removeClass('active');
				$('.comfort-img').eq(0).addClass('active');
			}
			if($(window).scrollTop() >= fold_3_start+16 && $(window).scrollTop() <= fold_3_start+30){
				$('.comfort-img').removeClass('active');
				$('.comfort-img').eq(1).addClass('active');
			}
			if($(window).scrollTop() >= fold_3_start+31 && $(window).scrollTop() <= fold_3_start+45){
				$('.comfort-img').removeClass('active');
				$('.comfort-img').eq(2).addClass('active');
			}
			if($(window).scrollTop() >= fold_3_start+46 && $(window).scrollTop() <= fold_3_start+60){
				$('.comfort-img').removeClass('active');
				$('.comfort-img').eq(3).addClass('active');
			}
			if($(window).scrollTop() >= fold_3_start+61 && $(window).scrollTop() <= fold_3_start+75){
				$('.comfort-img').removeClass('active');
				$('.comfort-img').eq(4).addClass('active');
			}
			if($(window).scrollTop() >= fold_3_start+76 && $(window).scrollTop() <= fold_3_start+90){
				$('.comfort-img').removeClass('active');
				$('.comfort-img').eq(5).addClass('active');
			}
			if($(window).scrollTop() >= fold_3_start+91 && $(window).scrollTop() <= fold_3_start+105){
				$('.comfort-img').removeClass('active');
				$('.comfort-img').eq(6).addClass('active');
			}
			if($(window).scrollTop() >= fold_3_start+106 && $(window).scrollTop() <= fold_3_start+120){
				$('.comfort-img').removeClass('active');
				$('.comfort-img').eq(7).addClass('active');
			}
			if($(window).scrollTop() >= fold_3_start+121 && $(window).scrollTop() <= fold_3_start+135){
				$('.comfort-img').removeClass('active');
				$('.comfort-img').eq(8).addClass('active');
			}
			if($(window).scrollTop() >= fold_3_start+136 && $(window).scrollTop() <= fold_3_start+150){
				$('.comfort-img').removeClass('active');
				$('.comfort-img').eq(9).addClass('active');
			}
			if($(window).scrollTop() >= fold_3_start+151 && $(window).scrollTop() <= fold_3_start+165){
				$('.comfort-img').removeClass('active');
				$('.comfort-img').eq(10).addClass('active');
			}
			if($(window).scrollTop() >= fold_3_start+166 && $(window).scrollTop() <= fold_3_start+180){
				$('.comfort-img').removeClass('active');
				$('.comfort-img').eq(11).addClass('active');
			}
			if($(window).scrollTop() >= fold_3_start+181 && $(window).scrollTop() <= fold_3_start+195){
				$('.comfort-img').removeClass('active');
				$('.comfort-img').eq(12).addClass('active');
			}
			if($(window).scrollTop() >= fold_3_start+196 && $(window).scrollTop() <= fold_3_start+210){
				$('.comfort-img').removeClass('active');
				$('.comfort-img').eq(13).addClass('active');
			}
			if($(window).scrollTop() >= fold_3_start+211 && $(window).scrollTop() <= fold_3_start+225){
				$('.comfort-img').removeClass('active');
				$('.comfort-img').eq(14).addClass('active');
			}
			if($(window).scrollTop() >= fold_3_start+226 && $(window).scrollTop() <= fold_3_start+240){
				$('.comfort-img').removeClass('active');
				$('.comfort-img').eq(15).addClass('active');
			}
			if($(window).scrollTop() >= fold_3_start+241 && $(window).scrollTop() <= fold_3_start+255){
				$('.comfort-img').removeClass('active');
				$('.comfort-img').eq(16).addClass('active');
			}
			if($(window).scrollTop() >= fold_3_start+256 && $(window).scrollTop() <= fold_3_start+270){
				$('.comfort-img').removeClass('active');
				$('.comfort-img').eq(17).addClass('active');
			}
			if($(window).scrollTop() >= fold_3_start+271){
				$('.comfort-img').removeClass('active');
				$('.comfort-img').eq(17).addClass('active');
			}
			
			
			// TIME
			if($(window).scrollTop() <= fold_3_start){
				$('.time-img').removeClass('active');
				$('.time-img').eq(0).addClass('active');
			}
			if($(window).scrollTop() >= fold_3_start && $(window).scrollTop() <= fold_3_start+15){
				$('.time-img').removeClass('active');
				$('.time-img').eq(0).addClass('active');
			}
			if($(window).scrollTop() >= fold_3_start+16 && $(window).scrollTop() <= fold_3_start+30){
				$('.time-img').removeClass('active');
				$('.time-img').eq(1).addClass('active');
			}
			if($(window).scrollTop() >= fold_3_start+31 && $(window).scrollTop() <= fold_3_start+45){
				$('.time-img').removeClass('active');
				$('.time-img').eq(2).addClass('active');
			}
			if($(window).scrollTop() >= fold_3_start+46 && $(window).scrollTop() <= fold_3_start+60){
				$('.time-img').removeClass('active');
				$('.time-img').eq(3).addClass('active');
			}
			if($(window).scrollTop() >= fold_3_start+61 && $(window).scrollTop() <= fold_3_start+75){
				$('.time-img').removeClass('active');
				$('.time-img').eq(4).addClass('active');
			}
			if($(window).scrollTop() >= fold_3_start+76 && $(window).scrollTop() <= fold_3_start+90){
				$('.time-img').removeClass('active');
				$('.time-img').eq(5).addClass('active');
			}
			if($(window).scrollTop() >= fold_3_start+91 && $(window).scrollTop() <= fold_3_start+105){
				$('.time-img').removeClass('active');
				$('.time-img').eq(6).addClass('active');
			}
			if($(window).scrollTop() >= fold_3_start+106 && $(window).scrollTop() <= fold_3_start+120){
				$('.time-img').removeClass('active');
				$('.time-img').eq(7).addClass('active');
			}
			if($(window).scrollTop() >= fold_3_start+121 && $(window).scrollTop() <= fold_3_start+135){
				$('.time-img').removeClass('active');
				$('.time-img').eq(8).addClass('active');
			}
			if($(window).scrollTop() >= fold_3_start+136 && $(window).scrollTop() <= fold_3_start+150){
				$('.time-img').removeClass('active');
				$('.time-img').eq(9).addClass('active');
			}
			if($(window).scrollTop() >= fold_3_start+151 && $(window).scrollTop() <= fold_3_start+165){
				$('.time-img').removeClass('active');
				$('.time-img').eq(10).addClass('active');
			}
			if($(window).scrollTop() >= fold_3_start+166 && $(window).scrollTop() <= fold_3_start+180){
				$('.time-img').removeClass('active');
				$('.time-img').eq(11).addClass('active');
			}
			if($(window).scrollTop() >= fold_3_start+181 && $(window).scrollTop() <= fold_3_start+195){
				$('.time-img').removeClass('active');
				$('.time-img').eq(12).addClass('active');
			}
			if($(window).scrollTop() >= fold_3_start+196 && $(window).scrollTop() <= fold_3_start+210){
				$('.time-img').removeClass('active');
				$('.time-img').eq(13).addClass('active');
			}
			if($(window).scrollTop() >= fold_3_start+211 && $(window).scrollTop() <= fold_3_start+225){
				$('.time-img').removeClass('active');
				$('.time-img').eq(14).addClass('active');
			}
			if($(window).scrollTop() >= fold_3_start+226 && $(window).scrollTop() <= fold_3_start+240){
				$('.time-img').removeClass('active');
				$('.time-img').eq(15).addClass('active');
			}
			if($(window).scrollTop() >= fold_3_start+241 && $(window).scrollTop() <= fold_3_start+255){
				$('.time-img').removeClass('active');
				$('.time-img').eq(16).addClass('active');
			}
			if($(window).scrollTop() >= fold_3_start+256 && $(window).scrollTop() <= fold_3_start+270){
				$('.time-img').removeClass('active');
				$('.time-img').eq(17).addClass('active');
			}
			if($(window).scrollTop() >= fold_3_start+271 && $(window).scrollTop() <= fold_3_start+285){
				$('.time-img').removeClass('active');
				$('.time-img').eq(18).addClass('active');
			}
			if($(window).scrollTop() >= fold_3_start+286 && $(window).scrollTop() <= fold_3_start+300){
				$('.time-img').removeClass('active');
				$('.time-img').eq(19).addClass('active');
			}
			if($(window).scrollTop() >= fold_3_start+301){
				$('.time-img').removeClass('active');
				$('.time-img').eq(19).addClass('active');
			}
			
			
			// OBJECTIVE
			if($(window).scrollTop() <= fold_3_start){
				$('.objective-img').removeClass('active');
				$('.objective-img').eq(0).addClass('active');
			}
			if($(window).scrollTop() >= fold_3_start && $(window).scrollTop() <= fold_3_start+15){
				$('.objective-img').removeClass('active');
				$('.objective-img').eq(0).addClass('active');
			}
			if($(window).scrollTop() >= fold_3_start+16 && $(window).scrollTop() <= fold_3_start+30){
				$('.objective-img').removeClass('active');
				$('.objective-img').eq(1).addClass('active');
			}
			if($(window).scrollTop() >= fold_3_start+31 && $(window).scrollTop() <= fold_3_start+45){
				$('.objective-img').removeClass('active');
				$('.objective-img').eq(2).addClass('active');
			}
			if($(window).scrollTop() >= fold_3_start+46 && $(window).scrollTop() <= fold_3_start+60){
				$('.objective-img').removeClass('active');
				$('.objective-img').eq(3).addClass('active');
			}
			if($(window).scrollTop() >= fold_3_start+61 && $(window).scrollTop() <= fold_3_start+75){
				$('.objective-img').removeClass('active');
				$('.objective-img').eq(4).addClass('active');
			}
			if($(window).scrollTop() >= fold_3_start+76 && $(window).scrollTop() <= fold_3_start+90){
				$('.objective-img').removeClass('active');
				$('.objective-img').eq(5).addClass('active');
			}
			if($(window).scrollTop() >= fold_3_start+91 && $(window).scrollTop() <= fold_3_start+105){
				$('.objective-img').removeClass('active');
				$('.objective-img').eq(6).addClass('active');
			}
			if($(window).scrollTop() >= fold_3_start+106 && $(window).scrollTop() <= fold_3_start+120){
				$('.objective-img').removeClass('active');
				$('.objective-img').eq(7).addClass('active');
			}
			if($(window).scrollTop() >= fold_3_start+121 && $(window).scrollTop() <= fold_3_start+135){
				$('.objective-img').removeClass('active');
				$('.objective-img').eq(8).addClass('active');
			}
			if($(window).scrollTop() >= fold_3_start+136 && $(window).scrollTop() <= fold_3_start+150){
				$('.objective-img').removeClass('active');
				$('.objective-img').eq(9).addClass('active');
			}
			if($(window).scrollTop() >= fold_3_start+151 && $(window).scrollTop() <= fold_3_start+165){
				$('.objective-img').removeClass('active');
				$('.objective-img').eq(10).addClass('active');
			}
			if($(window).scrollTop() >= fold_3_start+166 && $(window).scrollTop() <= fold_3_start+180){
				$('.objective-img').removeClass('active');
				$('.objective-img').eq(11).addClass('active');
			}
			if($(window).scrollTop() >= fold_3_start+181 && $(window).scrollTop() <= fold_3_start+195){
				$('.objective-img').removeClass('active');
				$('.objective-img').eq(12).addClass('active');
			}
			if($(window).scrollTop() >= fold_3_start+196 && $(window).scrollTop() <= fold_3_start+210){
				$('.objective-img').removeClass('active');
				$('.objective-img').eq(13).addClass('active');
			}
			if($(window).scrollTop() >= fold_3_start+211 && $(window).scrollTop() <= fold_3_start+225){
				$('.objective-img').removeClass('active');
				$('.objective-img').eq(14).addClass('active');
			}
			if($(window).scrollTop() >= fold_3_start+226 && $(window).scrollTop() <= fold_3_start+240){
				$('.objective-img').removeClass('active');
				$('.objective-img').eq(15).addClass('active');
			}
			if($(window).scrollTop() >= fold_3_start+241 && $(window).scrollTop() <= fold_3_start+255){
				$('.objective-img').removeClass('active');
				$('.objective-img').eq(16).addClass('active');
			}
			if($(window).scrollTop() >= fold_3_start+256 && $(window).scrollTop() <= fold_3_start+270){
				$('.objective-img').removeClass('active');
				$('.objective-img').eq(17).addClass('active');
			}
			if($(window).scrollTop() >= fold_3_start+271 && $(window).scrollTop() <= fold_3_start+285){
				$('.objective-img').removeClass('active');
				$('.objective-img').eq(18).addClass('active');
			}
			if($(window).scrollTop() >= fold_3_start+286 && $(window).scrollTop() <= fold_3_start+300){
				$('.objective-img').removeClass('active');
				$('.objective-img').eq(19).addClass('active');
			}
			if($(window).scrollTop() >= fold_3_start+301 && $(window).scrollTop() <= fold_3_start+315){
				$('.objective-img').removeClass('active');
				$('.objective-img').eq(20).addClass('active');
			}
			if($(window).scrollTop() >= fold_3_start+316 && $(window).scrollTop() <= fold_3_start+330){
				$('.objective-img').removeClass('active');
				$('.objective-img').eq(21).addClass('active');
			}
			if($(window).scrollTop() >= fold_3_start+331 && $(window).scrollTop() <= fold_3_start+345){
				$('.objective-img').removeClass('active');
				$('.objective-img').eq(22).addClass('active');
			}
			if($(window).scrollTop() >= fold_3_start+346 && $(window).scrollTop() <= fold_3_start+360){
				$('.objective-img').removeClass('active');
				$('.objective-img').eq(23).addClass('active');
			}
			if($(window).scrollTop() >= fold_3_start+361){
				$('.objective-img').removeClass('active');
				$('.objective-img').eq(23).addClass('active');
			}
		
			
		}
		function fold_4_anim() {
			
				
			if($(window).scrollTop() <= fold_4_start){
				$('.fold-4').removeClass('active');
			}
			if($(window).scrollTop() >= fold_4_start && $(window).scrollTop() <= fold_4_start+25){
				$('.fold-4').removeClass('active');
				$('.fold-4').eq(0).addClass('active');
			}
			if($(window).scrollTop() >= fold_4_start+26 && $(window).scrollTop() <= fold_4_start+50){
				$('.fold-4').removeClass('active');
				$('.fold-4').eq(1).addClass('active');
			}
			if($(window).scrollTop() >= fold_4_start+51 && $(window).scrollTop() <= fold_4_start+75){
				$('.fold-4').removeClass('active');
				$('.fold-4').eq(2).addClass('active');
			}
			if($(window).scrollTop() >= fold_4_start+76 && $(window).scrollTop() <= fold_4_start+100){
				$('.fold-4').removeClass('active');
				$('.fold-4').eq(3).addClass('active');
			}
			if($(window).scrollTop() >= fold_4_start+101 && $(window).scrollTop() <= fold_4_start+125){
				$('.fold-4').removeClass('active');
				$('.fold-4').eq(4).addClass('active');
			}
			if($(window).scrollTop() >= fold_4_start+126 && $(window).scrollTop() <= fold_4_start+150){
				$('.fold-4').removeClass('active');
				$('.fold-4').eq(5).addClass('active');
			}
			if($(window).scrollTop() >= fold_4_start+151 && $(window).scrollTop() <= fold_4_start+175){
				$('.fold-4').removeClass('active');
				$('.fold-4').eq(6).addClass('active');
			}
			if($(window).scrollTop() >= fold_4_start+176 && $(window).scrollTop() <= fold_4_start+200){
				$('.fold-4').removeClass('active');
				$('.fold-4').eq(7).addClass('active');
			}
			if($(window).scrollTop() >= fold_4_start+201 && $(window).scrollTop() <= fold_4_start+225){
				$('.fold-4').removeClass('active');
				$('.fold-4').eq(8).addClass('active');
			}
			if($(window).scrollTop() >= fold_4_start+226 && $(window).scrollTop() <= fold_4_start+250){
				$('.fold-4').removeClass('active');
				$('.fold-4').eq(9).addClass('active');
			}
			if($(window).scrollTop() >= fold_4_start+251 && $(window).scrollTop() <= fold_4_start+275){
				$('.fold-4').removeClass('active');
				$('.fold-4').eq(10).addClass('active');
			}
			if($(window).scrollTop() >= fold_4_start+276 && $(window).scrollTop() <= fold_4_start+300){
				$('.fold-4').removeClass('active');
				$('.fold-4').eq(11).addClass('active');
			}
			
			// BUILD
			if($(window).scrollTop() >= fold_4_start && $(window).scrollTop() <= fold_4_start+50){
				$('.build-1').css({'bottom':-120});
				$('.build-3').css({'bottom':-80});
				$('.build-handle').css({'left':-100});
			}
			if($(window).scrollTop() >= fold_4_start+51 && $(window).scrollTop() <= fold_4_start+100){
				$('.build-1').css({'bottom':-100});
				$('.build-3').css({'bottom':-60});
				$('.build-handle').css({'left':0});
			}
			if($(window).scrollTop() >= fold_4_start+101 && $(window).scrollTop() <= fold_4_start+150){
				$('.build-1').css({'bottom':-80});
				$('.build-3').css({'bottom':-50});
				$('.build-handle').css({'left':100});
			}
			if($(window).scrollTop() >= fold_4_start+151 && $(window).scrollTop() <= fold_4_start+200){
				$('.build-1').css({'bottom':-60});
				$('.build-3').css({'bottom':-40});
				$('.build-handle').css({'left':150});
			}
			if($(window).scrollTop() >= fold_4_start+201 && $(window).scrollTop() <= fold_4_start+250){
				$('.build-1').css({'bottom':-40});
				$('.build-3').css({'bottom':-20});
			}
			if($(window).scrollTop() >= fold_4_start+251 && $(window).scrollTop() <= fold_4_start+300){
				$('.build-1').css({'bottom':-20});
				$('.build-3').css({'bottom':-10});
			}
			if($(window).scrollTop() >= fold_4_start+301 && $(window).scrollTop() <= fold_4_start+350){
				$('.build-1').css({'bottom':0});
				$('.build-3').css({'bottom':0});
			}
			if($(window).scrollTop() >= fold_4_start+351){
				$('.build-1').css({'bottom':0});
				$('.build-3').css({'bottom':0});
			}
			
			
			// BUILD HANDLE ANIMATION
			if($(window).scrollTop() <= fold_4_start){
				$('.build-handle-img').eq(0).addClass('active');
			}
			if($(window).scrollTop() >= fold_4_start && $(window).scrollTop() <= fold_4_start+20){
				$('.build-handle-img').removeClass('active');
				$('.build-handle-img').eq(0).addClass('active');
			}
			if($(window).scrollTop() >= fold_4_start+21 && $(window).scrollTop() <= fold_4_start+40){
				$('.build-handle-img').removeClass('active');
				$('.build-handle-img').eq(1).addClass('active');
			}
			if($(window).scrollTop() >= fold_4_start+41 && $(window).scrollTop() <= fold_4_start+60){
				$('.build-handle-img').removeClass('active');
				$('.build-handle-img').eq(2).addClass('active');
			}
			if($(window).scrollTop() >= fold_4_start+61 && $(window).scrollTop() <= fold_4_start+80){
				$('.build-handle-img').removeClass('active');
				$('.build-handle-img').eq(3).addClass('active');
			}
			if($(window).scrollTop() >= fold_4_start+81 && $(window).scrollTop() <= fold_4_start+100){
				$('.build-handle-img').removeClass('active');
				$('.build-handle-img').eq(4).addClass('active');
			}
			if($(window).scrollTop() >= fold_4_start+101 && $(window).scrollTop() <= fold_4_start+120){
				$('.build-handle-img').removeClass('active');
				$('.build-handle-img').eq(5).addClass('active');
			}
			if($(window).scrollTop() >= fold_4_start+121 && $(window).scrollTop() <= fold_4_start+140){
				$('.build-handle-img').removeClass('active');
				$('.build-handle-img').eq(6).addClass('active');
			}
			if($(window).scrollTop() >= fold_4_start+141 && $(window).scrollTop() <= fold_4_start+160){
				$('.build-handle-img').removeClass('active');
				$('.build-handle-img').eq(7).addClass('active');
			}
			if($(window).scrollTop() >= fold_4_start+161 && $(window).scrollTop() <= fold_4_start+180){
				$('.build-handle-img').removeClass('active');
				$('.build-handle-img').eq(8).addClass('active');
			}
			if($(window).scrollTop() >= fold_4_start+181 && $(window).scrollTop() <= fold_4_start+200){
				$('.build-handle-img').removeClass('active');
				$('.build-handle-img').eq(9).addClass('active');
			}
			if($(window).scrollTop() >= fold_4_start+201 && $(window).scrollTop() <= fold_4_start+220){
				$('.build-handle-img').removeClass('active');
				$('.build-handle-img').eq(10).addClass('active');
			}
			if($(window).scrollTop() >= fold_4_start+221 && $(window).scrollTop() <= fold_4_start+240){
				$('.build-handle-img').removeClass('active');
				$('.build-handle-img').eq(11).addClass('active');
			}
			if($(window).scrollTop() >= fold_4_start+241 && $(window).scrollTop() <= fold_4_start+260){
				$('.build-handle-img').removeClass('active');
				$('.build-handle-img').eq(12).addClass('active');
			}
			if($(window).scrollTop() >= fold_4_start+261 && $(window).scrollTop() <= fold_4_start+280){
				$('.build-handle-img').removeClass('active');
				$('.build-handle-img').eq(13).addClass('active');
			}
			if($(window).scrollTop() >= fold_4_start+281 && $(window).scrollTop() <= fold_4_start+300){
				$('.build-handle-img').removeClass('active');
				$('.build-handle-img').eq(14).addClass('active');
			}
			if($(window).scrollTop() >= fold_4_start+301 && $(window).scrollTop() <= fold_4_start+320){
				$('.build-handle-img').removeClass('active');
				$('.build-handle-img').eq(15).addClass('active');
			}
			if($(window).scrollTop() >= fold_4_start+321 && $(window).scrollTop() <= fold_4_start+340){
				$('.build-handle-img').removeClass('active');
				$('.build-handle-img').eq(16).addClass('active');
			}
			if($(window).scrollTop() >= fold_4_start+341 && $(window).scrollTop() <= fold_4_start+360){
				$('.build-handle-img').removeClass('active');
				$('.build-handle-img').eq(17).addClass('active');
			}
			if($(window).scrollTop() >= fold_4_start+261 && $(window).scrollTop() <= fold_4_start+380){
				$('.build-handle-img').removeClass('active');
				$('.build-handle-img').eq(18).addClass('active');
			}
			if($(window).scrollTop() >= fold_4_start+281){
				$('.build-handle-img').removeClass('active');
				$('.build-handle-img').eq(18).addClass('active');
			}
		
			
			
		}
		function fold_5_anim() {
			
			if($(window).scrollTop() <= fold_5_start+100){
				$('.fold-5').removeClass('active');
			}
			if($(window).scrollTop() >= fold_5_start+100 && $(window).scrollTop() <= fold_5_start+125){
				$('.fold-5').removeClass('active');
				$('.fold-5').eq(0).addClass('active');
			}
			if($(window).scrollTop() >= fold_5_start+126 && $(window).scrollTop() <= fold_5_start+150){
				$('.fold-5').removeClass('active');
				$('.fold-5').eq(1).addClass('active');
			}
			if($(window).scrollTop() >= fold_5_start+151 && $(window).scrollTop() <= fold_5_start+175){
				$('.fold-5').removeClass('active');
				$('.fold-5').eq(2).addClass('active');
			}
			if($(window).scrollTop() >= fold_5_start+176 && $(window).scrollTop() <= fold_5_start+200){
				$('.fold-5').removeClass('active');
				$('.fold-5').eq(3).addClass('active');
			}
			if($(window).scrollTop() >= fold_5_start+201 && $(window).scrollTop() <= fold_5_start+225){
				$('.fold-5').removeClass('active');
				$('.fold-5').eq(4).addClass('active');
			}
			if($(window).scrollTop() >= fold_5_start+226 && $(window).scrollTop() <= fold_5_start+250){
				$('.fold-5').removeClass('active');
				$('.fold-5').eq(5).addClass('active');
			}
			if($(window).scrollTop() >= fold_5_start+251 && $(window).scrollTop() <= fold_5_start+275){
				$('.fold-5').removeClass('active');
				$('.fold-5').eq(6).addClass('active');
			}
			if($(window).scrollTop() >= fold_5_start+276 && $(window).scrollTop() <= fold_5_start+300){
				$('.fold-5').removeClass('active');
				$('.fold-5').eq(7).addClass('active');
			}
			if($(window).scrollTop() >= fold_5_start+301 && $(window).scrollTop() <= fold_5_start+325){
				$('.fold-5').removeClass('active');
				$('.fold-5').eq(8).addClass('active');
			}
		
				
			
			
			// MACHINE ANIMATION
			if($(window).scrollTop() <= fold_5_start){
				$('.machine').eq(0).addClass('active');
			}
			if($(window).scrollTop() >= fold_5_start && $(window).scrollTop() <= fold_5_start+10){
				$('.machine').removeClass('active');
				$('.machine').eq(0).addClass('active');
			}
			if($(window).scrollTop() >= fold_5_start+11 && $(window).scrollTop() <= fold_5_start+20){
				$('.machine').removeClass('active');
				$('.machine').eq(1).addClass('active');
			}
			if($(window).scrollTop() >= fold_5_start+21 && $(window).scrollTop() <= fold_5_start+30){
				$('.machine').removeClass('active');
				$('.machine').eq(2).addClass('active');
			}
			if($(window).scrollTop() >= fold_5_start+31 && $(window).scrollTop() <= fold_5_start+40){
				$('.machine').removeClass('active');
				$('.machine').eq(3).addClass('active');
			}
			if($(window).scrollTop() >= fold_5_start+41 && $(window).scrollTop() <= fold_5_start+50){
				$('.machine').removeClass('active');
				$('.machine').eq(4).addClass('active');
			}
			if($(window).scrollTop() >= fold_5_start+51 && $(window).scrollTop() <= fold_5_start+60){
				$('.machine').removeClass('active');
				$('.machine').eq(5).addClass('active');
			}
			if($(window).scrollTop() >= fold_5_start+61 && $(window).scrollTop() <= fold_5_start+70){
				$('.machine').removeClass('active');
				$('.machine').eq(6).addClass('active');
			}
			if($(window).scrollTop() >= fold_5_start+71 && $(window).scrollTop() <= fold_5_start+80){
				$('.machine').removeClass('active');
				$('.machine').eq(7).addClass('active');
			}
			if($(window).scrollTop() >= fold_5_start+81 && $(window).scrollTop() <= fold_5_start+90){
				$('.machine').removeClass('active');
				$('.machine').eq(8).addClass('active');
			}
			if($(window).scrollTop() >= fold_5_start+91 && $(window).scrollTop() <= fold_5_start+100){
				$('.machine').removeClass('active');
				$('.machine').eq(9).addClass('active');
			}
			if($(window).scrollTop() >= fold_5_start+101 && $(window).scrollTop() <= fold_5_start+110){
				$('.machine').removeClass('active');
				$('.machine').eq(10).addClass('active');
			}
			if($(window).scrollTop() >= fold_5_start+111 && $(window).scrollTop() <= fold_5_start+120){
				$('.machine').removeClass('active');
				$('.machine').eq(11).addClass('active');
			}
			if($(window).scrollTop() >= fold_5_start+121 && $(window).scrollTop() <= fold_5_start+130){
				$('.machine').removeClass('active');
				$('.machine').eq(12).addClass('active');
			}
			if($(window).scrollTop() >= fold_5_start+131 && $(window).scrollTop() <= fold_5_start+140){
				$('.machine').removeClass('active');
				$('.machine').eq(13).addClass('active');
			}
			if($(window).scrollTop() >= fold_5_start+141 && $(window).scrollTop() <= fold_5_start+150){
				$('.machine').removeClass('active');
				$('.machine').eq(14).addClass('active');
			}
			if($(window).scrollTop() >= fold_5_start+151 && $(window).scrollTop() <= fold_5_start+160){
				$('.machine').removeClass('active');
				$('.machine').eq(15).addClass('active');
			}
			if($(window).scrollTop() >= fold_5_start+161 && $(window).scrollTop() <= fold_5_start+170){
				$('.machine').removeClass('active');
				$('.machine').eq(16).addClass('active');
			}
			if($(window).scrollTop() >= fold_5_start+171 && $(window).scrollTop() <= fold_5_start+180){
				$('.machine').removeClass('active');
				$('.machine').eq(17).addClass('active');
			}
			if($(window).scrollTop() >= fold_5_start+181 && $(window).scrollTop() <= fold_5_start+190){
				$('.machine').removeClass('active');
				$('.machine').eq(18).addClass('active');
			}
			if($(window).scrollTop() >= fold_5_start+191 && $(window).scrollTop() <= fold_5_start+200){
				$('.machine').removeClass('active');
				$('.machine').eq(19).addClass('active');
			}
			if($(window).scrollTop() >= fold_5_start+201 && $(window).scrollTop() <= fold_5_start+210){
				$('.machine').removeClass('active');
				$('.machine').eq(20).addClass('active');
			}
			if($(window).scrollTop() >= fold_5_start+211 && $(window).scrollTop() <= fold_5_start+220){
				$('.machine').removeClass('active');
				$('.machine').eq(21).addClass('active');
			}
			if($(window).scrollTop() >= fold_5_start+221 && $(window).scrollTop() <= fold_5_start+230){
				$('.machine').removeClass('active');
				$('.machine').eq(22).addClass('active');
			}
			if($(window).scrollTop() >= fold_5_start+231 && $(window).scrollTop() <= fold_5_start+240){
				$('.machine').removeClass('active');
				$('.machine').eq(23).addClass('active');
			}
			if($(window).scrollTop() >= fold_5_start+241 && $(window).scrollTop() <= fold_5_start+250){
				$('.machine').removeClass('active');
				$('.machine').eq(24).addClass('active');
			}
			if($(window).scrollTop() >= fold_5_start+251 && $(window).scrollTop() <= fold_5_start+260){
				$('.machine').removeClass('active');
				$('.machine').eq(25).addClass('active');
				$('.box').css({'left':158});
			}
			if($(window).scrollTop() >= fold_5_start+261){
				$('.machine').removeClass('active');
				$('.machine').eq(25).addClass('active');
				$('.box').css({'left':158});
			}
			
			
			// BOX 
			if($(window).scrollTop() >= fold_5_start+261 && $(window).scrollTop() <= fold_5_start+270){
				$('.box').css({'left':168});
			}
			if($(window).scrollTop() >= fold_5_start+271 && $(window).scrollTop() <= fold_5_start+280){
				$('.box').css({'left':178});
			}
			if($(window).scrollTop() >= fold_5_start+281 && $(window).scrollTop() <= fold_5_start+290){
				$('.box').css({'left':188});
			}
			if($(window).scrollTop() >= fold_5_start+291 && $(window).scrollTop() <= fold_5_start+300){
				$('.box').css({'left':198});
			}
			if($(window).scrollTop() >= fold_5_start+301 && $(window).scrollTop() <= fold_5_start+310){
				$('.box').css({'left':208});
			}
			if($(window).scrollTop() >= fold_5_start+311 && $(window).scrollTop() <= fold_5_start+320){
				$('.box').css({'left':218});
			}
			if($(window).scrollTop() >= fold_5_start+321 && $(window).scrollTop() <= fold_5_start+330){
				$('.box').css({'left':228});
			}
			if($(window).scrollTop() >= fold_5_start+331 && $(window).scrollTop() <= fold_5_start+340){
				$('.box').css({'left':238});
			}
			if($(window).scrollTop() >= fold_5_start+341 && $(window).scrollTop() <= fold_5_start+350){
				$('.box').css({'left':248});
			}
			if($(window).scrollTop() >= fold_5_start+351){
				$('.box').css({'left':248});
			}
			
			
			//CAR
			if($(window).scrollTop() <= fold_5_start){
				$('.car').css({'left':530});
			}
			if($(window).scrollTop() >= fold_5_start && $(window).scrollTop() <= fold_5_start+10){
				$('.car').css({'left':535});
			}
			if($(window).scrollTop() >= fold_5_start+11 && $(window).scrollTop() <= fold_5_start+20){
				$('.car').css({'left':540});
			}
			if($(window).scrollTop() >= fold_5_start+21 && $(window).scrollTop() <= fold_5_start+30){
				$('.car').css({'left':545});
			}
			if($(window).scrollTop() >= fold_5_start+31 && $(window).scrollTop() <= fold_5_start+40){
				$('.car').css({'left':550});
			}
			if($(window).scrollTop() >= fold_5_start+41 && $(window).scrollTop() <= fold_5_start+50){
				$('.car').css({'left':555});
			}
			if($(window).scrollTop() >= fold_5_start+51 && $(window).scrollTop() <= fold_5_start+60){
				$('.car').css({'left':560});
			}
			if($(window).scrollTop() >= fold_5_start+61 && $(window).scrollTop() <= fold_5_start+70){
				$('.car').css({'left':565});
			}
			if($(window).scrollTop() >= fold_5_start+71 && $(window).scrollTop() <= fold_5_start+80){
				$('.car').css({'left':570});
			}
			if($(window).scrollTop() >= fold_5_start+81 && $(window).scrollTop() <= fold_5_start+90){
				$('.car').css({'left':575});
			}
			if($(window).scrollTop() >= fold_5_start+91 && $(window).scrollTop() <= fold_5_start+100){
				$('.car').css({'left':580});
			}
			if($(window).scrollTop() >= fold_5_start+101 && $(window).scrollTop() <= fold_5_start+110){
				$('.car').css({'left':585});
			}
			if($(window).scrollTop() >= fold_5_start+111 && $(window).scrollTop() <= fold_5_start+120){
				$('.car').css({'left':590});
			}
			if($(window).scrollTop() >= fold_5_start+121 && $(window).scrollTop() <= fold_5_start+130){
				$('.car').css({'left':595});
			}
			if($(window).scrollTop() >= fold_5_start+131 && $(window).scrollTop() <= fold_5_start+140){
				$('.car').css({'left':600});
			}
			if($(window).scrollTop() >= fold_5_start+141 && $(window).scrollTop() <= fold_5_start+150){
				$('.car').css({'left':605});
			}
			if($(window).scrollTop() >= fold_5_start+151 && $(window).scrollTop() <= fold_5_start+160){
				$('.car').css({'left':610});
			}
			if($(window).scrollTop() >= fold_5_start+161){
				$('.car').css({'left':610});
			}
		
			
		}
		function fold_6_anim() {
			
			if($(window).scrollTop() <= fold_6_start){
				$('.fold-6').removeClass('active');
			}
			if($(window).scrollTop() >= fold_6_start && $(window).scrollTop() <= fold_6_start+15){
				$('.fold-6').removeClass('active');
				$('.fold-6').eq(0).addClass('active');
			}
			if($(window).scrollTop() >= fold_6_start+16 && $(window).scrollTop() <= fold_6_start+30){
				$('.fold-6').removeClass('active');
				$('.fold-6').eq(1).addClass('active');
			}
			if($(window).scrollTop() >= fold_6_start+31 && $(window).scrollTop() <= fold_6_start+45){
				$('.fold-6').removeClass('active');
				$('.fold-6').eq(2).addClass('active');
			}
			if($(window).scrollTop() >= fold_6_start+46 && $(window).scrollTop() <= fold_6_start+60){
				$('.fold-6').removeClass('active');
				$('.fold-6').eq(3).addClass('active');
			}
			if($(window).scrollTop() >= fold_6_start+61 && $(window).scrollTop() <= fold_6_start+75){
				$('.fold-6').removeClass('active');
				$('.fold-6').eq(4).addClass('active');
			}
			if($(window).scrollTop() >= fold_6_start+76 && $(window).scrollTop() <= fold_6_start+90){
				$('.fold-6').removeClass('active');
				$('.fold-6').eq(5).addClass('active');
			}
			if($(window).scrollTop() >= fold_6_start+91 && $(window).scrollTop() <= fold_6_start+105){
				$('.fold-6').removeClass('active');
				$('.fold-6').eq(6).addClass('active');
			}
			if($(window).scrollTop() >= fold_6_start+106 && $(window).scrollTop() <= fold_6_start+120){
				$('.fold-6').removeClass('active');
				$('.fold-6').eq(7).addClass('active');
			}
			if($(window).scrollTop() >= fold_6_start+121 && $(window).scrollTop() <= fold_6_start+135){
				$('.fold-6').removeClass('active');
				$('.fold-6').eq(8).addClass('active');
			}
			if($(window).scrollTop() >= fold_6_start+136 && $(window).scrollTop() <= fold_6_start+150){
				$('.fold-6').removeClass('active');
				$('.fold-6').eq(9).addClass('active');
			}
			if($(window).scrollTop() >= fold_6_start+151 && $(window).scrollTop() <= fold_6_start+165){
				$('.fold-6').removeClass('active');
				$('.fold-6').eq(10).addClass('active');
			}
			if($(window).scrollTop() >= fold_6_start+166 && $(window).scrollTop() <= fold_6_start+180){
				$('.fold-6').removeClass('active');
				$('.fold-6').eq(11).addClass('active');
			}
			if($(window).scrollTop() >= fold_6_start+181 && $(window).scrollTop() <= fold_6_start+195){
				$('.fold-6').removeClass('active');
				$('.fold-6').eq(12).addClass('active');
			}
			if($(window).scrollTop() >= fold_6_start+196 && $(window).scrollTop() <= fold_6_start+210){
				$('.fold-6').removeClass('active');
				$('.fold-6').eq(13).addClass('active');
			}
			if($(window).scrollTop() >= fold_6_start+211 && $(window).scrollTop() <= fold_6_start+225){
				$('.fold-6').removeClass('active');
				$('.fold-6').eq(14).addClass('active');
			}
			if($(window).scrollTop() >= fold_6_start+226 && $(window).scrollTop() <= fold_6_start+240){
				$('.fold-6').removeClass('active');
				$('.fold-6').eq(15).addClass('active');
			}
			if($(window).scrollTop() >= fold_6_start+241 && $(window).scrollTop() <= fold_6_start+255){
				$('.fold-6').removeClass('active');
				$('.fold-6').eq(16).addClass('active');
			}
			if($(window).scrollTop() >= fold_6_start+256 && $(window).scrollTop() <= fold_6_start+270){
				$('.fold-6').removeClass('active');
				$('.fold-6').eq(17).addClass('active');
			}
			if($(window).scrollTop() >= fold_6_start+271 && $(window).scrollTop() <= fold_6_start+285){
				$('.fold-6').removeClass('active');
				$('.fold-6').eq(18).addClass('active');
			}
				
			
		
			// CHART 1
			if($(window).scrollTop() <= fold_6_start){
				$('.chart-1').eq(0).addClass('active');
				$('.chart-1-wrap').css({'top':80});
			}
			if($(window).scrollTop() >= fold_6_start && $(window).scrollTop() <= fold_6_start+10){
				$('.chart-1').removeClass('active');
				$('.chart-1').eq(0).addClass('active');
				$('.chart-1-wrap').css({'top':70});
			}
			if($(window).scrollTop() >= fold_6_start+11 && $(window).scrollTop() <= fold_6_start+20){
				$('.chart-1').removeClass('active');
				$('.chart-1').eq(1).addClass('active');
				$('.chart-1-wrap').css({'top':60});
			}
			if($(window).scrollTop() >= fold_6_start+21 && $(window).scrollTop() <= fold_6_start+30){
				$('.chart-1').removeClass('active');
				$('.chart-1').eq(2).addClass('active');
				$('.chart-1-wrap').css({'top':50});
			}
			if($(window).scrollTop() >= fold_6_start+31 && $(window).scrollTop() <= fold_6_start+40){
				$('.chart-1').removeClass('active');
				$('.chart-1').eq(3).addClass('active');
				$('.chart-1-wrap').css({'top':40});
			}
			if($(window).scrollTop() >= fold_6_start+41 && $(window).scrollTop() <= fold_6_start+50){
				$('.chart-1').removeClass('active');
				$('.chart-1').eq(4).addClass('active');
				$('.chart-1-wrap').css({'top':30});
			}
			if($(window).scrollTop() >= fold_6_start+51 && $(window).scrollTop() <= fold_6_start+60){
				$('.chart-1').removeClass('active');
				$('.chart-1').eq(5).addClass('active');
				$('.chart-1-wrap').css({'top':20});
			}
			if($(window).scrollTop() >= fold_6_start+61 && $(window).scrollTop() <= fold_6_start+70){
				$('.chart-1').removeClass('active');
				$('.chart-1').eq(6).addClass('active');
				$('.chart-1-wrap').css({'top':10});
			}
			if($(window).scrollTop() >= fold_6_start+71 && $(window).scrollTop() <= fold_6_start+80){
				$('.chart-1').removeClass('active');
				$('.chart-1').eq(7).addClass('active');
				$('.chart-1-wrap').css({'top':0});
			}
			if($(window).scrollTop() >= fold_6_start+81 && $(window).scrollTop() <= fold_6_start+90){
				$('.chart-1').removeClass('active');
				$('.chart-1').eq(8).addClass('active');
				$('.chart-1-wrap').css({'top':-10});
			}
			if($(window).scrollTop() >= fold_6_start+91 && $(window).scrollTop() <= fold_6_start+100){
				$('.chart-1').removeClass('active');
				$('.chart-1').eq(9).addClass('active');
				$('.chart-1-wrap').css({'top':-20});
			}
			if($(window).scrollTop() >= fold_6_start+101 && $(window).scrollTop() <= fold_6_start+110){
				$('.chart-1').removeClass('active');
				$('.chart-1').eq(10).addClass('active');
			}
			if($(window).scrollTop() >= fold_6_start+111 && $(window).scrollTop() <= fold_6_start+120){
				$('.chart-1').removeClass('active');
				$('.chart-1').eq(11).addClass('active');
			}
			if($(window).scrollTop() >= fold_6_start+121 && $(window).scrollTop() <= fold_6_start+130){
				$('.chart-1').removeClass('active');
				$('.chart-1').eq(12).addClass('active');
			}
			if($(window).scrollTop() >= fold_6_start+131 && $(window).scrollTop() <= fold_6_start+140){
				$('.chart-1').removeClass('active');
				$('.chart-1').eq(13).addClass('active');
			}
			if($(window).scrollTop() >= fold_6_start+141 && $(window).scrollTop() <= fold_6_start+150){
				$('.chart-1').removeClass('active');
				$('.chart-1').eq(14).addClass('active');
			}
			if($(window).scrollTop() >= fold_6_start+151 && $(window).scrollTop() <= fold_6_start+160){
				$('.chart-1').removeClass('active');
				$('.chart-1').eq(15).addClass('active');
			}
			if($(window).scrollTop() >= fold_6_start+161 && $(window).scrollTop() <= fold_6_start+170){
				$('.chart-1').removeClass('active');
				$('.chart-1').eq(16).addClass('active');
			}
			if($(window).scrollTop() >= fold_6_start+171 && $(window).scrollTop() <= fold_6_start+180){
				$('.chart-1').removeClass('active');
				$('.chart-1').eq(17).addClass('active');
			}
			if($(window).scrollTop() >= fold_6_start+181 && $(window).scrollTop() <= fold_6_start+190){
				$('.chart-1').removeClass('active');
				$('.chart-1').eq(18).addClass('active');
			}
			if($(window).scrollTop() >= fold_6_start+191 && $(window).scrollTop() <= fold_6_start+200){
				$('.chart-1').removeClass('active');
				$('.chart-1').eq(19).addClass('active');
			}
			if($(window).scrollTop() >= fold_6_start+201 && $(window).scrollTop() <= fold_6_start+210){
				$('.chart-1').removeClass('active');
				$('.chart-1').eq(20).addClass('active');
			}
			if($(window).scrollTop() >= fold_6_start+211 && $(window).scrollTop() <= fold_6_start+220){
				$('.chart-1').removeClass('active');
				$('.chart-1').eq(21).addClass('active');
			}
			if($(window).scrollTop() >= fold_6_start+221 && $(window).scrollTop() <= fold_6_start+230){
				$('.chart-1').removeClass('active');
				$('.chart-1').eq(22).addClass('active');
			}
			if($(window).scrollTop() >= fold_6_start+231 && $(window).scrollTop() <= fold_6_start+240){
				$('.chart-1').removeClass('active');
				$('.chart-1').eq(23).addClass('active');
			}
			if($(window).scrollTop() >= fold_6_start+241){
				$('.chart-1').removeClass('active');
				$('.chart-1').eq(23).addClass('active');
			}
		
			// CHART 2
			if($(window).scrollTop() <= fold_6_start){
				$('.chart-2').eq(0).addClass('active');
			}
			if($(window).scrollTop() >= fold_6_start && $(window).scrollTop() <= fold_6_start+105){
				$('.chart-2').removeClass('active');
				$('.chart-2').eq(0).addClass('active');
			}
			if($(window).scrollTop() >= fold_6_start+106 && $(window).scrollTop() <= fold_6_start+110){
				$('.chart-2').removeClass('active');
				$('.chart-2').eq(1).addClass('active');
			}
			if($(window).scrollTop() >= fold_6_start+111 && $(window).scrollTop() <= fold_6_start+115){
				$('.chart-2').removeClass('active');
				$('.chart-2').eq(2).addClass('active');
			}
			if($(window).scrollTop() >= fold_6_start+116 && $(window).scrollTop() <= fold_6_start+120){
				$('.chart-2').removeClass('active');
				$('.chart-2').eq(3).addClass('active');
			}
			if($(window).scrollTop() >= fold_6_start+121 && $(window).scrollTop() <= fold_6_start+125){
				$('.chart-2').removeClass('active');
				$('.chart-2').eq(4).addClass('active');
			}
			if($(window).scrollTop() >= fold_6_start+126 && $(window).scrollTop() <= fold_6_start+130){
				$('.chart-2').removeClass('active');
				$('.chart-2').eq(5).addClass('active');
			}
			if($(window).scrollTop() >= fold_6_start+131 && $(window).scrollTop() <= fold_6_start+135){
				$('.chart-2').removeClass('active');
				$('.chart-2').eq(6).addClass('active');
			}
			if($(window).scrollTop() >= fold_6_start+136 && $(window).scrollTop() <= fold_6_start+140){
				$('.chart-2').removeClass('active');
				$('.chart-2').eq(7).addClass('active');
			}
			if($(window).scrollTop() >= fold_6_start+141 && $(window).scrollTop() <= fold_6_start+145){
				$('.chart-2').removeClass('active');
				$('.chart-2').eq(8).addClass('active');
			}
			if($(window).scrollTop() >= fold_6_start+146 && $(window).scrollTop() <= fold_6_start+150){
				$('.chart-2').removeClass('active');
				$('.chart-2').eq(9).addClass('active');
			}
			if($(window).scrollTop() >= fold_6_start+151 && $(window).scrollTop() <= fold_6_start+155){
				$('.chart-2').removeClass('active');
				$('.chart-2').eq(10).addClass('active');
			}
			if($(window).scrollTop() >= fold_6_start+156 && $(window).scrollTop() <= fold_6_start+160){
				$('.chart-2').removeClass('active');
				$('.chart-2').eq(11).addClass('active');
			}
			if($(window).scrollTop() >= fold_6_start+161 && $(window).scrollTop() <= fold_6_start+165){
				$('.chart-2').removeClass('active');
				$('.chart-2').eq(12).addClass('active');
			}
			if($(window).scrollTop() >= fold_6_start+166 && $(window).scrollTop() <= fold_6_start+170){
				$('.chart-2').removeClass('active');
				$('.chart-2').eq(13).addClass('active');
			}
			if($(window).scrollTop() >= fold_6_start+171 && $(window).scrollTop() <= fold_6_start+175){
				$('.chart-2').removeClass('active');
				$('.chart-2').eq(14).addClass('active');
			}
			if($(window).scrollTop() >= fold_6_start+176 && $(window).scrollTop() <= fold_6_start+180){
				$('.chart-2').removeClass('active');
				$('.chart-2').eq(15).addClass('active');
			}
			if($(window).scrollTop() >= fold_6_start+181 && $(window).scrollTop() <= fold_6_start+185){
				$('.chart-2').removeClass('active');
				$('.chart-2').eq(16).addClass('active');
			}
			if($(window).scrollTop() >= fold_6_start+186 && $(window).scrollTop() <= fold_6_start+190){
				$('.chart-2').removeClass('active');
				$('.chart-2').eq(17).addClass('active');
			}
			if($(window).scrollTop() >= fold_6_start+191 && $(window).scrollTop() <= fold_6_start+195){
				$('.chart-2').removeClass('active');
				$('.chart-2').eq(18).addClass('active');
			}
			if($(window).scrollTop() >= fold_6_start+196 && $(window).scrollTop() <= fold_6_start+200){
				$('.chart-2').removeClass('active');
				$('.chart-2').eq(19).addClass('active');
			}
			if($(window).scrollTop() >= fold_6_start+201 && $(window).scrollTop() <= fold_6_start+205){
				$('.chart-2').removeClass('active');
				$('.chart-2').eq(20).addClass('active');
			}
			if($(window).scrollTop() >= fold_6_start+206 && $(window).scrollTop() <= fold_6_start+210){
				$('.chart-2').removeClass('active');
				$('.chart-2').eq(21).addClass('active');
			}
			if($(window).scrollTop() >= fold_6_start+211 && $(window).scrollTop() <= fold_6_start+215){
				$('.chart-2').removeClass('active');
				$('.chart-2').eq(22).addClass('active');
			}
			if($(window).scrollTop() >= fold_6_start+216 && $(window).scrollTop() <= fold_6_start+220){
				$('.chart-2').removeClass('active');
				$('.chart-2').eq(23).addClass('active');
			}
			if($(window).scrollTop() >= fold_6_start+221 && $(window).scrollTop() <= fold_6_start+225){
				$('.chart-2').removeClass('active');
				$('.chart-2').eq(24).addClass('active');
			}
			if($(window).scrollTop() >= fold_6_start+226 && $(window).scrollTop() <= fold_6_start+230){
				$('.chart-2').removeClass('active');
				$('.chart-2').eq(25).addClass('active');
			}
			if($(window).scrollTop() >= fold_6_start+231 && $(window).scrollTop() <= fold_6_start+235){
				$('.chart-2').removeClass('active');
				$('.chart-2').eq(26).addClass('active');
			}
			if($(window).scrollTop() >= fold_6_start+236 && $(window).scrollTop() <= fold_6_start+240){
				$('.chart-2').removeClass('active');
				$('.chart-2').eq(27).addClass('active');
			}
			if($(window).scrollTop() >= fold_6_start+241 && $(window).scrollTop() <= fold_6_start+245){
				$('.chart-2').removeClass('active');
				$('.chart-2').eq(28).addClass('active');
			}
			if($(window).scrollTop() >= fold_6_start+246 && $(window).scrollTop() <= fold_6_start+250){
				$('.chart-2').removeClass('active');
				$('.chart-2').eq(29).addClass('active');
			}
			if($(window).scrollTop() >= fold_6_start+251 && $(window).scrollTop() <= fold_6_start+255){
				$('.chart-2').removeClass('active');
				$('.chart-2').eq(30).addClass('active');
			}
			if($(window).scrollTop() >= fold_6_start+256 && $(window).scrollTop() <= fold_6_start+260){
				$('.chart-2').removeClass('active');
				$('.chart-2').eq(31).addClass('active');
			}
			if($(window).scrollTop() >= fold_6_start+261 && $(window).scrollTop() <= fold_6_start+265){
				$('.chart-2').removeClass('active');
				$('.chart-2').eq(32).addClass('active');
			}
			if($(window).scrollTop() >= fold_6_start+266 && $(window).scrollTop() <= fold_6_start+270){
				$('.chart-2').removeClass('active');
				$('.chart-2').eq(33).addClass('active');
			}
			if($(window).scrollTop() >= fold_6_start+271 && $(window).scrollTop() <= fold_6_start+275){
				$('.chart-2').removeClass('active');
				$('.chart-2').eq(34).addClass('active');
			}
			if($(window).scrollTop() >= fold_6_start+276 && $(window).scrollTop() <= fold_6_start+280){
				$('.chart-2').removeClass('active');
				$('.chart-2').eq(35).addClass('active');
			}
			if($(window).scrollTop() >= fold_6_start+281 && $(window).scrollTop() <= fold_6_start+285){
				$('.chart-2').removeClass('active');
				$('.chart-2').eq(36).addClass('active');
			}
			if($(window).scrollTop() >= fold_6_start+286 && $(window).scrollTop() <= fold_6_start+290){
				$('.chart-2').removeClass('active');
				$('.chart-2').eq(37).addClass('active');
			}
			if($(window).scrollTop() >= fold_6_start+291 && $(window).scrollTop() <= fold_6_start+295){
				$('.chart-2').removeClass('active');
				$('.chart-2').eq(38).addClass('active');
			}
			if($(window).scrollTop() >= fold_6_start+296 && $(window).scrollTop() <= fold_6_start+300){
				$('.chart-2').removeClass('active');
				$('.chart-2').eq(39).addClass('active');
			}
			if($(window).scrollTop() >= fold_6_start+301 && $(window).scrollTop() <= fold_6_start+305){
				$('.chart-2').removeClass('active');
				$('.chart-2').eq(40).addClass('active');
			}
			if($(window).scrollTop() >= fold_6_start+306 && $(window).scrollTop() <= fold_6_start+310){
				$('.chart-2').removeClass('active');
				$('.chart-2').eq(41).addClass('active');
			}
			if($(window).scrollTop() >= fold_6_start+311 && $(window).scrollTop() <= fold_6_start+315){
				$('.chart-2').removeClass('active');
				$('.chart-2').eq(42).addClass('active');
			}
			if($(window).scrollTop() >= fold_6_start+316){
				$('.chart-2').removeClass('active');
				$('.chart-2').eq(42).addClass('active');
			}
			
			// CHART 3
			if($(window).scrollTop() <= fold_6_start){
				$('.chart-3').eq(0).addClass('active');
			}
			if($(window).scrollTop() >= fold_6_start && $(window).scrollTop() <= fold_6_start+205){
				$('.chart-3').removeClass('active');
				$('.chart-3').eq(0).addClass('active');
			}
			if($(window).scrollTop() >= fold_6_start+206 && $(window).scrollTop() <= fold_6_start+210){
				$('.chart-3').removeClass('active');
				$('.chart-3').eq(1).addClass('active');
			}
			if($(window).scrollTop() >= fold_6_start+211 && $(window).scrollTop() <= fold_6_start+215){
				$('.chart-3').removeClass('active');
				$('.chart-3').eq(2).addClass('active');
			}
			if($(window).scrollTop() >= fold_6_start+216 && $(window).scrollTop() <= fold_6_start+220){
				$('.chart-3').removeClass('active');
				$('.chart-3').eq(3).addClass('active');
			}
			if($(window).scrollTop() >= fold_6_start+221 && $(window).scrollTop() <= fold_6_start+225){
				$('.chart-3').removeClass('active');
				$('.chart-3').eq(4).addClass('active');
			}
			if($(window).scrollTop() >= fold_6_start+226 && $(window).scrollTop() <= fold_6_start+230){
				$('.chart-3').removeClass('active');
				$('.chart-3').eq(5).addClass('active');
			}
			if($(window).scrollTop() >= fold_6_start+231 && $(window).scrollTop() <= fold_6_start+235){
				$('.chart-3').removeClass('active');
				$('.chart-3').eq(6).addClass('active');
			}
			if($(window).scrollTop() >= fold_6_start+236 && $(window).scrollTop() <= fold_6_start+240){
				$('.chart-3').removeClass('active');
				$('.chart-3').eq(7).addClass('active');
			}
			if($(window).scrollTop() >= fold_6_start+241 && $(window).scrollTop() <= fold_6_start+245){
				$('.chart-3').removeClass('active');
				$('.chart-3').eq(8).addClass('active');
			}
			if($(window).scrollTop() >= fold_6_start+246 && $(window).scrollTop() <= fold_6_start+250){
				$('.chart-3').removeClass('active');
				$('.chart-3').eq(9).addClass('active');
			}
			if($(window).scrollTop() >= fold_6_start+251 && $(window).scrollTop() <= fold_6_start+255){
				$('.chart-3').removeClass('active');
				$('.chart-3').eq(10).addClass('active');
			}
			if($(window).scrollTop() >= fold_6_start+256 && $(window).scrollTop() <= fold_6_start+260){
				$('.chart-3').removeClass('active');
				$('.chart-3').eq(11).addClass('active');
			}
			if($(window).scrollTop() >= fold_6_start+261 && $(window).scrollTop() <= fold_6_start+265){
				$('.chart-3').removeClass('active');
				$('.chart-3').eq(12).addClass('active');
			}
			if($(window).scrollTop() >= fold_6_start+266 && $(window).scrollTop() <= fold_6_start+270){
				$('.chart-3').removeClass('active');
				$('.chart-3').eq(13).addClass('active');
			}
			if($(window).scrollTop() >= fold_6_start+271 && $(window).scrollTop() <= fold_6_start+275){
				$('.chart-3').removeClass('active');
				$('.chart-3').eq(14).addClass('active');
			}
			if($(window).scrollTop() >= fold_6_start+276 && $(window).scrollTop() <= fold_6_start+280){
				$('.chart-3').removeClass('active');
				$('.chart-3').eq(15).addClass('active');
			}
			if($(window).scrollTop() >= fold_6_start+281 && $(window).scrollTop() <= fold_6_start+285){
				$('.chart-3').removeClass('active');
				$('.chart-3').eq(16).addClass('active');
			}
			if($(window).scrollTop() >= fold_6_start+286 && $(window).scrollTop() <= fold_6_start+290){
				$('.chart-3').removeClass('active');
				$('.chart-3').eq(17).addClass('active');
			}
			if($(window).scrollTop() >= fold_6_start+291 && $(window).scrollTop() <= fold_6_start+295){
				$('.chart-3').removeClass('active');
				$('.chart-3').eq(18).addClass('active');
			}
			if($(window).scrollTop() >= fold_6_start+296 && $(window).scrollTop() <= fold_6_start+300){
				$('.chart-3').removeClass('active');
				$('.chart-3').eq(19).addClass('active');
			}
			if($(window).scrollTop() >= fold_6_start+301 && $(window).scrollTop() <= fold_6_start+305){
				$('.chart-3').removeClass('active');
				$('.chart-3').eq(20).addClass('active');
			}
			if($(window).scrollTop() >= fold_6_start+306 && $(window).scrollTop() <= fold_6_start+310){
				$('.chart-3').removeClass('active');
				$('.chart-3').eq(21).addClass('active');
			}
			if($(window).scrollTop() >= fold_6_start+311 && $(window).scrollTop() <= fold_6_start+315){
				$('.chart-3').removeClass('active');
				$('.chart-3').eq(22).addClass('active');
			}
			if($(window).scrollTop() >= fold_6_start+316 && $(window).scrollTop() <= fold_6_start+320){
				$('.chart-3').removeClass('active');
				$('.chart-3').eq(23).addClass('active');
			}
			if($(window).scrollTop() >= fold_6_start+321 && $(window).scrollTop() <= fold_6_start+325){
				$('.chart-3').removeClass('active');
				$('.chart-3').eq(24).addClass('active');
			}
			if($(window).scrollTop() >= fold_6_start+326 && $(window).scrollTop() <= fold_6_start+330){
				$('.chart-3').removeClass('active');
				$('.chart-3').eq(25).addClass('active');
			}
			if($(window).scrollTop() >= fold_6_start+331 && $(window).scrollTop() <= fold_6_start+335){
				$('.chart-3').removeClass('active');
				$('.chart-3').eq(26).addClass('active');
			}
			if($(window).scrollTop() >= fold_6_start+336 && $(window).scrollTop() <= fold_6_start+340){
				$('.chart-3').removeClass('active');
				$('.chart-3').eq(27).addClass('active');
			}
			if($(window).scrollTop() >= fold_6_start+341 && $(window).scrollTop() <= fold_6_start+345){
				$('.chart-3').removeClass('active');
				$('.chart-3').eq(28).addClass('active');
			}
			if($(window).scrollTop() >= fold_6_start+346 && $(window).scrollTop() <= fold_6_start+350){
				$('.chart-3').removeClass('active');
				$('.chart-3').eq(29).addClass('active');
			}
			if($(window).scrollTop() >= fold_6_start+351 && $(window).scrollTop() <= fold_6_start+355){
				$('.chart-3').removeClass('active');
				$('.chart-3').eq(30).addClass('active');
			}
			if($(window).scrollTop() >= fold_6_start+356 && $(window).scrollTop() <= fold_6_start+360){
				$('.chart-3').removeClass('active');
				$('.chart-3').eq(31).addClass('active');
			}
			if($(window).scrollTop() >= fold_6_start+361 && $(window).scrollTop() <= fold_6_start+365){
				$('.chart-3').removeClass('active');
				$('.chart-3').eq(32).addClass('active');
			}
			if($(window).scrollTop() >= fold_6_start+366 && $(window).scrollTop() <= fold_6_start+370){
				$('.chart-3').removeClass('active');
				$('.chart-3').eq(33).addClass('active');
			}
			if($(window).scrollTop() >= fold_6_start+371 && $(window).scrollTop() <= fold_6_start+375){
				$('.chart-3').removeClass('active');
				$('.chart-3').eq(34).addClass('active');
			}
			if($(window).scrollTop() >= fold_6_start+376 && $(window).scrollTop() <= fold_6_start+380){
				$('.chart-3').removeClass('active');
				$('.chart-3').eq(35).addClass('active');
			}
			if($(window).scrollTop() >= fold_6_start+381 && $(window).scrollTop() <= fold_6_start+385){
				$('.chart-3').removeClass('active');
				$('.chart-3').eq(36).addClass('active');
			}
			if($(window).scrollTop() >= fold_6_start+386 && $(window).scrollTop() <= fold_6_start+390){
				$('.chart-3').removeClass('active');
				$('.chart-3').eq(37).addClass('active');
			}
			if($(window).scrollTop() >= fold_6_start+391 && $(window).scrollTop() <= fold_6_start+395){
				$('.chart-3').removeClass('active');
				$('.chart-3').eq(38).addClass('active');
			}
			if($(window).scrollTop() >= fold_6_start+396 && $(window).scrollTop() <= fold_6_start+400){
				$('.chart-3').removeClass('active');
				$('.chart-3').eq(39).addClass('active');
			}
			if($(window).scrollTop() >= fold_6_start+401 && $(window).scrollTop() <= fold_6_start+405){
				$('.chart-3').removeClass('active');
				$('.chart-3').eq(40).addClass('active');
			}
			if($(window).scrollTop() >= fold_6_start+406){
				$('.chart-3').removeClass('active');
				$('.chart-3').eq(40).addClass('active');
			}
		
		}
		function fold_7_anim() {
			
			if($(window).scrollTop() <= fold_7_start){
				$('.fold-7').removeClass('active');
			}
			if($(window).scrollTop() >= fold_7_start && $(window).scrollTop() <= fold_7_start+15){
				$('.fold-7').removeClass('active');
				$('.fold-7').eq(0).addClass('active');
			}
			if($(window).scrollTop() >= fold_7_start+16 && $(window).scrollTop() <= fold_7_start+30){
				$('.fold-7').removeClass('active');
				$('.fold-7').eq(1).addClass('active');
			}
			if($(window).scrollTop() >= fold_7_start+31 && $(window).scrollTop() <= fold_7_start+45){
				$('.fold-7').removeClass('active');
				$('.fold-7').eq(2).addClass('active');
			}
			if($(window).scrollTop() >= fold_7_start+46 && $(window).scrollTop() <= fold_7_start+60){
				$('.fold-7').removeClass('active');
				$('.fold-7').eq(3).addClass('active');
			}
			if($(window).scrollTop() >= fold_7_start+61 && $(window).scrollTop() <= fold_7_start+75){
				$('.fold-7').removeClass('active');
				$('.fold-7').eq(4).addClass('active');
			}
			if($(window).scrollTop() >= fold_7_start+76 && $(window).scrollTop() <= fold_7_start+90){
				$('.fold-7').removeClass('active');
				$('.fold-7').eq(5).addClass('active');
			}
			if($(window).scrollTop() >= fold_7_start+91 && $(window).scrollTop() <= fold_7_start+105){
				$('.fold-7').removeClass('active');
				$('.fold-7').eq(6).addClass('active');
			}
			if($(window).scrollTop() >= fold_7_start+106 && $(window).scrollTop() <= fold_7_start+120){
				$('.fold-7').removeClass('active');
				$('.fold-7').eq(7).addClass('active');
			}
			if($(window).scrollTop() >= fold_7_start+121 && $(window).scrollTop() <= fold_7_start+135){
				$('.fold-7').removeClass('active');
				$('.fold-7').eq(8).addClass('active');
			}
			if($(window).scrollTop() >= fold_7_start+136 && $(window).scrollTop() <= fold_7_start+150){
				$('.fold-7').removeClass('active');
				$('.fold-7').eq(9).addClass('active');
			}
			if($(window).scrollTop() >= fold_7_start+151 && $(window).scrollTop() <= fold_7_start+165){
				$('.fold-7').removeClass('active');
				$('.fold-7').eq(10).addClass('active');
			}
			if($(window).scrollTop() >= fold_7_start+166 && $(window).scrollTop() <= fold_7_start+180){
				$('.fold-7').removeClass('active');
				$('.fold-7').eq(11).addClass('active');
			}
			if($(window).scrollTop() >= fold_7_start+181 && $(window).scrollTop() <= fold_7_start+195){
				$('.fold-7').removeClass('active');
				$('.fold-7').eq(12).addClass('active');
			}
			if($(window).scrollTop() >= fold_7_start+196 && $(window).scrollTop() <= fold_7_start+210){
				$('.fold-7').removeClass('active');
				$('.fold-7').eq(13).addClass('active');
			}
			if($(window).scrollTop() >= fold_7_start+211 && $(window).scrollTop() <= fold_7_start+225){
				$('.fold-7').removeClass('active');
				$('.fold-7').eq(14).addClass('active');
			}
			if($(window).scrollTop() >= fold_7_start+226 && $(window).scrollTop() <= fold_7_start+240){
				$('.fold-7').removeClass('active');
				$('.fold-7').eq(15).addClass('active');
			}
			if($(window).scrollTop() >= fold_7_start+241 && $(window).scrollTop() <= fold_7_start+255){
				$('.fold-7').removeClass('active');
				$('.fold-7').eq(16).addClass('active');
			}
			if($(window).scrollTop() >= fold_7_start+256 && $(window).scrollTop() <= fold_7_start+270){
				$('.fold-7').removeClass('active');
				$('.fold-7').eq(17).addClass('active');
			}
			if($(window).scrollTop() >= fold_7_start+271 && $(window).scrollTop() <= fold_7_start+285){
				$('.fold-7').removeClass('active');
				$('.fold-7').eq(18).addClass('active');
			}
			if($(window).scrollTop() >= fold_7_start+286 && $(window).scrollTop() <= fold_7_start+300){
				$('.fold-7').removeClass('active');
				$('.fold-7').eq(19).addClass('active');
			}
			if($(window).scrollTop() >= fold_7_start+301 && $(window).scrollTop() <= fold_7_start+315){
				$('.fold-7').removeClass('active');
				$('.fold-7').eq(20).addClass('active');
			}
			if($(window).scrollTop() >= fold_7_start+316 && $(window).scrollTop() <= fold_7_start+330){
				$('.fold-7').removeClass('active');
				$('.fold-7').eq(21).addClass('active');
			}
				
			
			
			
			// MANAGE 1
			if($(window).scrollTop() <= fold_7_start){
				$('.manage-1').eq(0).addClass('active');
			}
			if($(window).scrollTop() >= fold_7_start && $(window).scrollTop() <= fold_7_start+5){
				$('.manage-1').removeClass('active');
				$('.manage-1').eq(0).addClass('active');
			}
			if($(window).scrollTop() >= fold_7_start+6 && $(window).scrollTop() <= fold_7_start+10){
				$('.manage-1').removeClass('active');
				$('.manage-1').eq(1).addClass('active');
			}
			if($(window).scrollTop() >= fold_7_start+11 && $(window).scrollTop() <= fold_7_start+15){
				$('.manage-1').removeClass('active');
				$('.manage-1').eq(2).addClass('active');
			}
			if($(window).scrollTop() >= fold_7_start+16 && $(window).scrollTop() <= fold_7_start+20){
				$('.manage-1').removeClass('active');
				$('.manage-1').eq(3).addClass('active');
			}
			if($(window).scrollTop() >= fold_7_start+21 && $(window).scrollTop() <= fold_7_start+25){
				$('.manage-1').removeClass('active');
				$('.manage-1').eq(4).addClass('active');
			}
			if($(window).scrollTop() >= fold_7_start+26 && $(window).scrollTop() <= fold_7_start+30){
				$('.manage-1').removeClass('active');
				$('.manage-1').eq(5).addClass('active');
			}
			if($(window).scrollTop() >= fold_7_start+31 && $(window).scrollTop() <= fold_7_start+35){
				$('.manage-1').removeClass('active');
				$('.manage-1').eq(6).addClass('active');
			}
			if($(window).scrollTop() >= fold_7_start+36 && $(window).scrollTop() <= fold_7_start+40){
				$('.manage-1').removeClass('active');
				$('.manage-1').eq(7).addClass('active');
			}
			if($(window).scrollTop() >= fold_7_start+41 && $(window).scrollTop() <= fold_7_start+45){
				$('.manage-1').removeClass('active');
				$('.manage-1').eq(8).addClass('active');
			}
			if($(window).scrollTop() >= fold_7_start+46 && $(window).scrollTop() <= fold_7_start+50){
				$('.manage-1').removeClass('active');
				$('.manage-1').eq(9).addClass('active');
			}
			if($(window).scrollTop() >= fold_7_start+51 && $(window).scrollTop() <= fold_7_start+55){
				$('.manage-1').removeClass('active');
				$('.manage-1').eq(10).addClass('active');
			}
			if($(window).scrollTop() >= fold_7_start+56 && $(window).scrollTop() <= fold_7_start+60){
				$('.manage-1').removeClass('active');
				$('.manage-1').eq(11).addClass('active');
			}
			if($(window).scrollTop() >= fold_7_start+61 && $(window).scrollTop() <= fold_7_start+65){
				$('.manage-1').removeClass('active');
				$('.manage-1').eq(12).addClass('active');
			}
			if($(window).scrollTop() >= fold_7_start+66 && $(window).scrollTop() <= fold_7_start+70){
				$('.manage-1').removeClass('active');
				$('.manage-1').eq(13).addClass('active');
			}
			if($(window).scrollTop() >= fold_7_start+71 && $(window).scrollTop() <= fold_7_start+75){
				$('.manage-1').removeClass('active');
				$('.manage-1').eq(14).addClass('active');
			}
			if($(window).scrollTop() >= fold_7_start+76 && $(window).scrollTop() <= fold_7_start+80){
				$('.manage-1').removeClass('active');
				$('.manage-1').eq(15).addClass('active');
			}
			if($(window).scrollTop() >= fold_7_start+81 && $(window).scrollTop() <= fold_7_start+85){
				$('.manage-1').removeClass('active');
				$('.manage-1').eq(16).addClass('active');
			}
			if($(window).scrollTop() >= fold_7_start+86 && $(window).scrollTop() <= fold_7_start+90){
				$('.manage-1').removeClass('active');
				$('.manage-1').eq(17).addClass('active');
			}
			if($(window).scrollTop() >= fold_7_start+91 && $(window).scrollTop() <= fold_7_start+95){
				$('.manage-1').removeClass('active');
				$('.manage-1').eq(18).addClass('active');
			}
			if($(window).scrollTop() >= fold_7_start+96 && $(window).scrollTop() <= fold_7_start+100){
				$('.manage-1').removeClass('active');
				$('.manage-1').eq(19).addClass('active');
			}
			if($(window).scrollTop() >= fold_7_start+101 && $(window).scrollTop() <= fold_7_start+105){
				$('.manage-1').removeClass('active');
				$('.manage-1').eq(20).addClass('active');
			}
			if($(window).scrollTop() >= fold_7_start+106 && $(window).scrollTop() <= fold_7_start+110){
				$('.manage-1').removeClass('active');
				$('.manage-1').eq(21).addClass('active');
			}
			if($(window).scrollTop() >= fold_7_start+111 && $(window).scrollTop() <= fold_7_start+115){
				$('.manage-1').removeClass('active');
				$('.manage-1').eq(22).addClass('active');
			}
			if($(window).scrollTop() >= fold_7_start+116 && $(window).scrollTop() <= fold_7_start+120){
				$('.manage-1').removeClass('active');
				$('.manage-1').eq(23).addClass('active');
			}
			if($(window).scrollTop() >= fold_7_start+121 && $(window).scrollTop() <= fold_7_start+125){
				$('.manage-1').removeClass('active');
				$('.manage-1').eq(24).addClass('active');
			}
			if($(window).scrollTop() >= fold_7_start+126 && $(window).scrollTop() <= fold_7_start+130){
				$('.manage-1').removeClass('active');
				$('.manage-1').eq(25).addClass('active');
			}
			if($(window).scrollTop() >= fold_7_start+131 && $(window).scrollTop() <= fold_7_start+135){
				$('.manage-1').removeClass('active');
				$('.manage-1').eq(26).addClass('active');
			}
			if($(window).scrollTop() >= fold_7_start+136 && $(window).scrollTop() <= fold_7_start+140){
				$('.manage-1').removeClass('active');
				$('.manage-1').eq(27).addClass('active');
			}
			if($(window).scrollTop() >= fold_7_start+141 && $(window).scrollTop() <= fold_7_start+145){
				$('.manage-1').removeClass('active');
				$('.manage-1').eq(28).addClass('active');
			}
			if($(window).scrollTop() >= fold_7_start+146 && $(window).scrollTop() <= fold_7_start+150){
				$('.manage-1').removeClass('active');
				$('.manage-1').eq(29).addClass('active');
			}
			if($(window).scrollTop() >= fold_7_start+151 && $(window).scrollTop() <= fold_7_start+155){
				$('.manage-1').removeClass('active');
				$('.manage-1').eq(30).addClass('active');
			}
			if($(window).scrollTop() >= fold_7_start+156 && $(window).scrollTop() <= fold_7_start+160){
				$('.manage-1').removeClass('active');
				$('.manage-1').eq(31).addClass('active');
			}
			if($(window).scrollTop() >= fold_7_start+161 && $(window).scrollTop() <= fold_7_start+165){
				$('.manage-1').removeClass('active');
				$('.manage-1').eq(32).addClass('active');
			}
			if($(window).scrollTop() >= fold_7_start+166 && $(window).scrollTop() <= fold_7_start+170){
				$('.manage-1').removeClass('active');
				$('.manage-1').eq(33).addClass('active');
			}
			if($(window).scrollTop() >= fold_7_start+171 && $(window).scrollTop() <= fold_7_start+175){
				$('.manage-1').removeClass('active');
				$('.manage-1').eq(34).addClass('active');
			}
			if($(window).scrollTop() >= fold_7_start+176 && $(window).scrollTop() <= fold_7_start+180){
				$('.manage-1').removeClass('active');
				$('.manage-1').eq(35).addClass('active');
			}
			if($(window).scrollTop() >= fold_7_start+181 && $(window).scrollTop() <= fold_7_start+185){
				$('.manage-1').removeClass('active');
				$('.manage-1').eq(36).addClass('active');
			}
			if($(window).scrollTop() >= fold_7_start+186 && $(window).scrollTop() <= fold_7_start+190){
				$('.manage-1').removeClass('active');
				$('.manage-1').eq(37).addClass('active');
			}
			if($(window).scrollTop() >= fold_7_start+191 && $(window).scrollTop() <= fold_7_start+195){
				$('.manage-1').removeClass('active');
				$('.manage-1').eq(38).addClass('active');
			}
			if($(window).scrollTop() >= fold_7_start+196 && $(window).scrollTop() <= fold_7_start+200){
				$('.manage-1').removeClass('active');
				$('.manage-1').eq(39).addClass('active');
			}
			if($(window).scrollTop() >= fold_7_start+201 && $(window).scrollTop() <= fold_7_start+205){
				$('.manage-1').removeClass('active');
				$('.manage-1').eq(40).addClass('active');
			}
			if($(window).scrollTop() >= fold_7_start+206 && $(window).scrollTop() <= fold_7_start+210){
				$('.manage-1').removeClass('active');
				$('.manage-1').eq(41).addClass('active');
			}
			if($(window).scrollTop() >= fold_7_start+211 && $(window).scrollTop() <= fold_7_start+215){
				$('.manage-1').removeClass('active');
				$('.manage-1').eq(42).addClass('active');
			}
			if($(window).scrollTop() >= fold_7_start+216 && $(window).scrollTop() <= fold_7_start+220){
				$('.manage-1').removeClass('active');
				$('.manage-1').eq(43).addClass('active');
			}
			if($(window).scrollTop() >= fold_7_start+221 && $(window).scrollTop() <= fold_7_start+225){
				$('.manage-1').removeClass('active');
				$('.manage-1').eq(44).addClass('active');
			}
			if($(window).scrollTop() >= fold_7_start+226 && $(window).scrollTop() <= fold_7_start+230){
				$('.manage-1').removeClass('active');
				$('.manage-1').eq(45).addClass('active');
			}
			if($(window).scrollTop() >= fold_7_start+231 && $(window).scrollTop() <= fold_7_start+235){
				$('.manage-1').removeClass('active');
				$('.manage-1').eq(46).addClass('active');
			}
			if($(window).scrollTop() >= fold_7_start+236 && $(window).scrollTop() <= fold_7_start+240){
				$('.manage-1').removeClass('active');
				$('.manage-1').eq(47).addClass('active');
			}
			if($(window).scrollTop() >= fold_7_start+241 && $(window).scrollTop() <= fold_7_start+245){
				$('.manage-1').removeClass('active');
				$('.manage-1').eq(48).addClass('active');
			}
			if($(window).scrollTop() >= fold_7_start+246 && $(window).scrollTop() <= fold_7_start+250){
				$('.manage-1').removeClass('active');
				$('.manage-1').eq(49).addClass('active');
			}
			if($(window).scrollTop() >= fold_7_start+251){
				$('.manage-1').removeClass('active');
				$('.manage-1').eq(49).addClass('active');
			}
			
			
			// MANAGE 2
			if($(window).scrollTop() <= fold_7_start){
				$('.manage-2').eq(0).addClass('active');
			}
			if($(window).scrollTop() >= fold_7_start && $(window).scrollTop() <= fold_7_start+105){
				$('.manage-2').removeClass('active');
				$('.manage-2').eq(0).addClass('active');
			}
			if($(window).scrollTop() >= fold_7_start+106 && $(window).scrollTop() <= fold_7_start+110){
				$('.manage-2').removeClass('active');
				$('.manage-2').eq(1).addClass('active');
			}
			if($(window).scrollTop() >= fold_7_start+111 && $(window).scrollTop() <= fold_7_start+115){
				$('.manage-2').removeClass('active');
				$('.manage-2').eq(2).addClass('active');
			}
			if($(window).scrollTop() >= fold_7_start+116 && $(window).scrollTop() <= fold_7_start+120){
				$('.manage-2').removeClass('active');
				$('.manage-2').eq(3).addClass('active');
			}
			if($(window).scrollTop() >= fold_7_start+121 && $(window).scrollTop() <= fold_7_start+125){
				$('.manage-2').removeClass('active');
				$('.manage-2').eq(4).addClass('active');
			}
			if($(window).scrollTop() >= fold_7_start+126 && $(window).scrollTop() <= fold_7_start+130){
				$('.manage-2').removeClass('active');
				$('.manage-2').eq(5).addClass('active');
			}
			if($(window).scrollTop() >= fold_7_start+131 && $(window).scrollTop() <= fold_7_start+135){
				$('.manage-2').removeClass('active');
				$('.manage-2').eq(6).addClass('active');
			}
			if($(window).scrollTop() >= fold_7_start+136 && $(window).scrollTop() <= fold_7_start+140){
				$('.manage-2').removeClass('active');
				$('.manage-2').eq(7).addClass('active');
			}
			if($(window).scrollTop() >= fold_7_start+141 && $(window).scrollTop() <= fold_7_start+145){
				$('.manage-2').removeClass('active');
				$('.manage-2').eq(8).addClass('active');
			}
			if($(window).scrollTop() >= fold_7_start+146 && $(window).scrollTop() <= fold_7_start+150){
				$('.manage-2').removeClass('active');
				$('.manage-2').eq(9).addClass('active');
			}
			if($(window).scrollTop() >= fold_7_start+151 && $(window).scrollTop() <= fold_7_start+155){
				$('.manage-2').removeClass('active');
				$('.manage-2').eq(10).addClass('active');
			}
			if($(window).scrollTop() >= fold_7_start+156 && $(window).scrollTop() <= fold_7_start+160){
				$('.manage-2').removeClass('active');
				$('.manage-2').eq(11).addClass('active');
			}
			if($(window).scrollTop() >= fold_7_start+161 && $(window).scrollTop() <= fold_7_start+165){
				$('.manage-2').removeClass('active');
				$('.manage-2').eq(12).addClass('active');
			}
			if($(window).scrollTop() >= fold_7_start+166 && $(window).scrollTop() <= fold_7_start+170){
				$('.manage-2').removeClass('active');
				$('.manage-2').eq(13).addClass('active');
			}
			if($(window).scrollTop() >= fold_7_start+171 && $(window).scrollTop() <= fold_7_start+175){
				$('.manage-2').removeClass('active');
				$('.manage-2').eq(14).addClass('active');
		
			}
			if($(window).scrollTop() >= fold_7_start+176 && $(window).scrollTop() <= fold_7_start+180){
				$('.manage-2').removeClass('active');
				$('.manage-2').eq(15).addClass('active');
			}
			if($(window).scrollTop() >= fold_7_start+181 && $(window).scrollTop() <= fold_7_start+185){
				$('.manage-2').removeClass('active');
				$('.manage-2').eq(16).addClass('active');
			}
			if($(window).scrollTop() >= fold_7_start+186 && $(window).scrollTop() <= fold_7_start+190){
				$('.manage-2').removeClass('active');
				$('.manage-2').eq(17).addClass('active');
			}
			if($(window).scrollTop() >= fold_7_start+191 && $(window).scrollTop() <= fold_7_start+195){
				$('.manage-2').removeClass('active');
				$('.manage-2').eq(18).addClass('active');
			}
			if($(window).scrollTop() >= fold_7_start+196 && $(window).scrollTop() <= fold_7_start+200){
				$('.manage-2').removeClass('active');
				$('.manage-2').eq(19).addClass('active');
			}
			if($(window).scrollTop() >= fold_7_start+201 && $(window).scrollTop() <= fold_7_start+205){
				$('.manage-2').removeClass('active');
				$('.manage-2').eq(20).addClass('active');
			}
			if($(window).scrollTop() >= fold_7_start+206 && $(window).scrollTop() <= fold_7_start+210){
				$('.manage-2').removeClass('active');
				$('.manage-2').eq(21).addClass('active');
			}
			if($(window).scrollTop() >= fold_7_start+211 && $(window).scrollTop() <= fold_7_start+215){
				$('.manage-2').removeClass('active');
				$('.manage-2').eq(22).addClass('active');
			}
			if($(window).scrollTop() >= fold_7_start+216 && $(window).scrollTop() <= fold_7_start+220){
				$('.manage-2').removeClass('active');
				$('.manage-2').eq(23).addClass('active');
			}
			if($(window).scrollTop() >= fold_7_start+221 && $(window).scrollTop() <= fold_7_start+225){
				$('.manage-2').removeClass('active');
				$('.manage-2').eq(24).addClass('active');
			}
			if($(window).scrollTop() >= fold_7_start+226 && $(window).scrollTop() <= fold_7_start+230){
				$('.manage-2').removeClass('active');
				$('.manage-2').eq(25).addClass('active');
			}
			if($(window).scrollTop() >= fold_7_start+231 && $(window).scrollTop() <= fold_7_start+235){
				$('.manage-2').removeClass('active');
				$('.manage-2').eq(26).addClass('active');
			}
			if($(window).scrollTop() >= fold_7_start+236 && $(window).scrollTop() <= fold_7_start+240){
				$('.manage-2').removeClass('active');
				$('.manage-2').eq(27).addClass('active');
			}
			if($(window).scrollTop() >= fold_7_start+241 && $(window).scrollTop() <= fold_7_start+245){
				$('.manage-2').removeClass('active');
				$('.manage-2').eq(28).addClass('active');
			}
			if($(window).scrollTop() >= fold_7_start+246 && $(window).scrollTop() <= fold_7_start+250){
				$('.manage-2').removeClass('active');
				$('.manage-2').eq(29).addClass('active');
			}
			if($(window).scrollTop() >= fold_7_start+251 && $(window).scrollTop() <= fold_7_start+255){
				$('.manage-2').removeClass('active');
				$('.manage-2').eq(30).addClass('active');
			}
			if($(window).scrollTop() >= fold_7_start+256 && $(window).scrollTop() <= fold_7_start+260){
				$('.manage-2').removeClass('active');
				$('.manage-2').eq(31).addClass('active');
			}
			if($(window).scrollTop() >= fold_7_start+261 && $(window).scrollTop() <= fold_7_start+265){
				$('.manage-2').removeClass('active');
				$('.manage-2').eq(32).addClass('active');
			}
			if($(window).scrollTop() >= fold_7_start+266 && $(window).scrollTop() <= fold_7_start+270){
				$('.manage-2').removeClass('active');
				$('.manage-2').eq(33).addClass('active');
			}
			if($(window).scrollTop() >= fold_7_start+271 && $(window).scrollTop() <= fold_7_start+275){
				$('.manage-2').removeClass('active');
				$('.manage-2').eq(34).addClass('active');
			}
			if($(window).scrollTop() >= fold_7_start+276 && $(window).scrollTop() <= fold_7_start+280){
				$('.manage-2').removeClass('active');
				$('.manage-2').eq(35).addClass('active');
			}
			if($(window).scrollTop() >= fold_7_start+281 && $(window).scrollTop() <= fold_7_start+285){
				$('.manage-2').removeClass('active');
				$('.manage-2').eq(36).addClass('active');
			}
			if($(window).scrollTop() >= fold_7_start+286 && $(window).scrollTop() <= fold_7_start+290){
				$('.manage-2').removeClass('active');
				$('.manage-2').eq(37).addClass('active');
			}
			if($(window).scrollTop() >= fold_7_start+291){
				$('.manage-2').removeClass('active');
				$('.manage-2').eq(37).addClass('active');
			}
			
			
			// MANAGE 3
			if($(window).scrollTop() <= fold_7_start){
				$('.manage-3').eq(0).addClass('active');
			}
			if($(window).scrollTop() >= fold_7_start && $(window).scrollTop() <= fold_7_start+105){
				$('.manage-3').removeClass('active');
				$('.manage-3').eq(0).addClass('active');
			}
			if($(window).scrollTop() >= fold_7_start+106 && $(window).scrollTop() <= fold_7_start+110){
				$('.manage-3').removeClass('active');
				$('.manage-3').eq(1).addClass('active');
			}
			if($(window).scrollTop() >= fold_7_start+111 && $(window).scrollTop() <= fold_7_start+115){
				$('.manage-3').removeClass('active');
				$('.manage-3').eq(2).addClass('active');
			}
			if($(window).scrollTop() >= fold_7_start+116 && $(window).scrollTop() <= fold_7_start+120){
				$('.manage-3').removeClass('active');
				$('.manage-3').eq(3).addClass('active');
			}
			if($(window).scrollTop() >= fold_7_start+121 && $(window).scrollTop() <= fold_7_start+125){
				$('.manage-3').removeClass('active');
				$('.manage-3').eq(4).addClass('active');
			}
			if($(window).scrollTop() >= fold_7_start+126 && $(window).scrollTop() <= fold_7_start+130){
				$('.manage-3').removeClass('active');
				$('.manage-3').eq(5).addClass('active');
			}
			if($(window).scrollTop() >= fold_7_start+131 && $(window).scrollTop() <= fold_7_start+135){
				$('.manage-3').removeClass('active');
				$('.manage-3').eq(6).addClass('active');
			}
			if($(window).scrollTop() >= fold_7_start+136 && $(window).scrollTop() <= fold_7_start+140){
				$('.manage-3').removeClass('active');
				$('.manage-3').eq(7).addClass('active');
			}
			if($(window).scrollTop() >= fold_7_start+141 && $(window).scrollTop() <= fold_7_start+145){
				$('.manage-3').removeClass('active');
				$('.manage-3').eq(8).addClass('active');
			}
			if($(window).scrollTop() >= fold_7_start+146 && $(window).scrollTop() <= fold_7_start+150){
				$('.manage-3').removeClass('active');
				$('.manage-3').eq(9).addClass('active');
			}
			if($(window).scrollTop() >= fold_7_start+151 && $(window).scrollTop() <= fold_7_start+155){
				$('.manage-3').removeClass('active');
				$('.manage-3').eq(10).addClass('active');
			}
			if($(window).scrollTop() >= fold_7_start+156 && $(window).scrollTop() <= fold_7_start+160){
				$('.manage-3').removeClass('active');
				$('.manage-3').eq(11).addClass('active');
			}
			if($(window).scrollTop() >= fold_7_start+161 && $(window).scrollTop() <= fold_7_start+165){
				$('.manage-3').removeClass('active');
				$('.manage-3').eq(12).addClass('active');
			}
			if($(window).scrollTop() >= fold_7_start+166 && $(window).scrollTop() <= fold_7_start+170){
				$('.manage-3').removeClass('active');
				$('.manage-3').eq(13).addClass('active');
			}
			if($(window).scrollTop() >= fold_7_start+171 && $(window).scrollTop() <= fold_7_start+175){
				$('.manage-3').removeClass('active');
				$('.manage-3').eq(14).addClass('active');
			}
			if($(window).scrollTop() >= fold_7_start+176 && $(window).scrollTop() <= fold_7_start+180){
				$('.manage-3').removeClass('active');
				$('.manage-3').eq(15).addClass('active');
			}
			if($(window).scrollTop() >= fold_7_start+181 && $(window).scrollTop() <= fold_7_start+185){
				$('.manage-3').removeClass('active');
				$('.manage-3').eq(16).addClass('active');
			}
			if($(window).scrollTop() >= fold_7_start+186 && $(window).scrollTop() <= fold_7_start+190){
				$('.manage-3').removeClass('active');
				$('.manage-3').eq(17).addClass('active');
			}
			if($(window).scrollTop() >= fold_7_start+191 && $(window).scrollTop() <= fold_7_start+195){
				$('.manage-3').removeClass('active');
				$('.manage-3').eq(18).addClass('active');
			}
			if($(window).scrollTop() >= fold_7_start+196 && $(window).scrollTop() <= fold_7_start+200){
				$('.manage-3').removeClass('active');
				$('.manage-3').eq(19).addClass('active');
			}
			if($(window).scrollTop() >= fold_7_start+201 && $(window).scrollTop() <= fold_7_start+205){
				$('.manage-3').removeClass('active');
				$('.manage-3').eq(20).addClass('active');
			}
			if($(window).scrollTop() >= fold_7_start+206 && $(window).scrollTop() <= fold_7_start+210){
				$('.manage-3').removeClass('active');
				$('.manage-3').eq(21).addClass('active');
			}
			if($(window).scrollTop() >= fold_7_start+211 && $(window).scrollTop() <= fold_7_start+215){
				$('.manage-3').removeClass('active');
				$('.manage-3').eq(22).addClass('active');
			}
			if($(window).scrollTop() >= fold_7_start+216 && $(window).scrollTop() <= fold_7_start+220){
				$('.manage-3').removeClass('active');
				$('.manage-3').eq(23).addClass('active');
			}
			if($(window).scrollTop() >= fold_7_start+221 && $(window).scrollTop() <= fold_7_start+225){
				$('.manage-3').removeClass('active');
				$('.manage-3').eq(24).addClass('active');
			}
			if($(window).scrollTop() >= fold_7_start+226 && $(window).scrollTop() <= fold_7_start+230){
				$('.manage-3').removeClass('active');
				$('.manage-3').eq(25).addClass('active');
			}
			if($(window).scrollTop() >= fold_7_start+231 && $(window).scrollTop() <= fold_7_start+235){
				$('.manage-3').removeClass('active');
				$('.manage-3').eq(26).addClass('active');
			}
			if($(window).scrollTop() >= fold_7_start+236 && $(window).scrollTop() <= fold_7_start+240){
				$('.manage-3').removeClass('active');
				$('.manage-3').eq(27).addClass('active');
			}
			if($(window).scrollTop() >= fold_7_start+241 && $(window).scrollTop() <= fold_7_start+245){
				$('.manage-3').removeClass('active');
				$('.manage-3').eq(28).addClass('active');
			}
			if($(window).scrollTop() >= fold_7_start+246 && $(window).scrollTop() <= fold_7_start+250){
				$('.manage-3').removeClass('active');
				$('.manage-3').eq(29).addClass('active');
			}
			if($(window).scrollTop() >= fold_7_start+251 && $(window).scrollTop() <= fold_7_start+255){
				$('.manage-3').removeClass('active');
				$('.manage-3').eq(30).addClass('active');
			}
			if($(window).scrollTop() >= fold_7_start+256 && $(window).scrollTop() <= fold_7_start+260){
				$('.manage-3').removeClass('active');
				$('.manage-3').eq(31).addClass('active');
			}
			if($(window).scrollTop() >= fold_7_start+261 && $(window).scrollTop() <= fold_7_start+265){
				$('.manage-3').removeClass('active');
				$('.manage-3').eq(32).addClass('active');
			}
			if($(window).scrollTop() >= fold_7_start+266 && $(window).scrollTop() <= fold_7_start+270){
				$('.manage-3').removeClass('active');
				$('.manage-3').eq(33).addClass('active');
			}
			if($(window).scrollTop() >= fold_7_start+271){
				$('.manage-3').removeClass('active');
				$('.manage-3').eq(33).addClass('active');
			}
		
			
			
		
		}
		function fold_8_anim() {
			
				
			if($(window).scrollTop() <= fold_8_start){
				$('.fold-8').removeClass('active');
			}
			if($(window).scrollTop() >= fold_8_start && $(window).scrollTop() <= fold_8_start+25){
				$('.fold-8').removeClass('active');
				$('.fold-8').eq(0).addClass('active');
			}
			if($(window).scrollTop() >= fold_8_start+26 && $(window).scrollTop() <= fold_8_start+50){
				$('.fold-8').removeClass('active');
				$('.fold-8').eq(1).addClass('active');
			}
			if($(window).scrollTop() >= fold_8_start+51 && $(window).scrollTop() <= fold_8_start+75){
				$('.fold-8').removeClass('active');
				$('.fold-8').eq(2).addClass('active');
			}
			if($(window).scrollTop() >= fold_8_start+76 && $(window).scrollTop() <= fold_8_start+100){
				$('.fold-8').removeClass('active');
				$('.fold-8').eq(3).addClass('active');
			}
			if($(window).scrollTop() >= fold_8_start+101 && $(window).scrollTop() <= fold_8_start+125){
				$('.fold-8').removeClass('active');
				$('.fold-8').eq(4).addClass('active');
			}
			if($(window).scrollTop() >= fold_8_start+126 && $(window).scrollTop() <= fold_8_start+150){
				$('.fold-8').removeClass('active');
				$('.fold-8').eq(5).addClass('active');
			}
			if($(window).scrollTop() >= fold_8_start+151 && $(window).scrollTop() <= fold_8_start+175){
				$('.fold-8').removeClass('active');
				$('.fold-8').eq(6).addClass('active');
			}
			if($(window).scrollTop() >= fold_8_start+176 && $(window).scrollTop() <= fold_8_start+200){
				$('.fold-8').removeClass('active');
				$('.fold-8').eq(7).addClass('active');
			}
			
			
			// MAP
			if($(window).scrollTop() <= fold_8_start){
				$('.map-1').eq(0).addClass('active');
			}
			if($(window).scrollTop() >= fold_8_start && $(window).scrollTop() <= fold_8_start+250){
				$('.map-1').removeClass('active');
				$('.map-1').eq(0).addClass('active');
			}
			if($(window).scrollTop() >= fold_8_start+251 && $(window).scrollTop() <= fold_8_start+255){
				$('.map-1').removeClass('active');
				$('.map-1').eq(1).addClass('active');
			}
			if($(window).scrollTop() >= fold_8_start+256 && $(window).scrollTop() <= fold_8_start+260){
				$('.map-1').removeClass('active');
				$('.map-1').eq(2).addClass('active');
			}
			if($(window).scrollTop() >= fold_8_start+261 && $(window).scrollTop() <= fold_8_start+265){
				$('.map-1').removeClass('active');
				$('.map-1').eq(3).addClass('active');
			}
			if($(window).scrollTop() >= fold_8_start+266 && $(window).scrollTop() <= fold_8_start+270){
				$('.map-1').removeClass('active');
				$('.map-1').eq(4).addClass('active');
			}
			if($(window).scrollTop() >= fold_8_start+271 && $(window).scrollTop() <= fold_8_start+275){
				$('.map-1').removeClass('active');
				$('.map-1').eq(5).addClass('active');
			}
			if($(window).scrollTop() >= fold_8_start+276 && $(window).scrollTop() <= fold_8_start+280){
				$('.map-1').removeClass('active');
				$('.map-1').eq(6).addClass('active');
			}
			if($(window).scrollTop() >= fold_8_start+281 && $(window).scrollTop() <= fold_8_start+285){
				$('.map-1').removeClass('active');
				$('.map-1').eq(7).addClass('active');
			}
			if($(window).scrollTop() >= fold_8_start+286 && $(window).scrollTop() <= fold_8_start+290){
				$('.map-1').removeClass('active');
				$('.map-1').eq(8).addClass('active');
			}
			if($(window).scrollTop() >= fold_8_start+291 && $(window).scrollTop() <= fold_8_start+295){
				$('.map-1').removeClass('active');
				$('.map-1').eq(9).addClass('active');
			}
			if($(window).scrollTop() >= fold_8_start+296 && $(window).scrollTop() <= fold_8_start+300){
				$('.map-1').removeClass('active');
				$('.map-1').eq(10).addClass('active');
			}
			if($(window).scrollTop() >= fold_8_start+301 && $(window).scrollTop() <= fold_8_start+305){
				$('.map-1').removeClass('active');
				$('.map-1').eq(11).addClass('active');
			}
			if($(window).scrollTop() >= fold_8_start+306 && $(window).scrollTop() <= fold_8_start+310){
				$('.map-1').removeClass('active');
				$('.map-1').eq(12).addClass('active');
			}
			if($(window).scrollTop() >= fold_8_start+311 && $(window).scrollTop() <= fold_8_start+315){
				$('.map-1').removeClass('active');
				$('.map-1').eq(13).addClass('active');
			}
			if($(window).scrollTop() >= fold_8_start+316 && $(window).scrollTop() <= fold_8_start+320){
				$('.map-1').removeClass('active');
				$('.map-1').eq(14).addClass('active');
			}
			if($(window).scrollTop() >= fold_8_start+321 && $(window).scrollTop() <= fold_8_start+325){
				$('.map-1').removeClass('active');
				$('.map-1').eq(15).addClass('active');
			}
			if($(window).scrollTop() >= fold_8_start+326 && $(window).scrollTop() <= fold_8_start+330){
				$('.map-1').removeClass('active');
				$('.map-1').eq(16).addClass('active');
			}
			if($(window).scrollTop() >= fold_8_start+331 && $(window).scrollTop() <= fold_8_start+335){
				$('.map-1').removeClass('active');
				$('.map-1').eq(17).addClass('active');
			}
			if($(window).scrollTop() >= fold_8_start+336 && $(window).scrollTop() <= fold_8_start+340){
				$('.map-1').removeClass('active');
				$('.map-1').eq(18).addClass('active');
			}
			if($(window).scrollTop() >= fold_8_start+341 && $(window).scrollTop() <= fold_8_start+345){
				$('.map-1').removeClass('active');
				$('.map-1').eq(19).addClass('active');
			}
			if($(window).scrollTop() >= fold_8_start+346 && $(window).scrollTop() <= fold_8_start+350){
				$('.map-1').removeClass('active');
				$('.map-1').eq(20).addClass('active');
			}
			if($(window).scrollTop() >= fold_8_start+351){
				$('.map-1').removeClass('active');
				$('.map-1').eq(20).addClass('active');
			}
			
			
			// PIN
			if($(window).scrollTop() <= fold_8_start){
				$('.pin').css({'top':-65,'opacity':0});
			}
			if($(window).scrollTop() >= fold_8_start && $(window).scrollTop() <= fold_8_start+250){
				$('.pin').css({'top':-55,'opacity':0});
			}
			if($(window).scrollTop() >= fold_8_start+251 && $(window).scrollTop() <= fold_8_start+255){
				$('.pin').css({'top':-45,'opacity':0});
			}
			if($(window).scrollTop() >= fold_8_start+256 && $(window).scrollTop() <= fold_8_start+260){
				$('.pin').css({'top':-35,'opacity':0});
			}
			if($(window).scrollTop() >= fold_8_start+261 && $(window).scrollTop() <= fold_8_start+265){
				$('.pin').css({'top':-25,'opacity':0});
			}
			if($(window).scrollTop() >= fold_8_start+266 && $(window).scrollTop() <= fold_8_start+270){
				$('.pin').css({'top':-15,'opacity':0});
			}
			if($(window).scrollTop() >= fold_8_start+271 && $(window).scrollTop() <= fold_8_start+275){
				$('.pin').css({'top':-5,'opacity':1});
			}
			if($(window).scrollTop() >= fold_8_start+276 && $(window).scrollTop() <= fold_8_start+280){
				$('.pin').css({'top':15,'opacity':1});
			}
			if($(window).scrollTop() >= fold_8_start+281 && $(window).scrollTop() <= fold_8_start+285){
				$('.pin').css({'top':25,'opacity':1});
			}
			if($(window).scrollTop() >= fold_8_start+286 && $(window).scrollTop() <= fold_8_start+290){
				$('.pin').css({'top':35,'opacity':1});
			}
			if($(window).scrollTop() >= fold_8_start+291 && $(window).scrollTop() <= fold_8_start+295){
				$('.pin').css({'top':45,'opacity':1});
			}
			if($(window).scrollTop() >= fold_8_start+296 && $(window).scrollTop() <= fold_8_start+300){
				$('.pin').css({'top':55,'opacity':1});
			}
			if($(window).scrollTop() >= fold_8_start+301 && $(window).scrollTop() <= fold_8_start+305){
				$('.pin').css({'top':65,'opacity':1});
			}
			if($(window).scrollTop() >= fold_8_start+306 && $(window).scrollTop() <= fold_8_start+310){
				$('.pin').css({'top':75,'opacity':1});
			}
			if($(window).scrollTop() >= fold_8_start+311 && $(window).scrollTop() <= fold_8_start+315){
				$('.pin').css({'top':85,'opacity':1});
			}
			if($(window).scrollTop() >= fold_8_start+316 && $(window).scrollTop() <= fold_8_start+320){
				$('.pin').css({'top':95,'opacity':1});
			}
			if($(window).scrollTop() >= fold_8_start+321 && $(window).scrollTop() <= fold_8_start+325){
				$('.pin').css({'top':105,'opacity':1});
			}
			if($(window).scrollTop() >= fold_8_start+326 && $(window).scrollTop() <= fold_8_start+330){
				$('.pin').css({'top':115,'opacity':1});
			}
			if($(window).scrollTop() >= fold_8_start+331 && $(window).scrollTop() <= fold_8_start+335){
				$('.pin').css({'top':125,'opacity':1});
			}
			if($(window).scrollTop() >= fold_8_start+336 && $(window).scrollTop() <= fold_8_start+340){
				$('.pin').css({'top':145,'opacity':1});
			}
			if($(window).scrollTop() >= fold_8_start+341 && $(window).scrollTop() <= fold_8_start+345){
				$('.pin').css({'top':155,'opacity':1});
			}
			if($(window).scrollTop() >= fold_8_start+346 && $(window).scrollTop() <= fold_8_start+350){
				$('.pin').css({'top':165,'opacity':1});
			}
			if($(window).scrollTop() >= fold_8_start+351 && $(window).scrollTop() <= fold_8_start+355){
				$('.pin').css({'top':175,'opacity':1});
			}
			if($(window).scrollTop() >= fold_8_start+356 && $(window).scrollTop() <= fold_8_start+360){
				$('.pin').css({'top':185,'opacity':1});
			}
			if($(window).scrollTop() >= fold_8_start+361 && $(window).scrollTop() <= fold_8_start+365){
				$('.pin').css({'top':195,'opacity':1});
			}
			if($(window).scrollTop() >= fold_8_start+366 && $(window).scrollTop() <= fold_8_start+370){
				$('.pin').css({'top':200,'opacity':1});
			}
			if($(window).scrollTop() >= fold_8_start+370){
				$('.pin').css({'top':200,'opacity':1});
			}
		
		}

  }
}


export default App;

