import React, { Component } from 'react';
import fold_4_mob from './images/fold-4-mob.png';
import build_bg from './images/build-bg.png';
import build_1 from './images/build-1.png';
import build_2 from './images/build-2.png';
import build_3 from './images/build-3.png';
import build_4 from './images/build-4.png';

import build_handle_1 from './images/3x_16/3x_16000.png';
import build_handle_2 from './images/3x_16/3x_16017.png';
import build_handle_3 from './images/3x_16/3x_16021.png';
import build_handle_4 from './images/3x_16/3x_16025.png';
import build_handle_5 from './images/3x_16/3x_16026.png';
import build_handle_6 from './images/3x_16/3x_16027.png';
import build_handle_7 from './images/3x_16/3x_16028.png';
import build_handle_8 from './images/3x_16/3x_16029.png';
import build_handle_9 from './images/3x_16/3x_16030.png';
import build_handle_10 from './images/3x_16/3x_16031.png';
import build_handle_11 from './images/3x_16/3x_16032.png';
import build_handle_12 from './images/3x_16/3x_16033.png';
import build_handle_13 from './images/3x_16/3x_16036.png';
import build_handle_14 from './images/3x_16/3x_16037.png';
import build_handle_15 from './images/3x_16/3x_16038.png';
import build_handle_16 from './images/3x_16/3x_16039.png';
import build_handle_17 from './images/3x_16/3x_16040.png';
import build_handle_18 from './images/3x_16/3x_16041.png';
import build_handle_19 from './images/3x_16/3x_16045.png';

class Fold4 extends Component {
  render() {
    return (
		<div className="fold" id="fold_4">
        	<div className="fold-left w-30">
            	<div>
                	<div className="small-heading">ASSET ALLOCATION</div>
                	<div className="sub-heading">We build.</div>
                    <div className="sub-desc">We’ll personalize a portfolio mix of stocks and bonds that meets your personal needs and preferences.</div>
					<img src={fold_4_mob} className="mob-img mobile" alt="" />
                    <span rel="asset-wrap" className="learn-more-link">LEARN MORE</span>
                </div>
                
            </div>
            <div className="fold-right w-70 desktop">
            	<div className="build-wrap">
                	<img src="" img-url={build_bg} alt="" />
                    <div className="build-handle">
                    	<img src="" img-url={build_handle_1} className="build-handle-img" alt="" />
                        <img src="" img-url={build_handle_2} className="build-handle-img" alt="" />
                        <img src="" img-url={build_handle_3} className="build-handle-img" alt="" />
                        <img src="" img-url={build_handle_4} className="build-handle-img" alt="" />
                        <img src="" img-url={build_handle_5} className="build-handle-img" alt="" />
                        <img src="" img-url={build_handle_6} className="build-handle-img" alt="" />
                        <img src="" img-url={build_handle_7} className="build-handle-img" alt="" />
                        <img src="" img-url={build_handle_8} className="build-handle-img" alt="" />
                        <img src="" img-url={build_handle_9} className="build-handle-img" alt="" />
                        <img src="" img-url={build_handle_10} className="build-handle-img" alt="" />
                        <img src="" img-url={build_handle_11} className="build-handle-img" alt="" />
                        <img src="" img-url={build_handle_12} className="build-handle-img" alt="" />
                        <img src="" img-url={build_handle_13} className="build-handle-img" alt="" />
                        <img src="" img-url={build_handle_14} className="build-handle-img" alt="" />
                        <img src="" img-url={build_handle_15} className="build-handle-img" alt="" />
                        <img src="" img-url={build_handle_16} className="build-handle-img" alt="" />
                        <img src="" img-url={build_handle_17} className="build-handle-img" alt="" />
                        <img src="" img-url={build_handle_18} className="build-handle-img" alt="" />
                        <img src="" img-url={build_handle_19} className="build-handle-img" alt="" />

                    </div>
                    <img src="" img-url={build_4} className="build-4" alt="" />
                    <img src="" img-url={build_1} className="build-1" alt="" />
                    <img src="" img-url={build_2} className="build-2" alt="" />
                    <img src="" img-url={build_3} className="build-3" alt="" />
                    
                </div>
            </div>
        </div>
    );
  }
}


export default Fold4;
