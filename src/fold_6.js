import React, { Component } from 'react';
import fold_6_mob from './images/fold-6-mob.png';

import chart_1_1 from './images/3x_28/3x_28002.png';
import chart_1_2 from './images/3x_28/3x_28005.png';
import chart_1_3 from './images/3x_28/3x_28008.png';
import chart_1_4 from './images/3x_28/3x_28009.png';
import chart_1_5 from './images/3x_28/3x_28011.png';
import chart_1_6 from './images/3x_28/3x_28012.png';
import chart_1_7 from './images/3x_28/3x_28013.png';
import chart_1_8 from './images/3x_28/3x_28014.png';
import chart_1_9 from './images/3x_28/3x_28015.png';
import chart_1_10 from './images/3x_28/3x_28016.png';
import chart_1_11 from './images/3x_28/3x_28017.png';
import chart_1_12 from './images/3x_28/3x_28018.png';
import chart_1_13 from './images/3x_28/3x_28019.png';
import chart_1_14 from './images/3x_28/3x_28020.png';
import chart_1_15 from './images/3x_28/3x_28021.png';
import chart_1_16 from './images/3x_28/3x_28022.png';
import chart_1_17 from './images/3x_28/3x_28023.png';
import chart_1_18 from './images/3x_28/3x_28024.png';
import chart_1_19 from './images/3x_28/3x_28025.png';
import chart_1_20 from './images/3x_28/3x_28026.png';
import chart_1_21 from './images/3x_28/3x_28027.png';
import chart_1_22 from './images/3x_28/3x_28028.png';
import chart_1_23 from './images/3x_28/3x_28029.png';
import chart_1_24 from './images/3x_28/3x_28030.png';

import chart_2_1 from './images/3x_29/3x_29003.png';
import chart_2_2 from './images/3x_29/3x_29004.png';
import chart_2_3 from './images/3x_29/3x_29005.png';
import chart_2_4 from './images/3x_29/3x_29006.png';
import chart_2_5 from './images/3x_29/3x_29007.png';
import chart_2_6 from './images/3x_29/3x_29008.png';
import chart_2_7 from './images/3x_29/3x_29009.png';
import chart_2_8 from './images/3x_29/3x_29010.png';
import chart_2_9 from './images/3x_29/3x_29011.png';
import chart_2_10 from './images/3x_29/3x_29012.png';
import chart_2_11 from './images/3x_29/3x_29013.png';
import chart_2_12 from './images/3x_29/3x_29014.png';
import chart_2_13 from './images/3x_29/3x_29015.png';
import chart_2_14 from './images/3x_29/3x_29016.png';
import chart_2_15 from './images/3x_29/3x_29017.png';
import chart_2_16 from './images/3x_29/3x_29018.png';
import chart_2_17 from './images/3x_29/3x_29019.png';
import chart_2_18 from './images/3x_29/3x_29020.png';
import chart_2_19 from './images/3x_29/3x_29021.png';
import chart_2_20 from './images/3x_29/3x_29022.png';
import chart_2_21 from './images/3x_29/3x_29023.png';
import chart_2_22 from './images/3x_29/3x_29024.png';
import chart_2_23 from './images/3x_29/3x_29025.png';
import chart_2_24 from './images/3x_29/3x_29026.png';
import chart_2_25 from './images/3x_29/3x_29027.png';
import chart_2_26 from './images/3x_29/3x_29028.png';
import chart_2_27 from './images/3x_29/3x_29029.png';
import chart_2_28 from './images/3x_29/3x_29030.png';
import chart_2_29 from './images/3x_29/3x_29031.png';
import chart_2_30 from './images/3x_29/3x_29032.png';
import chart_2_31 from './images/3x_29/3x_29033.png';
import chart_2_32 from './images/3x_29/3x_29034.png';
import chart_2_33 from './images/3x_29/3x_29035.png';
import chart_2_34 from './images/3x_29/3x_29036.png';
import chart_2_35 from './images/3x_29/3x_29037.png';
import chart_2_36 from './images/3x_29/3x_29038.png';
import chart_2_37 from './images/3x_29/3x_29039.png';
import chart_2_38 from './images/3x_29/3x_29040.png';
import chart_2_39 from './images/3x_29/3x_29041.png';
import chart_2_40 from './images/3x_29/3x_29042.png';
import chart_2_41 from './images/3x_29/3x_29043.png';
import chart_2_42 from './images/3x_29/3x_29044.png';
import chart_2_43 from './images/3x_29/3x_29045.png';

import chart_3_1 from './images/3x_30/3x_30049.png';
import chart_3_2 from './images/3x_30/3x_30050.png';
import chart_3_3 from './images/3x_30/3x_30051.png';
import chart_3_4 from './images/3x_30/3x_30052.png';
import chart_3_5 from './images/3x_30/3x_30053.png';
import chart_3_6 from './images/3x_30/3x_30054.png';
import chart_3_7 from './images/3x_30/3x_30055.png';
import chart_3_8 from './images/3x_30/3x_30056.png';
import chart_3_9 from './images/3x_30/3x_30057.png';
import chart_3_10 from './images/3x_30/3x_30058.png';
import chart_3_11 from './images/3x_30/3x_30059.png';
import chart_3_12 from './images/3x_30/3x_30060.png';
import chart_3_13 from './images/3x_30/3x_30061.png';
import chart_3_14 from './images/3x_30/3x_30062.png';
import chart_3_15 from './images/3x_30/3x_30063.png';
import chart_3_16 from './images/3x_30/3x_30064.png';
import chart_3_17 from './images/3x_30/3x_30065.png';
import chart_3_18 from './images/3x_30/3x_30066.png';
import chart_3_19 from './images/3x_30/3x_30067.png';
import chart_3_20 from './images/3x_30/3x_30068.png';
import chart_3_21 from './images/3x_30/3x_30069.png';
import chart_3_22 from './images/3x_30/3x_30070.png';
import chart_3_23 from './images/3x_30/3x_30071.png';
import chart_3_24 from './images/3x_30/3x_30072.png';
import chart_3_25 from './images/3x_30/3x_30073.png';
import chart_3_26 from './images/3x_30/3x_30074.png';
import chart_3_27 from './images/3x_30/3x_30075.png';
import chart_3_28 from './images/3x_30/3x_30076.png';
import chart_3_29 from './images/3x_30/3x_30077.png';
import chart_3_30 from './images/3x_30/3x_30078.png';
import chart_3_31 from './images/3x_30/3x_30079.png';
import chart_3_32 from './images/3x_30/3x_30080.png';
import chart_3_33 from './images/3x_30/3x_30081.png';
import chart_3_34 from './images/3x_30/3x_30082.png';
import chart_3_35 from './images/3x_30/3x_30083.png';
import chart_3_36 from './images/3x_30/3x_30084.png';
import chart_3_37 from './images/3x_30/3x_30085.png';
import chart_3_38 from './images/3x_30/3x_30086.png';
import chart_3_39 from './images/3x_30/3x_30087.png';
import chart_3_40 from './images/3x_30/3x_30088.png';
import chart_3_41 from './images/3x_30/3x_30089.png';

class Fold6 extends Component {
  render() {
    return (
		<div className="fold" id="fold_6">
        	<div className="fold-left w-30">
            	<div>
                	<div className="small-heading">PERSONALIZED PORTFOLIO</div>
                	<div className="sub-heading">We invest.</div>
                    <div className="sub-desc">You decide how much to invest, and we'll buy shares or even fractions of shares on your behalf, all for free and with no investment minimums.</div>
					<img src={fold_6_mob} className="mob-img mobile" alt="" />
                    <span rel="personalized-wrap" className="learn-more-link">LEARN MORE</span>
                </div>
                
            </div>
            <div className="fold-right w-70 desktop">
            	<div className="chart-wrap">
                	<div className="chart-1-wrap">
                    	<img src="" img-url={chart_1_1} className="chart-1" alt="" />
						<img src="" img-url={chart_1_2} className="chart-1" alt="" />
						<img src="" img-url={chart_1_3} className="chart-1" alt="" />
						<img src="" img-url={chart_1_4} className="chart-1" alt="" />
						<img src="" img-url={chart_1_5} className="chart-1" alt="" />
						<img src="" img-url={chart_1_6} className="chart-1" alt="" />
						<img src="" img-url={chart_1_7} className="chart-1" alt="" />
						<img src="" img-url={chart_1_8} className="chart-1" alt="" />
						<img src="" img-url={chart_1_9} className="chart-1" alt="" />
						<img src="" img-url={chart_1_10} className="chart-1" alt="" />
						<img src="" img-url={chart_1_11} className="chart-1" alt="" />
						<img src="" img-url={chart_1_12} className="chart-1" alt="" />
						<img src="" img-url={chart_1_13} className="chart-1" alt="" />
						<img src="" img-url={chart_1_14} className="chart-1" alt="" />
						<img src="" img-url={chart_1_15} className="chart-1" alt="" />
						<img src="" img-url={chart_1_16} className="chart-1" alt="" />
						<img src="" img-url={chart_1_17} className="chart-1" alt="" />
						<img src="" img-url={chart_1_18} className="chart-1" alt="" />
						<img src="" img-url={chart_1_19} className="chart-1" alt="" />
						<img src="" img-url={chart_1_20} className="chart-1" alt="" />
						<img src="" img-url={chart_1_21} className="chart-1" alt="" />
						<img src="" img-url={chart_1_22} className="chart-1" alt="" />
						<img src="" img-url={chart_1_23} className="chart-1" alt="" />
						<img src="" img-url={chart_1_24} className="chart-1" alt="" />
                    </div>
                	<div className="chart-2-wrap">
						<img src="" img-url={chart_2_1} className="chart-2" alt="" />
						<img src="" img-url={chart_2_2} className="chart-2" alt="" />
						<img src="" img-url={chart_2_3} className="chart-2" alt="" />
						<img src="" img-url={chart_2_4} className="chart-2" alt="" />
						<img src="" img-url={chart_2_5} className="chart-2" alt="" />
						<img src="" img-url={chart_2_6} className="chart-2" alt="" />
						<img src="" img-url={chart_2_7} className="chart-2" alt="" />
						<img src="" img-url={chart_2_8} className="chart-2" alt="" />
						<img src="" img-url={chart_2_9} className="chart-2" alt="" />
						<img src="" img-url={chart_2_10} className="chart-2" alt="" />
						<img src="" img-url={chart_2_11} className="chart-2" alt="" />
						<img src="" img-url={chart_2_12} className="chart-2" alt="" />
						<img src="" img-url={chart_2_13} className="chart-2" alt="" />
						<img src="" img-url={chart_2_14} className="chart-2" alt="" />
						<img src="" img-url={chart_2_15} className="chart-2" alt="" />
						<img src="" img-url={chart_2_16} className="chart-2" alt="" />
						<img src="" img-url={chart_2_17} className="chart-2" alt="" />
						<img src="" img-url={chart_2_18} className="chart-2" alt="" />
						<img src="" img-url={chart_2_19} className="chart-2" alt="" />
						<img src="" img-url={chart_2_20} className="chart-2" alt="" />
						<img src="" img-url={chart_2_21} className="chart-2" alt="" />
						<img src="" img-url={chart_2_22} className="chart-2" alt="" />
						<img src="" img-url={chart_2_23} className="chart-2" alt="" />
						<img src="" img-url={chart_2_24} className="chart-2" alt="" />
						<img src="" img-url={chart_2_25} className="chart-2" alt="" />
						<img src="" img-url={chart_2_26} className="chart-2" alt="" />
						<img src="" img-url={chart_2_27} className="chart-2" alt="" />
						<img src="" img-url={chart_2_28} className="chart-2" alt="" />
						<img src="" img-url={chart_2_29} className="chart-2" alt="" />
						<img src="" img-url={chart_2_30} className="chart-2" alt="" />
						<img src="" img-url={chart_2_31} className="chart-2" alt="" />
						<img src="" img-url={chart_2_32} className="chart-2" alt="" />
						<img src="" img-url={chart_2_33} className="chart-2" alt="" />
						<img src="" img-url={chart_2_34} className="chart-2" alt="" />
						<img src="" img-url={chart_2_35} className="chart-2" alt="" />
						<img src="" img-url={chart_2_36} className="chart-2" alt="" />
						<img src="" img-url={chart_2_37} className="chart-2" alt="" />
						<img src="" img-url={chart_2_38} className="chart-2" alt="" />
						<img src="" img-url={chart_2_39} className="chart-2" alt="" />
						<img src="" img-url={chart_2_40} className="chart-2" alt="" />
						<img src="" img-url={chart_2_41} className="chart-2" alt="" />
						<img src="" img-url={chart_2_42} className="chart-2" alt="" />
						<img src="" img-url={chart_2_43} className="chart-2" alt="" />

                    </div>
                    <div className="chart-3-wrap">
						<img src="" img-url={chart_3_1} className="chart-3" alt="" />
						<img src="" img-url={chart_3_2} className="chart-3" alt="" />
						<img src="" img-url={chart_3_3} className="chart-3" alt="" />
						<img src="" img-url={chart_3_4} className="chart-3" alt="" />
						<img src="" img-url={chart_3_5} className="chart-3" alt="" />
						<img src="" img-url={chart_3_6} className="chart-3" alt="" />
						<img src="" img-url={chart_3_7} className="chart-3" alt="" />
						<img src="" img-url={chart_3_8} className="chart-3" alt="" />
						<img src="" img-url={chart_3_9} className="chart-3" alt="" />
						<img src="" img-url={chart_3_10} className="chart-3" alt="" />
						<img src="" img-url={chart_3_11} className="chart-3" alt="" />
						<img src="" img-url={chart_3_12} className="chart-3" alt="" />
						<img src="" img-url={chart_3_13} className="chart-3" alt="" />
						<img src="" img-url={chart_3_14} className="chart-3" alt="" />
						<img src="" img-url={chart_3_15} className="chart-3" alt="" />
						<img src="" img-url={chart_3_16} className="chart-3" alt="" />
						<img src="" img-url={chart_3_17} className="chart-3" alt="" />
						<img src="" img-url={chart_3_18} className="chart-3" alt="" />
						<img src="" img-url={chart_3_19} className="chart-3" alt="" />
						<img src="" img-url={chart_3_20} className="chart-3" alt="" />
						<img src="" img-url={chart_3_21} className="chart-3" alt="" />
						<img src="" img-url={chart_3_22} className="chart-3" alt="" />
						<img src="" img-url={chart_3_23} className="chart-3" alt="" />
						<img src="" img-url={chart_3_24} className="chart-3" alt="" />
						<img src="" img-url={chart_3_25} className="chart-3" alt="" />
						<img src="" img-url={chart_3_26} className="chart-3" alt="" />
						<img src="" img-url={chart_3_27} className="chart-3" alt="" />
						<img src="" img-url={chart_3_28} className="chart-3" alt="" />
						<img src="" img-url={chart_3_29} className="chart-3" alt="" />
						<img src="" img-url={chart_3_30} className="chart-3" alt="" />
						<img src="" img-url={chart_3_31} className="chart-3" alt="" />
						<img src="" img-url={chart_3_32} className="chart-3" alt="" />
						<img src="" img-url={chart_3_33} className="chart-3" alt="" />
						<img src="" img-url={chart_3_34} className="chart-3" alt="" />
						<img src="" img-url={chart_3_35} className="chart-3" alt="" />
						<img src="" img-url={chart_3_36} className="chart-3" alt="" />
						<img src="" img-url={chart_3_37} className="chart-3" alt="" />
						<img src="" img-url={chart_3_38} className="chart-3" alt="" />
						<img src="" img-url={chart_3_39} className="chart-3" alt="" />
						<img src="" img-url={chart_3_40} className="chart-3" alt="" />
						<img src="" img-url={chart_3_41} className="chart-3" alt="" />
                        
                    </div>
                </div>
            </div>
        </div>
    );
  }
}


export default Fold6;
