import React, { Component } from 'react';
import google_btn from './images/google-btn.png';
import app_btn from './images/app-btn.png';
import fb_icon from './images/facebook.png';
import li_icon from './images/linkedin.png';
import tw_icon from './images/twitter.png';
import footer_icon from './images/lionheadf.png';


class Footer extends Component {
  render() {
    return (
      	<footer > 
            <div className="inner-wrap">
                <div className="clearfix">
                    <ul className="footer-left" >
                        <li className="heading">CONTACT</li>
                        <li className="address"><span>MoneyLion, Inc.<br />P.O. Box 1247<br />Sandy, UT 84091-1547<br />1-888-704-6970</span><br /><a className="link-0-424" href="mailto:support@moneylion.com">support@moneylion.com</a></li>
                    </ul>
                    <div className="footer-right">
                        <ul>
                            <li className="heading">COMPANY</li>
                            <li><a href="https://www.moneylion.com/about" target="_blank" rel="noopener noreferrer">About</a></li>
                            <li><a href="https://blog.moneylion.com/" target="_blank" rel="noopener noreferrer">Blog</a></li>
                            <li><a href="https://support.moneylion.com/" target="_blank" rel="noopener noreferrer">Support</a></li>
                            <li><a href="https://www.moneylion.com/legal" target="_blank" rel="noopener noreferrer">Legal</a></li>
                        </ul>
                    
                        <ul>
                            <li className="heading">MEMBERSHIPS</li>
                            <li><a href="https://www.moneylion.com/core" target="_blank" rel="noopener noreferrer">Core</a></li>
                            <li className="parent"><a href="https://www.moneylion.com/plus" target="_blank" rel="noopener noreferrer">Plus</a>
                                <ul>
                                    <li><a href="https://www.moneylion.com/plus/membership" target="_blank" rel="noopener noreferrer">Membership</a></li>
                                    <li><a href="https://www.moneylion.com/plus/borrowing" target="_blank" rel="noopener noreferrer">Borrowing</a></li>
                                    <li><a href="https://www.moneylion.com/plus/perks" target="_blank" rel="noopener noreferrer">Perks</a></li>
                                </ul>                                    
                            </li>
                        </ul>
                        
                        <ul>
                            <li className="heading">PRODUCTS</li>
                            <li><a href="/plus/investing" target="_blank" rel="noopener noreferrer">Investing</a></li>
                            <li><a href="/plus/borrowing" target="_blank" rel="noopener noreferrer">Loans</a></li>
                            <li><a href="/rewards" target="_blank" rel="noopener noreferrer">Rewards</a></li>
                            <li><a href="/free-credit-monitoring" target="_blank" rel="noopener noreferrer">Free Credit Monitoring</a></li>
                            <li><a href="/personal-finance-mobile-app" target="_blank" rel="noopener noreferrer">Mobile App</a></li>
                        </ul>
                    </div>
                </div>
                <div className="clearfix">            	
                    <div className="footer-links">
                        <a href="https://bnc.lt/E1yh/4FtmTYT4MJ" target="_blank" rel="noopener noreferrer"><img src={google_btn} alt="" /></a>
                        <a href="https://bnc.lt/E1yh/4FtmTYT4MJ" target="_blank" rel="noopener noreferrer"><img src={app_btn} alt="" /></a>
                        <div className="social-icons">
                        <a href="https://www.facebook.com/moneylion" target="_blank" rel="noopener noreferrer"><img src={fb_icon} alt="" /></a>
                        <a href="https://www.linkedin.com/company/moneylion" target="_blank" rel="noopener noreferrer"><img src={li_icon} alt="" /></a>
                        <a href="https://twitter.com/moneylion" target="_blank" rel="noopener noreferrer"><img src={tw_icon} alt="" /></a>
                        </div>
                    </div>
                </div>
                <div className="clearfix footer-desc">
                
                    <div className="footer-left">
                         <img src={footer_icon} alt="" />
                        <div className="footer-copyright">&copy; 2013 - 2018, MoneyLion, inc.<br />All Rights Reserved.</div>
                    </div>
                    <div className="footer-right">   
                        <p>MoneyLion Banking account provided by partner bank, Member FDIC. Cash Advance requires MoneyLion Bank Account and Direct Deposit. See <a href="https://moneylion.helpshift.com/a/moneylion-checking/?p=web" target="_blank" rel="noopener noreferrer">Banking and Cash Advance FAQs</a> for more information.</p>
                        <p>Investment Accounts Are Not FDIC Insured • No Bank Guarantee • May Lose Value. For important information and disclaimers relating to the MoneyLion Investment Account, see <a href="https://moneylion.helpshift.com/a/moneylion-plus-1518215303/?p=web&s=moneylion-plus-investment-account" target="_blank" rel="noopener noreferrer">Investment Account FAQs</a> and <a href="https://www.moneylion.com/adv" target="_blank" rel="noopener noreferrer">FORM ADV</a>. *Broker-Dealer charges a $0.25 withdrawal charge.</p> 
                        <p>"America&rsquo;s Most Powerful Financial Membership<sup className="small">SM</sup>" is based on our analysis of membership programs and products offered by competing financial institutions. <a href="https://moneylion.helpshift.com/a/moneylion-plus-1518215303/?p=web&s=plus-terms-and-conditions&f=america-s-most-powerful-financial-membership&l=en" target="_blank" rel="noopener noreferrer">Learn more</a></p>           
                        <p>All loans in the USA with an APR of 5.99% are made by either exempt or state-licensed subsidiaries or First Electronic Bank, a Utah Industrial Bank.</p>
                        <p>MoneyLion Plus costs $29 a month, but you can get that entire fee back because we add $1 cashback to your investment account every day you swipe through your financial cards in the MoneyLion app! As a member, you must also save $50 a month into your MoneyLion Plus investment account.</p>
                        <p>See Membership Agreement, related agreements and Terms and Conditions for full terms.</p>
                        <p>** MoneyLion clients have linked over 2 million external accounts to the MoneyLion platform, and activity in those accounts represents an aggregate of approximately $89 billion in transactions.</p>
                        <p>✝ <a href="https://moneylion.helpshift.com/a/moneylion-plus-1518215303/?p=web&l=en&s=plus-terms-and-conditions&f=how-is-the-annual-value-calculated-for-core-and-plus-membership" target="_blank" rel="noopener noreferrer">Learn more</a> about how we calculate savings.</p>
                    </div>
                    <div className="clear-both"></div>
                </div> 
                </div>               
        </footer>
    );
  }
}


export default Footer;
