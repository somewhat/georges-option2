import React, { Component } from 'react';
import fold_2_mob from './images/fold-2-mob.png';
import cloud_2 from './images/cloud-2.png';
import flight from './images/flight.png';
import cloud_1 from './images/cloud-1.png';
import fold_2_bg from './images/fold-2-bg.png';

class Fold2 extends Component {
  render() {
    return (
		<div className="fold" id="fold_2">
        	<div className="fold-left w-30">
            	<div>
                	<div className="sub-heading">We’ll put <span className="green">George</span> to work for you.</div>
                    <div className="sub-desc">Earning money is hard work, but investing it is easy with a zero-fee managed investment account from MoneyLion. Here’s how it works.</div>
                </div>
                <img src={fold_2_mob} className="mob-img mobile" alt="" />
            </div>
            <div className="fold-right w-70 desktop">
            	<div className="fold-2-inner-wrap">
                    
                    <img src={cloud_2} className="cloud-2" alt="" />
                    <img src={flight} className="flight" alt="" />
                    <img src={cloud_1} className="cloud-1" alt="" />
                    <img src={fold_2_bg} className="fold-2-bg"  alt="" />
                </div>
            </div>
        </div>
    );
  }
}


export default Fold2;
