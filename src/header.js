import React, { Component } from 'react';
import logo from './images/logo-1.png';

class Header extends Component {
  render() {
    return (
		<div>
		<div className="loader"><div className="loader-txt">MoneyLion</div></div>
		<header>
        	<div className="inner-wrap">
                <a href="https://www.moneylion.com/" className="logo" target="_blank" rel="noopener noreferrer"><img src={logo} alt=""/></a>
                <div className="menu-toggle"><span></span></div>
                <a href="https://bnc.lt/E1yh/4FtmTYT4MJ" target="_blank" rel="noopener noreferrer" className="join-link">JOIN</a>
                <nav>
                    <ul>
                        <li className="parent"><span>Memberships</span>
                            <ul>
                                <li><a href="https://www.moneylion.com/core" target="_blank" rel="noopener noreferrer">Core</a></li>
                                <li className="parent"><a href="https://www.moneylion.com/plus" target="_blank" rel="noopener noreferrer">Plus</a>
                                    <ul>
                                        <li><a href="https://www.moneylion.com/plus/membership" target="_blank" rel="noopener noreferrer">Membership</a></li>
                                        <li><a href="https://www.moneylion.com/plus/borrowing" target="_blank" rel="noopener noreferrer">Borrowing</a></li>
                                        <li><a href="https://www.moneylion.com/plus/perks" target="_blank" rel="noopener noreferrer">Perks</a></li>
                                    </ul>                                    
                                </li>
                            </ul>                            
                        </li>
                        <li className="parent"><span>Products</span>
                            <ul>
                                <li><a  href="https://www.moneylion.com/investing" target="_blank" rel="noopener noreferrer">Investing</a></li>
                                <li><a href="https://www.moneylion.com/plus/borrowing" target="_blank" rel="noopener noreferrer">Loans</a></li>
                                <li><a href="https://www.moneylion.com/rewards" target="_blank" rel="noopener noreferrer">Rewards</a></li>
                                <li><a href="https://www.moneylion.com/free-credit-monitoring" target="_blank" rel="noopener noreferrer">Free Credit Monitoring</a></li>
                                <li><a href="https://www.moneylion.com/personal-finance-mobile-app" target="_blank" rel="noopener noreferrer">Mobile App</a></li>
                            </ul>
                        </li>
                        <li><span>For Business</span></li>
                        <li className="parent"><span>Company</span>
                            <ul>
                                <li><a href="https://www.moneylion.com/about" target="_blank" rel="noopener noreferrer">About</a></li>
                                <li><a href="https://blog.moneylion.com" target="_blank" rel="noopener noreferrer">Blog</a></li>
                                <li><a href="https://support.moneylion.com" target="_blank" rel="noopener noreferrer">Support</a></li>
                                <li><a href="https://www.moneylion.com/legal" target="_blank" rel="noopener noreferrer">Legal</a></li>
                            </ul>                            
                        </li> 
                    </ul>                    
                </nav>
			</div>                
        </header>
		</div>
    );
  }
}


export default Header;
