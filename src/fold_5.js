import React, { Component } from 'react';
import fold_5_mob from './images/fold-5-mob.png';
import car from './images/car.png';
import boxes from './images/boxes.png';
import measurment_rod from './images/measurment-rod.png';

import machine_1 from './images/3x_23/3x_23024.png';
import machine_2 from './images/3x_23/3x_23027.png';
import machine_3 from './images/3x_23/3x_23030.png';
import machine_4 from './images/3x_23/3x_23031.png';
import machine_5 from './images/3x_23/3x_23036.png';
import machine_6 from './images/3x_23/3x_23040.png';
import machine_7 from './images/3x_23/3x_23041.png';
import machine_8 from './images/3x_23/3x_23042.png';
import machine_9 from './images/3x_23/3x_23043.png';
import machine_10 from './images/3x_23/3x_23044.png';
import machine_11 from './images/3x_23/3x_23045.png';
import machine_12 from './images/3x_23/3x_23046.png';
import machine_13 from './images/3x_23/3x_23047.png';
import machine_14 from './images/3x_23/3x_23048.png';
import machine_15 from './images/3x_23/3x_23049.png';
import machine_16 from './images/3x_23/3x_23050.png';
import machine_17 from './images/3x_23/3x_23051.png';
import machine_18 from './images/3x_23/3x_23052.png';
import machine_19 from './images/3x_23/3x_23053.png';
import machine_20 from './images/3x_23/3x_23054.png';
import machine_21 from './images/3x_23/3x_23055.png';
import machine_22 from './images/3x_23/3x_23056.png';
import machine_23 from './images/3x_23/3x_23057.png';
import machine_24 from './images/3x_23/3x_23058.png';
import machine_25 from './images/3x_23/3x_23059.png';
import machine_26 from './images/3x_23/3x_23066.png';

class Fold5 extends Component {
  render() {
    return (
		<div className="fold" id="fold_5">
        	<div className="fold-left w-30">
            	<div>
                	<div className="small-heading">INVESTMENT SELECTION</div>
                	<div className="sub-heading">We select.</div>
                    <div className="sub-desc">We’ll identify the right exchange-traded funds (ETFs) to bring your asset allocation to life with every George you invest.</div>
					<img src={fold_5_mob} className="mob-img mobile" alt="" />
                    <span rel="investment-wrap" className="learn-more-link">LEARN MORE</span>
                </div>
                
            </div>
            <div className="fold-right w-70 desktop">
            	<div className="measurment-wrap">
                	<img src="" img-url={car} className="car" alt="" />
                	<img src="" img-url={boxes} className="box" alt="" />
                	<img src="" img-url={measurment_rod} className="rod" alt="" />
                    <div className="machine-wrap">
                    	<img src="" img-url={machine_1} className="machine" alt="" />
                        <img src="" img-url={machine_2} className="machine" alt="" />
                        <img src="" img-url={machine_3} className="machine" alt="" />
                        <img src="" img-url={machine_4} className="machine" alt="" />
                        <img src="" img-url={machine_5} className="machine" alt="" />
                        <img src="" img-url={machine_6} className="machine" alt="" />
                        <img src="" img-url={machine_7} className="machine" alt="" />
                        <img src="" img-url={machine_8} className="machine" alt="" />
                        <img src="" img-url={machine_9} className="machine" alt="" />
                        <img src="" img-url={machine_10} className="machine" alt="" />
                        <img src="" img-url={machine_11} className="machine" alt="" />
                        <img src="" img-url={machine_12} className="machine" alt="" />
                        <img src="" img-url={machine_13} className="machine" alt="" />
                        <img src="" img-url={machine_14} className="machine" alt="" />
                        <img src="" img-url={machine_15} className="machine" alt="" />
                        <img src="" img-url={machine_16} className="machine" alt="" />
                        <img src="" img-url={machine_17} className="machine" alt="" />
                        <img src="" img-url={machine_18} className="machine" alt="" />
                        <img src="" img-url={machine_19} className="machine" alt="" />
                        <img src="" img-url={machine_20} className="machine" alt="" />
                        <img src="" img-url={machine_21} className="machine" alt="" />
                        <img src="" img-url={machine_22} className="machine" alt="" />
                        <img src="" img-url={machine_23} className="machine" alt="" />
                        <img src="" img-url={machine_24} className="machine" alt="" />
                        <img src="" img-url={machine_25} className="machine" alt="" />
                        <img src="" img-url={machine_26} className="machine" alt="" />
                    </div>
                </div>
            </div>
        </div>
    );
  }
}


export default Fold5;
