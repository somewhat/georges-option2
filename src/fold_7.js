import React, { Component } from 'react';
import fold_7_mob from './images/fold-7-mob.png';

import manage_1_1 from './images/3x_34/3x_34021.png';
import manage_1_2 from './images/3x_34/3x_34022.png';
import manage_1_3 from './images/3x_34/3x_34023.png';
import manage_1_4 from './images/3x_34/3x_34024.png';
import manage_1_5 from './images/3x_34/3x_34025.png';
import manage_1_6 from './images/3x_34/3x_34026.png';
import manage_1_7 from './images/3x_34/3x_34027.png';
import manage_1_8 from './images/3x_34/3x_34028.png';
import manage_1_9 from './images/3x_34/3x_34029.png';
import manage_1_10 from './images/3x_34/3x_34030.png';
import manage_1_11 from './images/3x_34/3x_34031.png';
import manage_1_12 from './images/3x_34/3x_34032.png';
import manage_1_13 from './images/3x_34/3x_34033.png';
import manage_1_14 from './images/3x_34/3x_34034.png';
import manage_1_15 from './images/3x_34/3x_34035.png';
import manage_1_16 from './images/3x_34/3x_34036.png';
import manage_1_17 from './images/3x_34/3x_34037.png';
import manage_1_18 from './images/3x_34/3x_34038.png';
import manage_1_19 from './images/3x_34/3x_34039.png';
import manage_1_20 from './images/3x_34/3x_34040.png';
import manage_1_21 from './images/3x_34/3x_34041.png';
import manage_1_22 from './images/3x_34/3x_34042.png';
import manage_1_23 from './images/3x_34/3x_34043.png';
import manage_1_24 from './images/3x_34/3x_34044.png';
import manage_1_25 from './images/3x_34/3x_34045.png';
import manage_1_26 from './images/3x_34/3x_34046.png';
import manage_1_27 from './images/3x_34/3x_34047.png';
import manage_1_28 from './images/3x_34/3x_34048.png';
import manage_1_29 from './images/3x_34/3x_34049.png';
import manage_1_30 from './images/3x_34/3x_34050.png';
import manage_1_31 from './images/3x_34/3x_34051.png';
import manage_1_32 from './images/3x_34/3x_34052.png';
import manage_1_33 from './images/3x_34/3x_34053.png';
import manage_1_34 from './images/3x_34/3x_34054.png';
import manage_1_35 from './images/3x_34/3x_34055.png';
import manage_1_36 from './images/3x_34/3x_34056.png';
import manage_1_37 from './images/3x_34/3x_34057.png';
import manage_1_38 from './images/3x_34/3x_34058.png';
import manage_1_39 from './images/3x_34/3x_34059.png';
import manage_1_40 from './images/3x_34/3x_34060.png';
import manage_1_41 from './images/3x_34/3x_34061.png';
import manage_1_42 from './images/3x_34/3x_34062.png';
import manage_1_43 from './images/3x_34/3x_34063.png';
import manage_1_44 from './images/3x_34/3x_34064.png';
import manage_1_45 from './images/3x_34/3x_34065.png';
import manage_1_46 from './images/3x_34/3x_34066.png';
import manage_1_47 from './images/3x_34/3x_34067.png';
import manage_1_48 from './images/3x_34/3x_34068.png';
import manage_1_49 from './images/3x_34/3x_34069.png';
import manage_1_50 from './images/3x_34/3x_34070.png';

import manage_2_1 from './images/3x_32/3x_32025.png';
import manage_2_2 from './images/3x_32/3x_32027.png';
import manage_2_3 from './images/3x_32/3x_32029.png';
import manage_2_4 from './images/3x_32/3x_32031.png';
import manage_2_5 from './images/3x_32/3x_32033.png';
import manage_2_6 from './images/3x_32/3x_32035.png';
import manage_2_7 from './images/3x_32/3x_32037.png';
import manage_2_8 from './images/3x_32/3x_32039.png';
import manage_2_9 from './images/3x_32/3x_32046.png';
import manage_2_10 from './images/3x_32/3x_32048.png';
import manage_2_11 from './images/3x_32/3x_32050.png';
import manage_2_12 from './images/3x_32/3x_32052.png';
import manage_2_13 from './images/3x_32/3x_32054.png';
import manage_2_14 from './images/3x_32/3x_32056.png';
import manage_2_15 from './images/3x_32/3x_32058.png';
import manage_2_16 from './images/3x_32/3x_32060.png';
import manage_2_17 from './images/3x_32/3x_32062.png';
import manage_2_18 from './images/3x_32/3x_32064.png';
import manage_2_19 from './images/3x_32/3x_32066.png';
import manage_2_20 from './images/3x_32/3x_32068.png';
import manage_2_21 from './images/3x_32/3x_32070.png';
import manage_2_22 from './images/3x_32/3x_32072.png';
import manage_2_23 from './images/3x_32/3x_32074.png';
import manage_2_24 from './images/3x_32/3x_32076.png';
import manage_2_25 from './images/3x_32/3x_32078.png';
import manage_2_26 from './images/3x_32/3x_32080.png';
import manage_2_27 from './images/3x_32/3x_32082.png';
import manage_2_28 from './images/3x_32/3x_32084.png';
import manage_2_29 from './images/3x_32/3x_32093.png';
import manage_2_30 from './images/3x_32/3x_32095.png';
import manage_2_31 from './images/3x_32/3x_32097.png';
import manage_2_32 from './images/3x_32/3x_32099.png';
import manage_2_33 from './images/3x_32/3x_32101.png';
import manage_2_34 from './images/3x_32/3x_32103.png';
import manage_2_35 from './images/3x_32/3x_32105.png';
import manage_2_36 from './images/3x_32/3x_32107.png';
import manage_2_37 from './images/3x_32/3x_32109.png';
import manage_2_38 from './images/3x_32/3x_32111.png';

import manage_3_1 from './images/3x_33/3x_33052.png';
import manage_3_2 from './images/3x_33/3x_33054.png';
import manage_3_3 from './images/3x_33/3x_33056.png';
import manage_3_4 from './images/3x_33/3x_33058.png';
import manage_3_5 from './images/3x_33/3x_33060.png';
import manage_3_6 from './images/3x_33/3x_33062.png';
import manage_3_7 from './images/3x_33/3x_33064.png';
import manage_3_8 from './images/3x_33/3x_33066.png';
import manage_3_9 from './images/3x_33/3x_33068.png';
import manage_3_10 from './images/3x_33/3x_33070.png';
import manage_3_11 from './images/3x_33/3x_33072.png';
import manage_3_12 from './images/3x_33/3x_33074.png';
import manage_3_13 from './images/3x_33/3x_33076.png';
import manage_3_14 from './images/3x_33/3x_33078.png';
import manage_3_15 from './images/3x_33/3x_33080.png';
import manage_3_16 from './images/3x_33/3x_33082.png';
import manage_3_17 from './images/3x_33/3x_33084.png';
import manage_3_18 from './images/3x_33/3x_33086.png';
import manage_3_19 from './images/3x_33/3x_33088.png';
import manage_3_20 from './images/3x_33/3x_33090.png';
import manage_3_21 from './images/3x_33/3x_33092.png';
import manage_3_22 from './images/3x_33/3x_33094.png';
import manage_3_23 from './images/3x_33/3x_33096.png';
import manage_3_24 from './images/3x_33/3x_33098.png';
import manage_3_25 from './images/3x_33/3x_33100.png';
import manage_3_26 from './images/3x_33/3x_33102.png';
import manage_3_27 from './images/3x_33/3x_33104.png';
import manage_3_28 from './images/3x_33/3x_33106.png';
import manage_3_29 from './images/3x_33/3x_33108.png';
import manage_3_30 from './images/3x_33/3x_33110.png';
import manage_3_31 from './images/3x_33/3x_33112.png';
import manage_3_32 from './images/3x_33/3x_33114.png';
import manage_3_33 from './images/3x_33/3x_33116.png';
import manage_3_34 from './images/3x_33/3x_33118.png';



class Fold7 extends Component {
  render() {
    return (
		<div className="fold" id="fold_7">
        	<div className="fold-left w-30">
            	<div>
                	<div className="small-heading">REBALANCING</div>
                	<div className="sub-heading">We manage.</div>
                    <div className="sub-desc">We’ll monitor and regularly rebalance your investments to keep them aligned to your personalized plan and objectives.</div>
					<img src={fold_7_mob} className="mob-img mobile" alt="" />
                    <span rel="rebalancing-wrap" className="learn-more-link">LEARN MORE</span>
                </div>
                
            </div>
            <div className="fold-right w-70 desktop">
            	<div className="manage-wrap">
                	<div className="manage-1-wrap">
                    	<img src="" img-url={manage_1_1} className="manage-1" alt="" />
						<img src="" img-url={manage_1_2} className="manage-1" alt="" />
						<img src="" img-url={manage_1_3} className="manage-1" alt="" />
						<img src="" img-url={manage_1_4} className="manage-1" alt="" />
						<img src="" img-url={manage_1_5} className="manage-1" alt="" />
						<img src="" img-url={manage_1_6} className="manage-1" alt="" />
						<img src="" img-url={manage_1_7} className="manage-1" alt="" />
						<img src="" img-url={manage_1_8} className="manage-1" alt="" />
						<img src="" img-url={manage_1_9} className="manage-1" alt="" />
						<img src="" img-url={manage_1_10} className="manage-1" alt="" />
						<img src="" img-url={manage_1_11} className="manage-1" alt="" />
						<img src="" img-url={manage_1_12} className="manage-1" alt="" />
						<img src="" img-url={manage_1_13} className="manage-1" alt="" />
						<img src="" img-url={manage_1_14} className="manage-1" alt="" />
						<img src="" img-url={manage_1_15} className="manage-1" alt="" />
						<img src="" img-url={manage_1_16} className="manage-1" alt="" />
						<img src="" img-url={manage_1_17} className="manage-1" alt="" />
						<img src="" img-url={manage_1_18} className="manage-1" alt="" />
						<img src="" img-url={manage_1_19} className="manage-1" alt="" />
						<img src="" img-url={manage_1_20} className="manage-1" alt="" />
						<img src="" img-url={manage_1_21} className="manage-1" alt="" />
						<img src="" img-url={manage_1_22} className="manage-1" alt="" />
						<img src="" img-url={manage_1_23} className="manage-1" alt="" />
						<img src="" img-url={manage_1_24} className="manage-1" alt="" />
						<img src="" img-url={manage_1_25} className="manage-1" alt="" />
						<img src="" img-url={manage_1_26} className="manage-1" alt="" />
						<img src="" img-url={manage_1_27} className="manage-1" alt="" />
						<img src="" img-url={manage_1_28} className="manage-1" alt="" />
						<img src="" img-url={manage_1_29} className="manage-1" alt="" />
						<img src="" img-url={manage_1_30} className="manage-1" alt="" />
						<img src="" img-url={manage_1_31} className="manage-1" alt="" />
						<img src="" img-url={manage_1_32} className="manage-1" alt="" />
						<img src="" img-url={manage_1_33} className="manage-1" alt="" />
						<img src="" img-url={manage_1_34} className="manage-1" alt="" />
						<img src="" img-url={manage_1_35} className="manage-1" alt="" />
						<img src="" img-url={manage_1_36} className="manage-1" alt="" />
						<img src="" img-url={manage_1_37} className="manage-1" alt="" />
						<img src="" img-url={manage_1_38} className="manage-1" alt="" />
						<img src="" img-url={manage_1_39} className="manage-1" alt="" />
						<img src="" img-url={manage_1_40} className="manage-1" alt="" />
						<img src="" img-url={manage_1_41} className="manage-1" alt="" />
						<img src="" img-url={manage_1_42} className="manage-1" alt="" />
						<img src="" img-url={manage_1_43} className="manage-1" alt="" />
						<img src="" img-url={manage_1_44} className="manage-1" alt="" />
						<img src="" img-url={manage_1_45} className="manage-1" alt="" />
						<img src="" img-url={manage_1_46} className="manage-1" alt="" />
						<img src="" img-url={manage_1_47} className="manage-1" alt="" />
						<img src="" img-url={manage_1_48} className="manage-1" alt="" />
						<img src="" img-url={manage_1_49} className="manage-1" alt="" />
						<img src="" img-url={manage_1_50} className="manage-1" alt="" />
	                </div>
                    <div className="manage-2-wrap">
                    	<img src="" img-url={manage_2_1} className="manage-2" alt="" />
						<img src="" img-url={manage_2_2} className="manage-2" alt="" />
						<img src="" img-url={manage_2_3} className="manage-2" alt="" />
						<img src="" img-url={manage_2_4} className="manage-2" alt="" />
						<img src="" img-url={manage_2_5} className="manage-2" alt="" />
						<img src="" img-url={manage_2_6} className="manage-2" alt="" />
						<img src="" img-url={manage_2_7} className="manage-2" alt="" />
						<img src="" img-url={manage_2_8} className="manage-2" alt="" />
						<img src="" img-url={manage_2_9} className="manage-2" alt="" />
						<img src="" img-url={manage_2_10} className="manage-2" alt="" />
						<img src="" img-url={manage_2_11} className="manage-2" alt="" />
						<img src="" img-url={manage_2_12} className="manage-2" alt="" />
						<img src="" img-url={manage_2_13} className="manage-2" alt="" />
						<img src="" img-url={manage_2_14} className="manage-2" alt="" />
						<img src="" img-url={manage_2_15} className="manage-2" alt="" />
						<img src="" img-url={manage_2_16} className="manage-2" alt="" />
						<img src="" img-url={manage_2_17} className="manage-2" alt="" />
						<img src="" img-url={manage_2_18} className="manage-2" alt="" />
						<img src="" img-url={manage_2_19} className="manage-2" alt="" />
						<img src="" img-url={manage_2_20} className="manage-2" alt="" />
						<img src="" img-url={manage_2_21} className="manage-2" alt="" />
						<img src="" img-url={manage_2_22} className="manage-2" alt="" />
						<img src="" img-url={manage_2_23} className="manage-2" alt="" />
						<img src="" img-url={manage_2_24} className="manage-2" alt="" />
						<img src="" img-url={manage_2_25} className="manage-2" alt="" />
						<img src="" img-url={manage_2_26} className="manage-2" alt="" />
						<img src="" img-url={manage_2_27} className="manage-2" alt="" />
						<img src="" img-url={manage_2_28} className="manage-2" alt="" />
						<img src="" img-url={manage_2_29} className="manage-2" alt="" />
						<img src="" img-url={manage_2_30} className="manage-2" alt="" />
						<img src="" img-url={manage_2_31} className="manage-2" alt="" />
						<img src="" img-url={manage_2_32} className="manage-2" alt="" />
						<img src="" img-url={manage_2_33} className="manage-2" alt="" />
						<img src="" img-url={manage_2_34} className="manage-2" alt="" />
						<img src="" img-url={manage_2_35} className="manage-2" alt="" />
						<img src="" img-url={manage_2_36} className="manage-2" alt="" />
						<img src="" img-url={manage_2_37} className="manage-2" alt="" />
						<img src="" img-url={manage_2_38} className="manage-2" alt="" />

                    </div>
                    <div className="manage-3-wrap">
                    	<img src="" img-url={manage_3_1} className="manage-3" alt="" />
						<img src="" img-url={manage_3_2} className="manage-3" alt="" />
						<img src="" img-url={manage_3_3} className="manage-3" alt="" />
						<img src="" img-url={manage_3_4} className="manage-3" alt="" />
						<img src="" img-url={manage_3_5} className="manage-3" alt="" />
						<img src="" img-url={manage_3_6} className="manage-3" alt="" />
						<img src="" img-url={manage_3_7} className="manage-3" alt="" />
						<img src="" img-url={manage_3_8} className="manage-3" alt="" />
						<img src="" img-url={manage_3_9} className="manage-3" alt="" />
						<img src="" img-url={manage_3_10} className="manage-3" alt="" />
						<img src="" img-url={manage_3_11} className="manage-3" alt="" />
						<img src="" img-url={manage_3_12} className="manage-3" alt="" />
						<img src="" img-url={manage_3_13} className="manage-3" alt="" />
						<img src="" img-url={manage_3_14} className="manage-3" alt="" />
						<img src="" img-url={manage_3_15} className="manage-3" alt="" />
						<img src="" img-url={manage_3_16} className="manage-3" alt="" />
						<img src="" img-url={manage_3_17} className="manage-3" alt="" />
						<img src="" img-url={manage_3_18} className="manage-3" alt="" />
						<img src="" img-url={manage_3_19} className="manage-3" alt="" />
						<img src="" img-url={manage_3_20} className="manage-3" alt="" />
						<img src="" img-url={manage_3_21} className="manage-3" alt="" />
						<img src="" img-url={manage_3_22} className="manage-3" alt="" />
						<img src="" img-url={manage_3_23} className="manage-3" alt="" />
						<img src="" img-url={manage_3_24} className="manage-3" alt="" />
						<img src="" img-url={manage_3_25} className="manage-3" alt="" />
						<img src="" img-url={manage_3_26} className="manage-3" alt="" />
						<img src="" img-url={manage_3_27} className="manage-3" alt="" />
						<img src="" img-url={manage_3_28} className="manage-3" alt="" />
						<img src="" img-url={manage_3_29} className="manage-3" alt="" />
						<img src="" img-url={manage_3_30} className="manage-3" alt="" />
						<img src="" img-url={manage_3_31} className="manage-3" alt="" />
						<img src="" img-url={manage_3_32} className="manage-3" alt="" />
						<img src="" img-url={manage_3_33} className="manage-3" alt="" />
						<img src="" img-url={manage_3_34} className="manage-3" alt="" />

                        
					</div>                        
                </div>
            </div>
        </div>
    );
  }
}


export default Fold7;
